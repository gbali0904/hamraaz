# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in E:\JavaAssets\Android_Studio\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

#-keep public class in.gov.hamraaz.Account.AccountLandingActivity
#-keep public class in.gov.hamraaz.changepassword.ChangeDefaultPassword
#-keep public class in.gov.hamraaz.changepassword.ChangePasswordActivity
#-keep public class in.gov.hamraaz.Account.ForgotPasswordActivity
#-keep public class in.gov.hamraaz.login.LoginActivity
#-keep public class in.gov.hamraaz.Account.OTPActivity
#-keep public class in.gov.hamraaz.Account.SignupActivity
#-keep public class in.gov.hamraaz.Home.MainActivity
#-keep public class in.gov.hamraaz.Home.WebViewActivity
#-keep public class in.gov.hamraaz.Utils.DialogUtil
#-keep public class in.gov.hamraaz.Utils.EncryptionUtil
#-keep public class in.gov.hamraaz.Utils.MyFirebaseMessagingService
#-keep public class in.gov.hamraaz.Utils.RemoteConfigManager
#-keep public class in.gov.hamraaz.Utils.TextUtil
#-keep public class in.gov.hamraaz.Notification.PolicyImportantInfoActivityctivity

##---------------Begin: proguard configuration for Gson  ----------
# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature

# For using GSON @Expose annotation
-keepattributes *Annotation*

# Gson specific classes
-dontwarn sun.misc.**
#-keep class com.google.gson.stream.** { *; }

# Application classes that will be serialized/deserialized over Gson
-keep class com.google.gson.examples.android.model.** { *; }

# Prevent proguard from stripping interface information from TypeAdapterFactory,
# JsonSerializer, JsonDeserializer instances (so they can be used in @JsonAdapter)
-keep class * implements com.google.gson.TypeAdapterFactory
-keep class * implements com.google.gson.JsonSerializer
-keep class * implements com.google.gson.JsonDeserializer

##---------------End: proguard configuration for Gson  ----------

##---------------Begin: proguard configuration for Firebase  ----------
-keep class com.firebase.** { *; }
-keep class org.apache.** { *; }
-keepnames class com.fasterxml.jackson.** { *; }
-keepnames class javax.servlet.** { *; }
-keepnames class org.ietf.jgss.** { *; }
-dontwarn org.w3c.dom.**
-dontwarn org.joda.time.**
-dontwarn org.shaded.apache.**
-dontwarn org.ietf.jgss.**

# Only necessary if you downloaded the SDK jar directly instead of from maven.
-keep class com.shaded.fasterxml.jackson.** { *; }

-keep class !com.my.package.** { *; }

##---------------End: proguard configuration for Firebase  ----------

#-dontwarn *.*

-dontwarn in.gov.hamraaz.**

-optimizations !code/simplification/arithmetic

-keepattributes InnerClasses
-keepattributes EnclosingMethod
-dontskipnonpubliclibraryclasses
-forceprocessing
-optimizationpasses 99
-overloadaggressively


-keep class !in.gov.hamraaz.** { *; }
-keep public class in.gov.hamraaz.login.** { *; }
-keep public class in.gov.hamraaz.Account.** { *; }
-keep public class in.gov.hamraaz.changepassword.** { *; }
-keep public class in.gov.hamraaz.contactus.** { *; }
-keep public class in.gov.hamraaz.family.** { *; }
-keep public class in.gov.hamraaz.fragment.** { *; }
-keep public class in.gov.hamraaz.Grievance.** { *; }
-keep public class in.gov.hamraaz.Home.** { *; }
-keep public class in.gov.hamraaz.leave.** { *; }
-keep public class in.gov.hamraaz.Notification.** { *; }
-keep public class in.gov.hamraaz.obsns.** { *; }
-keep public class in.gov.hamraaz.paydetail.** { *; }
-keep public class in.gov.hamraaz.postrequestview.** { *; }
-keep public class in.gov.hamraaz.ppodetail.** { *; }
-keep public class in.gov.hamraaz.profile.** { *; }
-keep public class in.gov.hamraaz.pto.** { *; }
-keep public class in.gov.hamraaz.servicevoter.** { *; }
-keep public class in.gov.hamraaz.Utils.** { *; }
-keep public class in.gov.hamraaz.App
-keep public class in.gov.hamraaz.SplashActivity




-keep class org.junit.** { *; }
-dontwarn org.junit.**

-keep class junit.** { *; }

-assumenosideeffects class android.util.Log {
public static *** d(...);
public static *** v(...);
public static *** i(...);
public static *** w(...);
public static *** e(...);
}



-dontwarn junit.**



-dontwarn android.**
-dontwarn android.test.**
-dontwarn android.support.test.**

