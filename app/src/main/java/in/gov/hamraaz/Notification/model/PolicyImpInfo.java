package in.gov.hamraaz.Notification.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Pallaw Pathak on 01/12/18. - https://www.linkedin.com/in/pallaw-pathak-a6a324a1/
 */

public class PolicyImpInfo {

    /**
     * title : pto Publication
     * msg : As per latest policy, all individuals whether married or below 25 years of age are eligible for HRA.  Please ensure publication of Part II Order for same immediately.  The individual should not be allotted any type of government married accommodation for claiming HRA.
     * Date : 01/09/2018
     */

    @SerializedName("title")
    private String title;
    @SerializedName("msg")
    private String msg;
    @SerializedName("Date")
    private String Date;
    @SerializedName("pdf_url")
    private String pdf_url;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String Date) {
        this.Date = Date;
    }

    public String getPdf_url() {
        return pdf_url;
    }

    public void setPdf_url(String pdf_url) {
        this.pdf_url = pdf_url;
    }
}
