package in.gov.hamraaz.Notification.pdfview;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import in.gov.hamraaz.R;
import in.gov.hamraaz.Utils.Constant;
import in.gov.hamraaz.Utils.RemoteConfigManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class DocumentFragment extends Fragment {


    public static final String TAG = DocumentFragment.class.getSimpleName();
    @BindView(R.id.webview)
    WebView webView;
    Unbinder unbinder;
    private String pdf_url;
    private FragmentActivity activity;
    private ProgressDialog progDailog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_document, container, false);
        unbinder = ButterKnife.bind(this, view);

        Bundle bundle = getArguments();
        pdf_url = bundle.getString(Constant.PDF_URL);

        String url = pdf_url;
        Log.e("this", "pdf url::::   " + url);


        activity = getActivity();

        progDailog = ProgressDialog.show(activity, "Loading", "Please wait...", true);
        progDailog.setCancelable(false);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);

        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                progDailog.show();
                view.loadUrl(url);

                return true;
            }

            @Override
            public void onPageFinished(WebView view, final String url) {
                progDailog.dismiss();
            }
        });
        if (pdf_url.endsWith(".jpg") || pdf_url.endsWith(".png") || pdf_url.endsWith(".PNG") | pdf_url.endsWith(".JPEG") | pdf_url.endsWith(".jpeg")) {
            webView.loadUrl(url);
        } else {
            webView.loadUrl(getResources().getString(R.string.google_drive_url) + url);
        }
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
