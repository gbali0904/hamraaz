package in.gov.hamraaz.Notification;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.gov.hamraaz.App;
import in.gov.hamraaz.Grievance.GrievanceThreadAdapter;
import in.gov.hamraaz.Grievance.ShowGrievanceActivity;
import in.gov.hamraaz.Grievance.model.GrievanceThread;
import in.gov.hamraaz.Home.MainActivity;
import in.gov.hamraaz.Home.WebViewActivity;
import in.gov.hamraaz.Notification.model.Notification;
import in.gov.hamraaz.R;
import in.gov.hamraaz.Utils.DialogUtil;
import in.gov.hamraaz.Utils.RemoteConfigManager;
import in.gov.hamraaz.Utils.TAGs;
import in.gov.hamraaz.Utils.Util;

public class NotificationActivity extends AppCompatActivity {

    private static String panHash;
    @BindView(R.id.rvNotification) RecyclerView rvNotification;
    private NotificationThreadAdapter adapter;
    ArrayList<Notification> notificationThreads = new ArrayList<>();
    private String url="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Notifications");

        rvNotification.setHasFixedSize(true);
        rvNotification.setLayoutManager(new LinearLayoutManager(this));
        adapter = new NotificationThreadAdapter(notificationThreads);
        rvNotification.setAdapter(adapter);

        downloadNotifications();
    }

    private void downloadNotifications() {
        final ProgressDialog progress = DialogUtil.createProgressDialog(NotificationActivity.this, "Loading", "Please wait");
        progress.show();

        if (!Util.checkInternet(NotificationActivity.this)) {
            url = "https://10.0.0.161/s_pan/p_notification.aspx";
        } else {
            url = RemoteConfigManager.getViewNotificationUrl();
        }
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.length() > 0) {
                    try {
                        progress.dismiss();
                        Gson gson = new Gson();
                        Type type = new TypeToken<List<Notification>>() {
                        }.getType();
                        ArrayList<Notification> notificationThreadsList = gson.fromJson(response, type);

                        notificationThreads.clear();
                        notificationThreads.addAll(notificationThreadsList);
                        adapter.notifyDataSetChanged();

                    } catch (Exception e) {
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("p_hash", App.getInstance().getPreference().getString(TAGs.KEY_SCHEME_FRAME,""));
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            default:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public static void showNotifiation(Context context, String panHash) {
        NotificationActivity.panHash = panHash;
        context.startActivity(new Intent(context, NotificationActivity.class));
    }
}
