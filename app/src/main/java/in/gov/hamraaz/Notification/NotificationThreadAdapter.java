package in.gov.hamraaz.Notification;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.gov.hamraaz.Grievance.model.GrievanceThread;
import in.gov.hamraaz.Notification.model.Notification;
import in.gov.hamraaz.R;

/**
 * Created by Pallaw Pathak on 31/10/18. - https://www.linkedin.com/in/pallaw-pathak-a6a324a1/
 */

public class NotificationThreadAdapter extends RecyclerView.Adapter<NotificationThreadAdapter.MyViewHolder> {
    private ArrayList<Notification> mDataset = new ArrayList<>();


    public NotificationThreadAdapter(ArrayList<Notification> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_notification, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.bindData(mDataset.get(position));

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtItemTitle) TextView txtItemTitle;
        @BindView(R.id.txtItemDate) TextView txtItemDate;
        @BindView(R.id.txtItemMsg) TextView txtItemMsg;

        public MyViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        public void bindData(Notification item) {
            txtItemTitle.setText(String.format("Notification title: %s", item.getNotif_title()));
            txtItemDate.setText(item.getNotif_dt());
            txtItemMsg.setText(item.getNotif_msg());
        }

    }
}
