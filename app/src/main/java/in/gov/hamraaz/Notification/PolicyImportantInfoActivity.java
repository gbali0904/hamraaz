package in.gov.hamraaz.Notification;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.gov.hamraaz.Notification.model.PolicyImpInfo;
import in.gov.hamraaz.R;
import in.gov.hamraaz.Utils.Constant;
import in.gov.hamraaz.Utils.DialogUtil;
import in.gov.hamraaz.Utils.RemoteConfigManager;

public class PolicyImportantInfoActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.rvPolicyImpInfo) RecyclerView rvPolicyImpInfo;
    private PolicyImpInfoListAdapter adapter;
    private String pan_hass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flash_message);
        ButterKnife.bind(this);

        Bundle extras = getIntent().getExtras();
        if (null != extras) {
            pan_hass = extras.getString(Constant.KEY_PAN_HASH);
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.nav_policy_imp_info));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rvPolicyImpInfo.setHasFixedSize(true);
        rvPolicyImpInfo.setLayoutManager(new LinearLayoutManager(this));
        adapter = new PolicyImpInfoListAdapter(this);
        rvPolicyImpInfo.setAdapter(adapter);

        downloadMsg();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            default:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void downloadMsg() {
        final ProgressDialog progressDialog = DialogUtil.createProgressDialog(PolicyImportantInfoActivity.this, "Please wait", "Please wait while we are downloading your flash messages");
        progressDialog.show();

        String url = RemoteConfigManager.getFlashMsgUrl();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.cancel();


                Type collectionType = new TypeToken<List<PolicyImpInfo>>(){}.getType();
                List<PolicyImpInfo> policyImpInfos = new Gson().fromJson( response , collectionType);

                adapter.setDatat(policyImpInfos);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.cancel();
                DialogUtil.createAlertDialog(PolicyImportantInfoActivity.this, "Error", "Something went wrong Please try again", "Okay")
                        .show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("schemeFrame", pan_hass);
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
