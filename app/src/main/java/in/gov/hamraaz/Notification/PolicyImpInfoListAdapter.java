package in.gov.hamraaz.Notification;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.gov.hamraaz.Notification.model.Notification;
import in.gov.hamraaz.Notification.model.PolicyImpInfo;
import in.gov.hamraaz.Notification.pdfview.DocumentFragment;
import in.gov.hamraaz.R;
import in.gov.hamraaz.Utils.Constant;
import in.gov.hamraaz.Utils.RemoteConfigManager;
import in.gov.hamraaz.fragment.MainActivityFragment;
import in.gov.hamraaz.leave.LeaveFragment;

/**
 * Created by Pallaw Pathak on 31/10/18. - https://www.linkedin.com/in/pallaw-pathak-a6a324a1/
 */

public class PolicyImpInfoListAdapter extends RecyclerView.Adapter<PolicyImpInfoListAdapter.MyViewHolder> {
    private final PolicyImportantInfoActivity policyImportantInfoActivity;
    private List<PolicyImpInfo> mDataset;


    public PolicyImpInfoListAdapter(PolicyImportantInfoActivity policyImportantInfoActivity) {
        mDataset = new ArrayList<>();
        this.policyImportantInfoActivity = policyImportantInfoActivity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_policy, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.bindData(mDataset.get(position), policyImportantInfoActivity);

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void setDatat(List<PolicyImpInfo> policyImpInfos) {
        mDataset = policyImpInfos;
        notifyDataSetChanged();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtItemTitle)
        TextView txtItemTitle;
        @BindView(R.id.txtItemDate)
        TextView txtItemDate;
        @BindView(R.id.txtItemMsg)
        TextView txtItemMsg;
        @BindView(R.id.clickHere)
        TextView clickHere;

        public MyViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        public void bindData(final PolicyImpInfo item, final PolicyImportantInfoActivity policyImportantInfoActivity) {
            txtItemTitle.setText(String.format("Title: %s", item.getTitle()));
            txtItemDate.setText(String.format("Date: %s", item.getDate()));
            txtItemMsg.setText("" + item.getMsg());
            if (item.getPdf_url() == null || item.getPdf_url().equals("") || item.getPdf_url().isEmpty()) {
                clickHere.setVisibility(View.GONE);
            } else {
                clickHere.setVisibility(View.VISIBLE);
            }

            clickHere.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DocumentFragment documentFragment = new DocumentFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString(Constant.PDF_URL, RemoteConfigManager.getPdfBaseUrl() + item.getPdf_url());
                    documentFragment.setArguments(bundle);
                    MainActivityFragment.instansiate(documentFragment, policyImportantInfoActivity, DocumentFragment.TAG, "Document View");

                }
            });
        }

    }
}
