package in.gov.hamraaz.Notification.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Pallaw Pathak on 29/11/18. - https://www.linkedin.com/in/pallaw-pathak-a6a324a1/
 */

public class Notification {

    /**
     * notif_title : test ro
     * notif_msg : testing message ro : last month pto has been updated
     * notif_dt : 27/11/2018
     */

    @SerializedName("notif_title")
    private String notif_title;
    @SerializedName("notif_msg")
    private String notif_msg;
    @SerializedName("notif_dt")
    private String notif_dt;

    public String getNotif_title() {
        return notif_title;
    }

    public void setNotif_title(String notif_title) {
        this.notif_title = notif_title;
    }

    public String getNotif_msg() {
        return notif_msg;
    }

    public void setNotif_msg(String notif_msg) {
        this.notif_msg = notif_msg;
    }

    public String getNotif_dt() {
        return notif_dt;
    }

    public void setNotif_dt(String notif_dt) {
        this.notif_dt = notif_dt;
    }
}
