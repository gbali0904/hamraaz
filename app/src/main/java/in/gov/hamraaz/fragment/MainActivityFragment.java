package in.gov.hamraaz.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.gov.hamraaz.R;
import in.gov.hamraaz.Utils.FragmentManagerUtil;

public class MainActivityFragment extends AppCompatActivity {

    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.frameLay)
    FrameLayout frameLay;
    @BindView(R.id.fram_dialog)
    RelativeLayout framDialog;
    private static Fragment fragment;
    private static Context context;
    private static String tag;
    private static String status;

    public static void instansiate(Fragment fragment, Context context, String tag, String status) {
        MainActivityFragment.fragment = fragment;
        MainActivityFragment.context = context;
        MainActivityFragment.tag = tag;
        MainActivityFragment.status = status;
        context.startActivity(new Intent(context, MainActivityFragment.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_fragment);
        ButterKnife.bind(this);
        title.setText(status);
        FragmentManagerUtil.replaceFragment(getSupportFragmentManager(), R.id.fram_dialog, fragment, false, tag);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @OnClick({R.id.back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back:
                super.onBackPressed();
                break;
        }
    }

}
