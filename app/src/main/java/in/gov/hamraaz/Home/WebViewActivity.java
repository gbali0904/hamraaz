package in.gov.hamraaz.Home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.gov.hamraaz.R;
import in.gov.hamraaz.Utils.TextUtil;

public class WebViewActivity extends AppCompatActivity {

    private static CharSequence title;
    private static String type;
    private static String response;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.wv_main) WebView wvMain;

    public static void startActivityWith(Context context, CharSequence title, String type, String response) {
        WebViewActivity.title = title;
        WebViewActivity.type = type;
        WebViewActivity.response = response;

        context.startActivity(new Intent(context, WebViewActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(title);
        try {
            view_type(type, response);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            default:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    void view_type(String type, String value) throws JSONException {
        WebView wv = (WebView) findViewById(R.id.wv_main);
        wv.setVisibility(View.VISIBLE);

        if (TextUtil.isEqual(type, "family")) {
            String html_txt = "<div align=center><h2><u>Family</u></h2><table border=1 width=90%><tr><td align=center><font color='#940000'><b>Ser</b></font></td><td  align=center><font color='#940000'><b>Name</b></font></td><td  align=center><font color='#940000'><b>DOB</b></font></td><td  align=center><font color='#940000'><b>Relation</b></font></td></tr>";
            JSONArray ja = new JSONArray(value);
            JSONObject jo = null;
            for (int i = 0; i < ja.length(); i++) {
                jo = ja.getJSONObject(i);
                html_txt = html_txt + "<tr><td align=center>" + (i + 1) + "</td><td>" + jo.get("FM_NAME") + "</td><td>" + (jo
                        .get("DOB")
                        .toString()) + "</td><td>" + jo.get("RELATION") + "</td></tr>";
            }
            html_txt = html_txt + "</table></div>";
            wv.loadDataWithBaseURL(null, html_txt, "text/html", "utf-8", null);
            wv.loadData(html_txt, "text/html; charset=utf-8", "utf-8");
        }


        if (TextUtil.isEqual(type, "address")) {
            String html_txt = "<h2><u>Address</u></h2>";
            JSONArray ja = new JSONArray(value);
            JSONObject jo = null;
            for (int i = 0; i < ja.length(); i++) {
                jo = ja.getJSONObject(i);


                html_txt = html_txt + "<h3>" + "Village : " + jo.get("fd_village") + "</h3>";
                html_txt = html_txt + "<h3>" + "Tehsil : " + jo.get("fd_tehsil") + "</h3>";
                html_txt = html_txt + "<h3>" + "Post Office : " + jo.get("fd_po") + "</h3>";
                html_txt = html_txt + "<h3>" + "Tele Off : " + jo.get("fd_tel_off") + "</h3>";
                html_txt = html_txt + "<h3>" + "Police Stn : " + jo.get("fd_police_stn") + "</h3>";
                html_txt = html_txt + "<h3>" + "Rly Stn : " + jo.get("fd_rly_stn") + "</h3>";
                html_txt = html_txt + "<h3>" + "Dist : " + jo.get("fd_dist") + "</h3>";
                html_txt = html_txt + "<h3>" + "State : " + jo.get("fd_state") + "</h3>";
            }
            wv.loadDataWithBaseURL(null, html_txt, "text/html", "utf-8", null);

            wv.loadData(html_txt, "text/html; charset=utf-8", "utf-8");
        }


        if (TextUtil.isEqual(type, "pto")) {
            String html_txt = "<div align=center><h2><u>PART II ORDERS DETAILS<br>(LAST THREE YEARS)</u></h2>" + "<table border=1 width=90%>" + "<tr><td align=center><font color='#940000'><b>SER</b></font></td>" + "<td  align=center><font color='#940000'><b>pto NO</b></font></td>" + "<td  align=center><font color='#940000'><b>ITEM NO</b></font></td>" + "<td  align=center><font color='#940000'><b>DESC</b></font></td>" + "<td  align=center><font color='#940000'><b>RO RECD DT</b></font></td>" + "<td  align=center><font color='#940000'><b>RO STATUS_MODE</b></font></td>" + "<td  align=center><font color='#940000'><b>RO REJ REASON</b></font></td>" + "<td  align=center><font color='#940000'><b>PAO STATUS_MODE</b></font></td>" + "<td  align=center><font color='#940000'><b>PAO REJ REASON</b></font></td>" + "</tr>";
            JSONArray ja = new JSONArray(value);
            JSONObject jo = null;

            for (int i = 0; i < ja.length(); i++) {
                jo = ja.getJSONObject(i);


                html_txt = html_txt + "<tr><td align=center>" + (i + 1) + "</td>" + "<td>" + jo.get("PTO_NO") + "</td>" + "<td>" + jo
                        .get("ITEM_NO") + "</td>" + "<td>" + jo.get("PTO_Desc") + "</td>" + "<td>" + (jo
                        .get("RO_RECD_DT")
                        .toString()) + "</td>" + "<td>" + jo.get("RO_STATUS") + "</td>" + "<td>" + jo
                        .get("RO_REJ_REASON") + "</td>" +

                        "<td>" + jo.get("PAO_STATUS") + "</td>" + "<td>" + jo.get("PAO_REJ_REASON") + "</td>" + "</tr>";
            }
            html_txt = html_txt + "</table></div>";
            wv.loadDataWithBaseURL(null, html_txt, "text/html", "utf-8", null);

            wv.loadData(html_txt, "text/html; charset=utf-8", "utf-8");
        }

        if (TextUtil.isEqual(type, "noti")) {
            String html_txt = "<div align=center><h2><u>NOTIFICATION SINCE LAST 60 DAYS</u></h2><table border=1 width=90%><tr><td align=center><font color='#940000'><b>Ser</b></font></td><td  align=center><font color='#940000'><b>Desc</b></font></td></tr>";
            JSONArray ja = new JSONArray(value);
            JSONObject jo = null;

            for (int i = 0; i < ja.length(); i++) {
                jo = ja.getJSONObject(i);

                if (TextUtil.isEqual(jo.get("vfd_type").toString(), "hony"))
                    html_txt = html_txt + "<tr><td align=center>" + (i + 1) + "</td><td><i>Notification Date : " + (jo
                            .get("vfd_noti_dt")
                            .toString()) + "</i><br/>Bahut Bahut Mubarak !!<br/>Aapko <b><u><font color='#940000'>" + jo
                            .get("vfd_dt") + "</font></u></b> ko   <b><u><font color='#940000'>" + (jo
                            .get("vfd_desc")
                            .toString()) + "</font></u></b> ka rank diya gaya hai.</td></tr>";

                if (TextUtil.isEqual(jo.get("vfd_type").toString(), "med_due"))
                    html_txt = html_txt + "<tr><td align=center>" + (i + 1) + "</td><td><i>Notification Date : " + (jo
                            .get("vfd_noti_dt")
                            .toString()) + "</i><br/>Apki medical category review  <b><u><font color='#940000'>" + (jo
                            .get("vfd_dt")
                            .toString()) + "</font></u></b> ko hai. Krupya samay par len.</td></tr>";


                if (TextUtil.isEqual(jo.get("vfd_type").toString(), "post"))
                    html_txt = html_txt + "<tr><td align=center>" + (i + 1) + "</td><td><i>Notification Date : " + (jo
                            .get("vfd_noti_dt")
                            .toString()) + "</i><br/>Aapka Posting Order issue ho gaya hai.  Move date :- <b><u><font color='#940000'>" + (jo
                            .get("vfd_dt")
                            .toString()) + "</font></u></b>.Adhik jaankari ke liye kripya apni unit se sampark karen</td></tr>";

                if (TextUtil.isEqual(jo.get("vfd_type").toString(), "prom"))
                    html_txt = html_txt + "<tr><td align=center>" + (i + 1) + "</td><td><i>Notification Date : " + (jo
                            .get("vfd_noti_dt")
                            .toString()) + "</i><br/>Bahut Bahut Mubarak apka Promotion Order issue ho gaya hai.  Effective  date :- <b><u><font color='#940000'>" + (jo
                            .get("vfd_dt")
                            .toString()) + "</font></u></b>. Kripya apni unit se sampark karen..</td></tr>";

                if (TextUtil.isEqual(jo.get("vfd_type").toString(), "acr"))
                    html_txt = html_txt + "<tr><td align=center>" + (i + 1) + "</td><td><i>Notification Date : " + (jo
                            .get("vfd_noti_dt")
                            .toString()) + "</i><br/>Apka saal :- <b><u><font color='#940000'>" + (jo
                            .get("vfd_dt")
                            .toString()) + "</font></u></b> ka ACR Record Office mein <b> Nahi </b> pahuncha hai. Kripya apni unit mein sampark karen.</td></tr>";
                if (TextUtil.isEqual(jo.get("vfd_type").toString(), "acr_recd"))
                    html_txt = html_txt + "<tr><td align=center>" + (i + 1) + "</td><td><i>Notification Date : " + (jo
                            .get("vfd_noti_dt")
                            .toString()) + "</i><br/>Apka  saal :- <b><u><font color='#940000'>" + (jo
                            .get("vfd_dt")
                            .toString()) + "</font></u></b> ka ACR Record Office mein  <b> Puhuncha </b> gaya hai </td></tr>";
                if (TextUtil.isEqual(jo.get("vfd_type").toString(), "course"))
                    html_txt = html_txt + "<tr><td align=center>" + (i + 1) + "</td><td><i>Notification Date : " + (jo
                            .get("vfd_noti_dt")
                            .toString()) + "</i><br/>Aapka course main detailment aa gaya hai jo ki  <b><u><font color='#940000'>" + (jo
                            .get("vfd_dt")
                            .toString()) + "</font></u></b>.ko shuru hoga. Adhik jaankari Krupya apni unit main sampark karen.</td></tr>";
                if (TextUtil.isEqual(jo.get("vfd_type").toString(), "pto_recd"))
                    html_txt = html_txt + "<tr><td align=center>" + (i + 1) + "</td><td><i>Notification Date : " + (jo
                            .get("vfd_noti_dt")
                            .toString()) + "</i><br/>Aapka Part II Order No <b><u><font color='#940000'>" + (jo
                            .get("vfd_desc")
                            .toString()) + "</font></u></b>.Record Office/PAO (OR) main <b><u><font color='#940000'>" + (jo
                            .get("vfd_dt")
                            .toString()) + "</font></u></b> ko pahunch gaya hai</td></tr>";
                if (TextUtil.isEqual(jo.get("vfd_type").toString(), "pto_accept"))
                    html_txt = html_txt + "<tr><td align=center>" + (i + 1) + "</td><td><i>Notification Date : " + (jo
                            .get("vfd_noti_dt")
                            .toString()) + "</i><br/>Aapka Part II Order No <b><u><font color='#940000'>" + (jo
                            .get("vfd_desc")
                            .toString()) + "</font></u></b>.Record Office/PAO (OR) main <b><u><font color='#940000'>" + (jo
                            .get("vfd_dt")
                            .toString()) + "</font></u></b>ho gaya hai</td></tr>";

                if (TextUtil.isEqual(jo.get("vfd_type").toString(), "disch"))
                    html_txt = html_txt + "<tr><td align=center>" + (i + 1) + "</td><td><i>Notification Date : " + (jo
                            .get("vfd_noti_dt")
                            .toString()) + "</i><br/>Apka discharge order  issue ho gaya hai. Apka SOS date hai :- <b><u><font color='#940000'>" + (jo
                            .get("vfd_dt")
                            .toString()) + "</font></u></b>. Kripya apna pension document banane ke liye apne unit se sampark karen" + " .  Apse anurodh hai ki apne pension documents  Record Office ko samai par pahunchaye ." + "</td></tr>";


                //    html_txt=html_txt+"<tr><td align=center>"+(i+1)+"</td><td>"+jo.get("FM_NAME") + "</td><td>"+(jo.get("DOB").toString()) + "</td><td>" + jo.get("RELATION")+"</td></tr>";
            }
            html_txt = html_txt + "</table></div>";
            wv.loadDataWithBaseURL(null, html_txt, "text/html", "utf-8", null);

            wv.loadData(html_txt, "text/html; charset=utf-8", "utf-8");
        }
        if (TextUtil.isEqual(type, "pay_dtls")) {
            String html_txt = "";
            JSONArray ja = new JSONArray(value);
            JSONObject jo = null;

            for (int i = 0; i < ja.length(); i++) {
                jo = ja.getJSONObject(i);
                String month_ending = jo.getString("MONTH_ENDING");
                html_txt = "<div align=center> <h4><u> PAY DETAILS FOR THE MONTH ENDING : "+month_ending+"</u></h4> <table border=1 width=90%><tr><td align=center colspan=2  width=50%><font color='#940000'><b>CREDITS</b></font></td><td  align=center colspan=2><font color='#940000'   width=50%><b>DEBITS</b></font></td></tr>";

                html_txt = html_txt + "<tr><td>Opening Bal</td><td align=right>" + jo.get("Op_balance") + "</td><td>Opening Bal</td><td align=right>" + jo
                        .get("Op_balance_debit") + "</tr>";
                html_txt = html_txt + "<tr><td>Band Pay</td><td  align=right>" + jo.get("Band_pay") + "</td><td>AFPPF Subs</td><td align=right>" + jo
                        .get("AFPPF_subscription") + "</tr>";
                html_txt = html_txt + "<tr><td>Grade Pay</td><td  align=right>" + jo.get("Grade_pay") + "</td><td>AGIF Debit</td><td align=right>" + jo
                        .get("AGIF") + "</tr>";
                html_txt = html_txt + "<tr><td>X Pay</td><td align=right>" + jo.get("GP_X_pay") + "</td><td>PLI Debit</td><td align=right>" + jo
                        .get("PLI") + "</tr>";
                html_txt = html_txt + "<tr><td>MS Pay</td><td align=right>" + jo.get("MS_pay") + "</td><td>Loan Adv</td><td align=right>" + jo
                        .get("Loan_advance") + "</tr>";
                html_txt = html_txt + "<tr><td>DA</td><td align=right>" + jo.get("DA") + "</td><td>E Ticketing</td><td align=right>" + jo
                        .get("E_Ticketing") + "</tr>";
                html_txt = html_txt + "<tr><td>PMHA</td><td align=right>" + jo.get("PMHA") + "</td><td>Income Tax</td><td align=right>" + jo
                        .get("Income_tax_EC") + "</tr>";
                html_txt = html_txt + "<tr><td>LRA</td><td align=right>" + jo.get("LRA") + "</td><td>FAMO</td><td align=right>" + jo
                        .get("FAMO") + "</tr>";
                html_txt = html_txt + "<tr><td>Recurring Allw</td><td align=right>" + jo.get("Recurr_alwc") + "</td><td></td><td></tr>";
                html_txt = html_txt + "<tr><td>Arrears P&A</td><td align=right>" + jo.get("Arr_Rec_pay_alwc") + "</td><td></td><td></tr>";
                html_txt = html_txt + "<tr><td>Other Allw</td><td align=right>" + jo.get("Other_adj") + "</td><td>Credited to Bank</td><td align=right>" + jo
                        .get("Amt_credit_to_bank") + "</td></tr>";
                html_txt = html_txt + "<tr><td><b>Total Credit</td><td align=right><b>" + jo.get("Tolal_credit") + "</td><td><b>Total Debit Debit</td><td align=right><b>" + jo
                        .get("Total_Debit") + "</tr>";


            }
            html_txt = html_txt + "</table></div>";
            wv.loadDataWithBaseURL(null, html_txt, "text/html", "utf-8", null);

            wv.loadData(html_txt, "text/html; charset=utf-8", "utf-8");
        }
        if (TextUtil.isEqual(type, "lve_encash")) {
            String html_txt = "<div align=center><h2><u>Leave Encashment</u></h2><table border=1 width=90%><tr><td align=center  width=50%><font color='#940000'><b>Year</b></font></td><td  align=center ><font color='#940000'   width=50%><b>Days</b></font></td></tr>";
            JSONArray ja = new JSONArray(value);
            JSONObject jo = null;

            for (int i = 0; i < ja.length(); i++) {
                jo = ja.getJSONObject(i);

                if (TextUtil.isEqual(jo.get("fd_year").toString(), "Total"))
                    html_txt = html_txt + "<tr><td align=center><b>" + jo.get("fd_year") + "</b></td><td align=center><b>" + jo
                            .get("fd_days") + "</b></td></tr>";
                else
                    html_txt = html_txt + "<tr><td align=center>" + jo.get("fd_year") + "</td><td align=center>" + jo
                            .get("fd_days") + "</td></tr>";
            }
            html_txt = html_txt + "</table></div>";
            wv.loadDataWithBaseURL(null, html_txt, "text/html", "utf-8", null);

            wv.loadData(html_txt, "text/html; charset=utf-8", "utf-8");
        }
        if (TextUtil.isEqual(type, "ppo")) {
            String html_txt = "<div align=center><h2><u>PPO Details</u></h2><table border=0 width=90%  cellpadding='9'>";
            JSONArray ja = new JSONArray(value);
            JSONObject jo = null;

            for (int i = 0; i < ja.length(); i++) {
                jo = ja.getJSONObject(i);


                html_txt = html_txt + "<tr><td align=left width=45%>PPO No</td><td align=center  width=10%>-</td><td align=left  width=45%>" + jo
                        .get("ppo_no") + "</td></tr>";
                html_txt = html_txt + "<tr><td align=left width=45%>PPO Date</td><td align=center  width=10%>-</td><td align=left  width=45%>" + (jo
                        .get("ppo_dt")
                        .toString()) + "</td></tr>";
                html_txt = html_txt + "<tr><td align=left width=45%>Rate of Service Pension</td><td align=center  width=10%>-</td><td align=left  width=45%>" + jo
                        .get("rate_service_pen") + "</td></tr>";
                html_txt = html_txt + "<tr><td align=left width=45%>Rate of Disability Pension</td><td align=center  width=10%>-</td><td align=left  width=45%>" + jo
                        .get("rate_dis_pen") + "</td></tr>";
                html_txt = html_txt + "<tr><td align=left width=45%>Rate of War Injury Pension</td><td align=center  width=10%>-</td><td align=left  width=45%>" + jo
                        .get("rate_war_injury_pen") + "</td></tr>";
                html_txt = html_txt + "<tr><td align=left width=45%>DCRG Amount</td><td align=center  width=10%>-</td><td align=left  width=45%>" + jo
                        .get("dcrg_amt") + "</td></tr>";
                html_txt = html_txt + "<tr><td align=left width=45%>Commutation Amount</td><td align=center  width=10%>-</td><td align=left  width=45%>" + jo
                        .get("commutation_amt") + "</td></tr>";
                html_txt = html_txt + "<tr><td align=left width=45%>Last four digits of SB Acct</td><td align=center  width=10%>-</td><td align=left  width=45%>" + jo
                        .get("last_four_digit_acc_no") + "</td></tr>";
                html_txt = html_txt + "<tr><td align=left width=45%>Name of Family Pensioner</td><td align=center  width=10%>-</td><td align=left  width=45%>" + jo
                        .get("name_family_pen") + "</td></tr>";

            }
            html_txt = html_txt + "</table></div>";
            wv.loadDataWithBaseURL(null, html_txt, "text/html", "utf-8", null);

            wv.loadData(html_txt, "text/html; charset=utf-8", "utf-8");
        }
        if (TextUtil.isEqual(type, "fund")) {
            String html_txt = "<div align=center><h2><u>AFPP Fund Withdrawal</u></h2><table border=1 width=90%><tr><td align=center><font color='#940000'><b>Ser</b></font></td><td  align=center><font color='#940000'><b>Dt RO Recd</b></font></td><td  align=center><font color='#940000'><b>Dt PAO Recd</b></font></td><td  align=center><font color='#940000'><b>Dt PAO Passed</b></font></td><td  align=center><font color='#940000'><b>Amt Passed</b></font></td></tr>";
            JSONArray ja = new JSONArray(value);
            JSONObject jo = null;

            for (int i = 0; i < ja.length(); i++) {
                jo = ja.getJSONObject(i);


                html_txt = html_txt + "<tr><td align=center>" + (i + 1) + "</td><td>" + (jo.get("fd_ro_recd_dt")
                        .toString()) + "</td><td>" + (jo.get("fd_pao_recd_dt")
                        .toString()) + "</td><td>" + (jo.get("fd_pao_passed_dt")
                        .toString()) + "</td><td>" + jo.get("fd_amt_passed") + "</td></tr>";
            }
            html_txt = html_txt + "</table></div>";
            wv.loadDataWithBaseURL(null, html_txt, "text/html", "utf-8", null);

            wv.loadData(html_txt, "text/html; charset=utf-8", "utf-8");
        }


        if (TextUtil.isEqual(type, "obsn")) {
            String html_txt = "<div align=center><h2><u>OBSNS ON PAY AND ALLOWANCES</u></h2><table border=1 width=90%><tr><td align=center><font color='#940000'><b>Ser</b></font></td><td  align=center><font color='#940000'><b>Item</b></font></td><td  align=center><font color='#940000'><b>Amt Claimed</b></font></td><td  align=center><font color='#940000'><b>Dt Recd in RO</b></font></td><td  align=center><font color='#940000'><b>Dt Recd in PAO(OR)</b></font></td><td  align=center><font color='#940000'><b>Status in PAO(OR)</b></font></td><td  align=center><font color='#940000'><b>Adjusted in MPS</b></font></td></tr>";
            JSONArray ja = new JSONArray(value);
            JSONObject jo = null;
            for (int i = 0; i < ja.length(); i++) {
                jo = ja.getJSONObject(i);
                html_txt = html_txt + "<tr><td align=center>" + (i + 1) + "</td><td>" + jo.get("fd_desc") + "</td><td>" + jo
                        .get("fd_amt_claimed") + "</td><td>" + (jo.get("fd_dt_recd_ro")
                        .toString()) + "</td><td>" + (jo.get("fd_dt_recd_paor")
                        .toString()) + "</td><td>" + jo.get("fd_status_paor") + "</td><td>" + jo.get("fd_adjusted_mps") + "</td></tr>";
            }
            html_txt = html_txt + "</table></div>";
            wv.loadDataWithBaseURL(null, html_txt, "text/html", "utf-8", null);
            wv.loadData(html_txt, "text/html; charset=utf-8", "utf-8");
        }

        if (TextUtil.isEqual(type, "lodge_view")) {
            String html_txt = "<div align=center><h2><u>View Observation Status</u></h2><table border=1 width=90%><tr><td align=center><font color='#940000'><b>Ser</b></font></td><td  align=center><font color='#940000'><b>ID</b></font></td><td  align=center><font color='#940000'><b>Subject</b></font></td><td  align=center><font color='#940000'><b>Observation</b></font></td><td  align=center><font color='#940000'><b>Observation dt</b></font></td><td  align=center><font color='#940000'><b>Reply</b></font></td><td  align=center><font color='#940000'><b>Reply dt</b></font></td></tr>";
            JSONArray ja = new JSONArray(value);
            JSONObject jo = null;
            for (int i = 0; i < ja.length(); i++) {
                jo = ja.getJSONObject(i);
                html_txt = html_txt + "<tr><td align=center>" + (i + 1) + "</td><td>" + jo.get("fd_id") + "</td><td>" + jo
                        .get("fd_subject") + "</td><td>" + jo.get("fd_grievance") + "</td><td>" + (jo
                        .get("fd_date_time")
                        .toString()) + "</td><td>" + jo.get("fd_reply") + "</td><td>" + (jo.get("fd_date_time_r")
                        .toString()) + "</td></tr>";
            }
            html_txt = html_txt + "</table></div>";

            wv.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {

                    //   Toast.makeText(MainActivity.this,"Long Click Disabled",Toast.LENGTH_SHORT).show();

                    return true;
                }
            });
            wv.setLongClickable(false);

            wv.setHapticFeedbackEnabled(false);
            wv.loadDataWithBaseURL(null, html_txt, "text/html", "utf-8", null);
            wv.loadData(html_txt, "text/html; charset=utf-8", "utf-8");
        }


    }
}
