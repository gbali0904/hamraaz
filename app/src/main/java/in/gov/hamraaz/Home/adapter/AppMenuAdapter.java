package in.gov.hamraaz.Home.adapter;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.gov.hamraaz.App;
import in.gov.hamraaz.Home.MainActivity;
import in.gov.hamraaz.Home.ModelForAppMenu;
import in.gov.hamraaz.R;

public class AppMenuAdapter extends RecyclerView.Adapter {
    private final MainActivity mainActivity;
    private final boolean status;

    private List<ModelForAppMenu> modelForAppMenus;
    private OnItemClicked onClick;
    private OnNavItemClicked onNavClick;


    public AppMenuAdapter(MainActivity mainActivity, boolean status) {
        this.mainActivity = mainActivity;
        this.status = status;
        modelForAppMenus = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        View v;
        if (status) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.app_nav_adapter, parent, false);
            holder = new ItemNavViewHolder(v);
        } else {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.app_menu_adapter, parent, false);
            holder = new ItemViewHolder(v);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (status) {
            ((ItemNavViewHolder) holder).bind(position);
        } else {
            ((ItemViewHolder) holder).bind(position);
        }
    }

    @Override
    public int getItemCount() {
        return modelForAppMenus.size();
    }

    public void setData(List<ModelForAppMenu> modelForAppMenus) {
        this.modelForAppMenus = modelForAppMenus;
        notifyDataSetChanged();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image_app_menu)
        ImageView imageAppMenu;
        @BindView(R.id.txt_app_name)
        TextView txtAppName;
        @BindView(R.id.btnMainOrderDetail)
        CardView btnMainOrderDetail;
        @BindView(R.id.lay)
        LinearLayout lay;

        public ItemViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        public void bind(final int position) {
            final ModelForAppMenu modelForAppMenu = modelForAppMenus.get(position);
            String iconPath = modelForAppMenu.getIcon();
            Drawable drawableFromId = App.getImage(mainActivity, iconPath.toLowerCase());
            imageAppMenu.setImageDrawable(drawableFromId);
            txtAppName.setText("" + modelForAppMenu.getApp_Menu_Name());
            btnMainOrderDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClick.onItemClick(Integer.parseInt(String.valueOf(modelForAppMenu.getApp_Menu_Id())));
                }
            });
        }
    }

    public class ItemNavViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image_app_menu)
        ImageView imageAppMenu;
        @BindView(R.id.txt_app_name)
        TextView txtAppName;
        @BindView(R.id.lay)
        LinearLayout lay;

        public ItemNavViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        public void bind(int position) {
            final ModelForAppMenu modelForAppMenu = modelForAppMenus.get(position);
            String iconPath = modelForAppMenu.getIcon();
            Drawable drawableFromId = App.getImage(mainActivity, iconPath.toLowerCase());
            imageAppMenu.setImageDrawable(drawableFromId);
            txtAppName.setText("" + modelForAppMenu.getApp_Menu_Name());
            lay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onNavClick.onNavItemClick(Integer.parseInt(String.valueOf(modelForAppMenu.getApp_Menu_Id())));
                }
            });
        }
    }


    //make interface like this
    public interface OnItemClicked {
        void onItemClick(int position);
    }

    public void setOnClick(OnItemClicked onClick) {
        this.onClick = onClick;
    }
    //make interface like this
    public interface OnNavItemClicked {
        void onNavItemClick(int position);
    }

    public void setOnNavClick(OnNavItemClicked onClick) {
        this.onNavClick = onClick;
    }
}
