package in.gov.hamraaz.Home.model;

public class ModelForForm {

    /**
     * form16pdf : {"code":1,"error":true,"msg":"Successfully Fetched","PayPdf":"/HtIY9UyrNHG/MPS_PDF_ZIP/FORM16_PDF_Dir_/2019/D85C55311B47103939B060B03971D3C6C6DDAE2F_2019.pdf"}
     */

    private Form16pdfBean form16pdf;

    public Form16pdfBean getForm16pdf() {
        return form16pdf;
    }

    public void setForm16pdf(Form16pdfBean form16pdf) {
        this.form16pdf = form16pdf;
    }

    public static class Form16pdfBean {
        /**
         * code : 1
         * error : true
         * msg : Successfully Fetched
         * PayPdf : /HtIY9UyrNHG/MPS_PDF_ZIP/FORM16_PDF_Dir_/2019/D85C55311B47103939B060B03971D3C6C6DDAE2F_2019.pdf
         */

        private int code;
        private boolean error;
        private String msg;
        private String PayPdf;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public boolean isError() {
            return error;
        }

        public void setError(boolean error) {
            this.error = error;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public String getPayPdf() {
            return PayPdf;
        }

        public void setPayPdf(String PayPdf) {
            this.PayPdf = PayPdf;
        }
    }
}
