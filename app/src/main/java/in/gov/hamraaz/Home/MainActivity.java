package in.gov.hamraaz.Home;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import in.gov.hamraaz.Utils.*;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.gov.hamraaz.App;
import in.gov.hamraaz.Grievance.GrievanceActivity;
import in.gov.hamraaz.Grievance.ShowGrievanceActivity;
import in.gov.hamraaz.Home.adapter.AppMenuAdapter;
import in.gov.hamraaz.Home.model.ModelForForm;
import in.gov.hamraaz.Home.model.ModelForPaySlip;
import in.gov.hamraaz.Notification.NotificationActivity;
import in.gov.hamraaz.Notification.PolicyImportantInfoActivity;
import in.gov.hamraaz.R;
import in.gov.hamraaz.changepassword.ChangePasswordActivity;
import in.gov.hamraaz.contactus.ContactUs;
import in.gov.hamraaz.family.FamilyFragment;
import in.gov.hamraaz.fragment.MainActivityFragment;
import in.gov.hamraaz.fundwithdraw.FundWithdrawalFragment;
import in.gov.hamraaz.leave.LeaveFragment;
import in.gov.hamraaz.login.LoginActivity;
import in.gov.hamraaz.obsns.ObsnsFragment;
import in.gov.hamraaz.pao_pto_status.PaoPtoStatusFragment;
import in.gov.hamraaz.paydetail.PayDetailFragment;
import in.gov.hamraaz.postrequestview.PostRequestViewFragment;
import in.gov.hamraaz.ppodetail.PPODetail;
import in.gov.hamraaz.profile.MyProfileFragment;
import in.gov.hamraaz.pto.PartOrderFragment;
import in.gov.hamraaz.servicevoter.ServiceVoterFragment;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    public static final String KEY_PAN_HASH = "pan_hash";
    private static final int PERMISSION_REQUEST_CODE = 101;
    private final long interval = 1 * 1000;
    String panHash = "";
    String public_path;
    Boolean Check_rept = true;
    ProgressDialog progress;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.wv_main)
    WebView wvMain;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.textView)
    TextView textView;

    @BindView(R.id.navigation_drawer_list)
    RecyclerView navigationDrawerList;
    private FirebaseAnalytics mFirebaseAnalytics;
    private String succ_resp;
    private AppMenuAdapter appMenuAdapter;
    private AppMenuAdapter appMenuAdapter1;
    private String url = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        public_path = saveToPreference("pub");
        setSupportActionBar(toolbar);
        setupUi();
    }

    @Override
    protected void onResume() {
        super.onResume();
        panHash = App.getInstance().getPreference().getString(TAGs.KEY_PAN_HASH, "");
        Log.e("this", "pan hash :::" + panHash);
        succ_resp = App.getInstance().getPreference().getString(TAGs.SUCCESS_RESPONSE, "");
        if (!Util.checkInternet(MainActivity.this)) {
            url = "https://10.0.0.161/s_Pan_65_ver/s_App_Menu.aspx";
        } else {
            url = RemoteConfigManager.getAppMenuUrl();
        }
        appMenuApiCall(url, panHash);
    }

    private void setupUi() {

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAppInfo();
            }
        });
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();
        //navigationView.setNavigationItemSelectedListener(this);
        FireBaseEvent("9", "item_Main", "content_Main");

        appMenuAdapter = new AppMenuAdapter(this, false);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
        recyclerview.setLayoutManager(gridLayoutManager);
        recyclerview.setAdapter(appMenuAdapter);
        appMenuAdapter.setOnClick(new AppMenuAdapter.OnItemClicked() {
            @Override
            public void onItemClick(int position) {
                appMenuItemClick(position);
            }
        });

        appMenuAdapter1 = new AppMenuAdapter(this, true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        navigationDrawerList.setLayoutManager(linearLayoutManager);
        navigationDrawerList.setAdapter(appMenuAdapter1);
        appMenuAdapter1.setOnNavClick(new AppMenuAdapter.OnNavItemClicked() {
            @Override
            public void onNavItemClick(int position) {
                appMenuItemClick(position);
            }
        });

    }

    private void appMenuItemClick(int position) {
        switch (position) {
            case 1:
                Intent toGrievanceActivity = new Intent(MainActivity.this, GrievanceActivity.class);
                toGrievanceActivity.putExtra(GrievanceActivity.KEY_PAN_HASH, panHash);
                toGrievanceActivity.putExtra(Constant.STATUS, "0");
                startActivity(toGrievanceActivity);
                break;
            case 2:
                Intent toShowGrievanceActivity = new Intent(MainActivity.this, ShowGrievanceActivity.class);
                toShowGrievanceActivity.putExtra(GrievanceActivity.KEY_PAN_HASH, panHash);
                startActivity(toShowGrievanceActivity);
                break;
            case 3:
                PartOrderFragment partOrderFragment = new PartOrderFragment();
                Bundle bundle7 = new Bundle();
                bundle7.putString(Constant.KEY_PAN_HASH, panHash);
                partOrderFragment.setArguments(bundle7);
                MainActivityFragment.instansiate(partOrderFragment, MainActivity.this, PartOrderFragment.TAG, getResources().getString(R.string.nav_orderDetails));
                break;
            case 4:
                FamilyFragment familyFragment = new FamilyFragment();
                Bundle bundle3 = new Bundle();
                bundle3.putString(Constant.KEY_PAN_HASH, panHash);
                familyFragment.setArguments(bundle3);
                MainActivityFragment.instansiate(familyFragment, MainActivity.this, FamilyFragment.TAG, "Family Detail");
                break;
            case 5:
                PayDetailFragment payDetailFragment = new PayDetailFragment();
                Bundle bundle2 = new Bundle();
                bundle2.putString(Constant.KEY_PAN_HASH, panHash);
                payDetailFragment.setArguments(bundle2);
                MainActivityFragment.instansiate(payDetailFragment, MainActivity.this, PayDetailFragment.TAG, getResources().getString(R.string.nav_payDetails));
                break;
            case 6:
                PPODetail ppoDetail = new PPODetail();
                Bundle bundle8 = new Bundle();
                bundle8.putString(Constant.KEY_PAN_HASH, panHash);
                ppoDetail.setArguments(bundle8);
                MainActivityFragment.instansiate(ppoDetail, MainActivity.this, PPODetail.TAG, getResources()
                        .getString(R.string.nav_ppo_detials));
                break;
            case 7:
                FundWithdrawalFragment fundWithdrawal = new FundWithdrawalFragment();
                Bundle bundle6 = new Bundle();
                bundle6.putString(Constant.KEY_PAN_HASH, panHash);
                fundWithdrawal.setArguments(bundle6);
                MainActivityFragment.instansiate(fundWithdrawal, MainActivity.this, FundWithdrawalFragment.TAG, getResources()
                        .getString(R.string.nav_fund_withdrawal));
                break;
            case 8:
                LeaveFragment leaveFragment = new LeaveFragment();
                Bundle bundle = new Bundle();
                bundle.putString(Constant.KEY_PAN_HASH, panHash);
                leaveFragment.setArguments(bundle);
                MainActivityFragment.instansiate(leaveFragment, MainActivity.this, LeaveFragment.TAG, getResources()
                        .getString(R.string.nav_leave_enhancment));
                break;
            case 9:
                ServiceVoterFragment serviceVoterFragment = new ServiceVoterFragment();
                Bundle bundle9 = new Bundle();
                bundle9.putString(Constant.KEY_PAN_HASH, panHash);
                serviceVoterFragment.setArguments(bundle9);
                MainActivityFragment.instansiate(serviceVoterFragment, MainActivity.this, ServiceVoterFragment.TAG, getResources()
                        .getString(R.string.service_voter));

                break;
            case 10:
                NotificationActivity.showNotifiation(MainActivity.this, panHash);
                break;
            case 11:
                startActivity(new Intent(MainActivity.this, PolicyImportantInfoActivity.class)
                        .putExtra(Constant.KEY_PAN_HASH, panHash));
                break;
            case 12:
                Intent intent = new Intent(MainActivity.this, ChangePasswordActivity.class);
                intent.putExtra(ChangePasswordActivity.KEY_PAN_HASH, panHash);
                startActivity(intent);
                break;

            case 13:
                MyProfileFragment myProfileFragment = new MyProfileFragment();
                Bundle bundle5 = new Bundle();
                bundle5.putString(Constant.KEY_PAN_HASH, panHash);
                myProfileFragment.setArguments(bundle5);
                MainActivityFragment.instansiate(myProfileFragment, MainActivity.this, MyProfileFragment.TAG, getResources()
                        .getString(R.string.my_profile));
                break;

            case 14:
                PostRequestViewFragment postRequestViewFragment = new PostRequestViewFragment();
                Bundle bundle11 = new Bundle();
                bundle11.putString(Constant.KEY_PAN_HASH, panHash);
                postRequestViewFragment.setArguments(bundle11);
                MainActivityFragment.instansiate(postRequestViewFragment, MainActivity.this, PostRequestViewFragment.TAG, getResources()
                        .getString(R.string.post_view));

                break;
            case 15:
                ObsnsFragment obsnsFragment = new ObsnsFragment();
                Bundle bundle4 = new Bundle();
                bundle4.putString(Constant.KEY_PAN_HASH, panHash);
                obsnsFragment.setArguments(bundle4);
                MainActivityFragment.instansiate(obsnsFragment, MainActivity.this, ObsnsFragment.TAG, getResources()
                        .getString(R.string.nav_obsns));
                break;
            case 16:
                Intent toGrievanceActivity1 = new Intent(MainActivity.this, GrievanceActivity.class);
                toGrievanceActivity1.putExtra(GrievanceActivity.KEY_PAN_HASH, panHash);
                toGrievanceActivity1.putExtra(Constant.STATUS, "1");
                startActivity(toGrievanceActivity1);
                break;
            case 17:
                ContactUs contactUs = new ContactUs();
                Bundle bundle17 = new Bundle();
                bundle17.putString(Constant.KEY_PAN_HASH, panHash);
                contactUs.setArguments(bundle17);
                MainActivityFragment.instansiate(contactUs, MainActivity.this, ContactUs.TAG, getResources()
                        .getString(R.string.contact_us));

                break;
            case 18:
                Intent toGrievanceActivity2 = new Intent(MainActivity.this, GrievanceActivity.class);
                toGrievanceActivity2.putExtra(GrievanceActivity.KEY_PAN_HASH, panHash);
                toGrievanceActivity2.putExtra(Constant.STATUS, "2");
                startActivity(toGrievanceActivity2);

                break;
            case 19:
                PaoPtoStatusFragment paoPtoStatusFragment = new PaoPtoStatusFragment();
                Bundle bundl = new Bundle();
                bundl.putString(Constant.KEY_PAN_HASH, panHash);
                paoPtoStatusFragment.setArguments(bundl);
                MainActivityFragment.instansiate(paoPtoStatusFragment, MainActivity.this, ObsnsFragment.TAG, getResources()
                        .getString(R.string.nav_obsns));
                break;
            case 200:
                showLogoutDialog();
                break;

        }
    }

    private void appMenuApiCall(String appMenuUrl, final String panHash) {
        final ProgressDialog progress = DialogUtil.createProgressDialog(MainActivity.this, "Loading", "Please wait");
        progress.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, appMenuUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.length() > 0) {
                    try {
                        progress.dismiss();
                        Type collectionType = new TypeToken<List<ModelForAppMenu>>() {
                        }.getType();
                        List<ModelForAppMenu> modelForAppMenus = new Gson().fromJson(response, collectionType);
                        List<ModelForAppMenu> modelForNAVAppMenus = new Gson().fromJson(response, collectionType);
                        int size = modelForAppMenus.size();
                        size = size++;
                        ModelForAppMenu modelForAppMenu = new ModelForAppMenu();
                        modelForAppMenu.setApp_Menu_Id(200);
                        modelForAppMenu.setApp_Menu_Name("Logout");
                        modelForAppMenu.setMenu_status("A");
                        modelForAppMenu.setIcon("logout");
                        modelForNAVAppMenus.add(size, modelForAppMenu);
                        List<ModelForAppMenu> appMenus = new ArrayList<>();
                        List<ModelForAppMenu> appNAVMenus = new ArrayList<>();
                        if (succ_resp.equalsIgnoreCase("_Topic_Id_23")) {
                            for (int i = 0; i < modelForAppMenus.size(); i++) {
                                appMenus.add(modelForAppMenus.get(i));
                            }
                            for (int i = 0; i < modelForNAVAppMenus.size(); i++) {
                                appNAVMenus.add(modelForNAVAppMenus.get(i));
                            }
                        } else {
                            for (int i = 0; i < modelForAppMenus.size(); i++) {
                                if (modelForAppMenus.get(i).getApp_Menu_Id() == 14) {
                                } else {
                                    appMenus.add(modelForAppMenus.get(i));
                                }
                            }
                            for (int i = 0; i < modelForNAVAppMenus.size(); i++) {
                                if (modelForNAVAppMenus.get(i).getApp_Menu_Id() == 14) {
                                } else {
                                    appNAVMenus.add(modelForNAVAppMenus.get(i));
                                }
                            }
                        }

                        Log.e("this", "appMenus::" + new Gson().toJson(appMenus));


                        appMenuAdapter.setData(appMenus);
                        appMenuAdapter1.setData(appNAVMenus);
                        navigationView.invalidate();
                    } catch (Exception e) {
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("schemeFrame", panHash);
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        addSubMenuInPaySlip(menu);
        addSubMenuInForm(menu);
        return true;
    }

    private void addSubMenuInForm(final Menu menu) {
        final String url = RemoteConfigManager.getMenuFormUrl();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.length() > 0) {

                    try {

                        JSONArray ja = new JSONArray(response);
                        JSONObject jo = null;
                        String title = "";
                        String id = "";
                        for (int i = 0; i < ja.length(); i++) {
                            jo = ja.getJSONObject(i);
                            title = "" + jo.get("Year");
                            id = "" + jo.get("col_id");
                            menu.findItem(R.id.action_download)
                                    .getSubMenu()
                                    .findItem(R.id.action_form)
                                    .getSubMenu()
                                    .add(Menu.NONE, 100, Menu.NONE, title);
                        }

                    } catch (Exception e) {
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("schemeFrame", panHash);
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void addSubMenuInPaySlip(final Menu menu) {
        final String url = RemoteConfigManager.getMenuMpsUrl();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.length() > 9) {
                    try {
                        JSONArray ja = new JSONArray(response);
                        JSONObject jo = null;
                        for (int i = 0; i < ja.length(); i++) {
                            jo = ja.getJSONObject(i);
                            String title = "";
                            if (TextUtil.isEqual(jo.get("Month").toString(), ("01")))
                                title = "Jan";
                            if (TextUtil.isEqual(jo.get("Month").toString(), ("02")))
                                title = "Feb";
                            if (TextUtil.isEqual(jo.get("Month").toString(), ("03")))
                                title = "Mar";
                            if (TextUtil.isEqual(jo.get("Month").toString(), ("04")))
                                title = "Apr";
                            if (TextUtil.isEqual(jo.get("Month").toString(), ("05")))
                                title = "May";
                            if (TextUtil.isEqual(jo.get("Month").toString(), ("06")))
                                title = "Jun";
                            if (TextUtil.isEqual(jo.get("Month").toString(), ("07")))
                                title = "Jul";
                            if (TextUtil.isEqual(jo.get("Month").toString(), ("08")))
                                title = "Aug";
                            if (TextUtil.isEqual(jo.get("Month").toString(), ("09")))
                                title = "Sep";
                            if (TextUtil.isEqual(jo.get("Month").toString(), ("10")))
                                title = "Oct";
                            if (TextUtil.isEqual(jo.get("Month").toString(), ("11")))
                                title = "Nov";
                            if (TextUtil.isEqual(jo.get("Month").toString(), ("12")))
                                title = "Dec";


                            title = title + " " + jo.get("Year");


                            MenuItem menuItem = menu.findItem(R.id.action_download)
                                    .getSubMenu()
                                    .findItem(R.id.action_dn_mps)
                                    .getSubMenu()
                                    .add(Menu.NONE, 1, Menu.NONE, title);


                        }

                    } catch (Exception e) {
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //                progress.cancel();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("schemeFrame", panHash);
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();
        String monthString = item.getTitle().toString().substring(0, 3);
        String yearString = item.getTitle().toString().substring(4);
        switch (monthString.toLowerCase()) {
            case "jan":
                pdfServerCall("01", yearString);
                break;
            case "feb":
                pdfServerCall("02", yearString);
                break;
            case "mar":
                pdfServerCall("03", yearString);
                break;
            case "apr":
                pdfServerCall("04", yearString);
                break;
            case "may":
                pdfServerCall("05", yearString);
                break;
            case "jun":
                pdfServerCall("06", yearString);
                break;
            case "jul":
                pdfServerCall("07", yearString);
                break;
            case "aug":
                pdfServerCall("08", yearString);
                break;
            case "sep":
                pdfServerCall("09", yearString);
                break;
            case "oct":
                pdfServerCall("10", yearString);
                break;
            case "nov":
                pdfServerCall("11", yearString);
                break;
            case "dec":
                pdfServerCall("12", yearString);
                break;
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_info) {
            showAppInfo();
            return true;
        }
        if (item.getItemId() == 100) {
            checkPermission(item);

        }
        if (id == R.id.action_contact) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:+919560641424"));
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    public void checkPermission(MenuItem item) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkPermissionCheck()) {
                // Code for above or equal 23 API Oriented Device
                // Your Permission granted already .Do next code
                downloadForm16(item.getTitle().toString());
            } else {
                requestPermission(); // Code for permission
            }
        } else {

            downloadForm16(item.getTitle().toString());
        }

    }

    private boolean checkPermissionCheck() {
        int result = ContextCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(MainActivity.this, "Write External Storage permission allows us to do store images. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("value", "Permission Granted, Now you can use local drive .");
                } else {
                    Log.e("value", "Permission Denied, You cannot use local drive .");
                }
                break;
        }
    }


    private void downloadForm16(final String title) {
        final ProgressDialog progressDialog = DialogUtil.createProgressDialog(MainActivity.this, "Loading", "Please wait...");
        progressDialog.show();
        String url = RemoteConfigManager.getFormDownloadUrl();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                ModelForForm modelForPaySlip = new Gson().fromJson(response, ModelForForm.class);
                ModelForForm.Form16pdfBean paypdf = modelForPaySlip.getForm16pdf();
                if (paypdf.getCode() == 1 && !paypdf.getPayPdf().equals("")) {
                    new DownloadTask(MainActivity.this, RemoteConfigManager.getFromDownloadBaseUrl(), paypdf.getPayPdf().replaceAll(" ", ""), "Form 16");
                } else {
                    DialogUtil.createAlertDialog(MainActivity.this, "Error", paypdf.getMsg(), "Okay")
                            .show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("year", title);// year
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + panHash);
                headers.put("timestamp", Util.getTimestamp());
                return headers;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void showLogoutDialog() {
        DialogUtil.createAlertDialog(MainActivity.this, "Logout", "Are you sure ?", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Clear Session
                App.getInstance().getPreferenceEditor().putBoolean(TAGs.LOGIN_STATUS, false).commit();
                App.getInstance().getPreferenceEditor().putString(TAGs.KEY_PAN_HASH, "").commit();

                Intent
                        intent = new Intent(MainActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        }, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //click No
                dialog.dismiss();
            }
        }).show();
    }

    private void showAppInfo() {
        DialogUtil.createAlertDialog(MainActivity.this, "© 2017 By Indian Army", "All rights reserved. No part of this software may be reproduced, distributed, or transmitted in any form or by any means, including sharing other than via Goverment Mobile Seva  Store without the prior written permission from IA.", "Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).show();
    }

    private void open_mps_pdf(String baseurl, String pdfUrl) {

        String url = baseurl + pdfUrl;
        Uri uri = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);

    }

    private void pdfServerCall(final String month, final String year) {
        final ProgressDialog progressDialog = DialogUtil.createProgressDialog(MainActivity.this, "Loading", "Please wait...");
        progressDialog.show();
        String url = RemoteConfigManager.getPDFDownloadUrl();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();

                ModelForPaySlip modelForPaySlip = new Gson().fromJson(response, ModelForPaySlip.class);
                ModelForPaySlip.PaypdfBean paypdf = modelForPaySlip.getPaypdf();
                if (paypdf.getCode() == 1 && !paypdf.getPayPdf().equals("")) {
                    open_mps_pdf(RemoteConfigManager.getFromDownloadBaseUrl(), paypdf.getPayPdf());
                } else {
                    DialogUtil.createAlertDialog(MainActivity.this, "Error", paypdf.getMsg(), "Okay")
                            .show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("month", month);// month
                params.put("year", year);// year
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + panHash);
                headers.put("timestamp", Util.getTimestamp());
                return headers;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    String saveToPreference(String value) {
        SharedPreferences sharedpreferences;
        sharedpreferences = getSharedPreferences("Ammumma", Context.MODE_PRIVATE);
        String savedValue = sharedpreferences.getString(value, "");
        return savedValue;
    }

    private void FireBaseEvent(String item_id, String item_name, String content_type) {
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, item_id);
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, item_name);
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, content_type);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        item.setChecked(true);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


}
