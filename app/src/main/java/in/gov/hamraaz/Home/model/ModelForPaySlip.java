package in.gov.hamraaz.Home.model;

public class ModelForPaySlip {

    /**
     * paypdf : {"code":1,"error":true,"msg":"Successfully Fetched","PayPdf":"/HtIY9UyrNHG/MPS_PDF_ZIP/MPS_PDF_Dir_/08/D85C55311B47103939B060B03971D3C6C6DDAE2F_08_2019.pdf"}
     */

    private PaypdfBean paypdf;

    public PaypdfBean getPaypdf() {
        return paypdf;
    }

    public void setPaypdf(PaypdfBean paypdf) {
        this.paypdf = paypdf;
    }

    public static class PaypdfBean {
        /**
         * code : 1
         * error : true
         * msg : Successfully Fetched
         * PayPdf : /HtIY9UyrNHG/MPS_PDF_ZIP/MPS_PDF_Dir_/08/D85C55311B47103939B060B03971D3C6C6DDAE2F_08_2019.pdf
         */

        private int code;
        private boolean error;
        private String msg;
        private String PayPdf;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public boolean isError() {
            return error;
        }

        public void setError(boolean error) {
            this.error = error;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public String getPayPdf() {
            return PayPdf;
        }

        public void setPayPdf(String PayPdf) {
            this.PayPdf = PayPdf;
        }
    }
}
