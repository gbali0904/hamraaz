package in.gov.hamraaz.Home;

public class ModelForAppMenu {


    /**
     * App_Menu_Id : 10
     * App_Menu_Name : Hidayaten
     * menu_status : A
     * Icon : Notification
     */

    private int App_Menu_Id;
    private String App_Menu_Name;
    private String menu_status;
    private String Icon;

    public int getApp_Menu_Id() {
        return App_Menu_Id;
    }

    public void setApp_Menu_Id(int App_Menu_Id) {
        this.App_Menu_Id = App_Menu_Id;
    }

    public String getApp_Menu_Name() {
        return App_Menu_Name;
    }

    public void setApp_Menu_Name(String App_Menu_Name) {
        this.App_Menu_Name = App_Menu_Name;
    }

    public String getMenu_status() {
        return menu_status;
    }

    public void setMenu_status(String menu_status) {
        this.menu_status = menu_status;
    }

    public String getIcon() {
        return Icon;
    }

    public void setIcon(String Icon) {
        this.Icon = Icon;
    }
}
