package in.gov.hamraaz.paydetail;


import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import in.gov.hamraaz.App;
import in.gov.hamraaz.Home.model.ModelForForm;
import in.gov.hamraaz.Home.model.ModelForPaySlip;
import in.gov.hamraaz.Notification.pdfview.DocumentFragment;
import in.gov.hamraaz.R;
import in.gov.hamraaz.Utils.Constant;
import in.gov.hamraaz.Utils.DialogUtil;
import in.gov.hamraaz.Utils.DownloadTask;
import in.gov.hamraaz.Utils.RemoteConfigManager;
import in.gov.hamraaz.Utils.TAGs;
import in.gov.hamraaz.Utils.TextUtil;
import in.gov.hamraaz.Utils.Util;
import in.gov.hamraaz.fragment.MainActivityFragment;
import in.gov.hamraaz.login.LoginActivity;
import in.gov.hamraaz.paydetail.model.ModelForPayDetail;

/**
 * A simple {@link Fragment} subclass.
 */
public class PayDetailFragment extends Fragment {


    public static final String TAG = PayDetailFragment.class.getSimpleName();

    private static final int PERMISSION_REQUEST_CODE = 101;
    Unbinder unbinder;
    @BindView(R.id.creditonebal)
    TextView creditonebal;
    @BindView(R.id.debitonebal)
    TextView debitonebal;
    @BindView(R.id.bandpay)
    TextView bandpay;
    @BindView(R.id.afppfSubs)
    TextView afppfSubs;
    @BindView(R.id.gradepay)
    TextView gradepay;
    @BindView(R.id.agifdebit)
    TextView agifdebit;
    @BindView(R.id.xpay)
    TextView xpay;
    @BindView(R.id.plidebit)
    TextView plidebit;
    @BindView(R.id.mspay)
    TextView mspay;
    @BindView(R.id.loanAdv)
    TextView loanAdv;
    @BindView(R.id.da)
    TextView da;
    @BindView(R.id.eticketing)
    TextView eticketing;
    @BindView(R.id.pmha)
    TextView pmha;
    @BindView(R.id.imcometax)
    TextView imcometax;
    @BindView(R.id.lra)
    TextView lra;
    @BindView(R.id.famo)
    TextView famo;
    @BindView(R.id.reccuringallw)
    TextView reccuringallw;
    @BindView(R.id.arrears)
    TextView arrears;
    @BindView(R.id.otherallw)
    TextView otherallw;
    @BindView(R.id.credittobank)
    TextView credittobank;
    @BindView(R.id.totalcredit)
    TextView totalcredit;
    @BindView(R.id.totaldebit)
    TextView totaldebit;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.spinnerpayslip)
    Spinner spinnerpayslip;
    @BindView(R.id.buttonViewPaySlip)
    Button buttonViewPaySlip;
    @BindView(R.id.spinnerform)
    Spinner spinnerform;
    @BindView(R.id.buttonViewform)
    Button buttonViewform;
    private String pan_hass;
    private String monthStringSplit = "";
    private String yearString = "";
    private String yearForm = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pay_detail, container, false);
        unbinder = ButterKnife.bind(this, view);
        Bundle bundle = getArguments();
        pan_hass = bundle.getString(Constant.KEY_PAN_HASH);
        //  makeServerCall(RemoteConfigManager.getViewPayDetailsUrl(), pan_hass);
        buttonViewPaySlip.setEnabled(false);
        buttonViewform.setEnabled(false);
        addSubMenuInPaySlip();
        addSubMenuInForm();
        return view;
    }


    private void addSubMenuInForm() {
        final String url = RemoteConfigManager.getMenuFormUrl();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.length() > 0) {

                    try {

                        JSONArray ja = new JSONArray(response);
                        JSONObject jo = null;
                        ArrayList<String> year = new ArrayList<>();
                        year.add("Select Year");
                        for (int i = 0; i < ja.length(); i++) {
                            jo = ja.getJSONObject(i);
                            String titleYear = "" + jo.get("Year");
                            String id = "" + jo.get("col_id");
                            year.add(titleYear);
                        }

                        // Creating adapter for spinner
                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, year);
                        // Drop down layout style - list view with radio button
                        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        // attaching data adapter to spinner
                        spinnerform.setAdapter(dataAdapter);
                        spinnerform.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                if (i == 0) {
                                } else {
                                    yearForm = adapterView.getItemAtPosition(i).toString();
                                    buttonViewform.setEnabled(true);
                                }

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });


                    } catch (Exception e) {
                    }
                }
            }
        }, error -> {
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("schemeFrame", pan_hass);
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }


    private void viewForm16(final String title, boolean type) {
        final ProgressDialog progressDialog = DialogUtil.createProgressDialog(getActivity(), "Loading", "Please wait...");
        progressDialog.show();
        String url = RemoteConfigManager.getFormDownloadUrl();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                ModelForForm modelForPaySlip = new Gson().fromJson(response, ModelForForm.class);
                ModelForForm.Form16pdfBean paypdf = modelForPaySlip.getForm16pdf();
                if (type) {
                    if (paypdf.getCode() == 1 && !paypdf.getPayPdf().equals("")) {
                        // new DownloadTask(getActivity(), RemoteConfigManager.getFromDownloadBaseUrl(), paypdf.getPayPdf().replaceAll(" ", ""));
                        open_mps_pdf(RemoteConfigManager.getFromDownloadBaseUrl(), paypdf.getPayPdf().replaceAll(" ", ""), "Form-16");

                    } else {
                        DialogUtil.createAlertDialog(getActivity(), "Error", paypdf.getMsg(), "Okay")
                                .show();
                    }
                } else {
                    if (paypdf.getCode() == 1 && !paypdf.getPayPdf().equals("")) {
                        new DownloadTask(getActivity(), RemoteConfigManager.getFromDownloadBaseUrl(), paypdf.getPayPdf().replaceAll(" ", ""), "Form 16");
                    } else {
                        DialogUtil.createAlertDialog(getActivity(), "Error", paypdf.getMsg(), "Okay")
                                .show();
                    }
                }


            }
        }, error -> {
            progressDialog.dismiss();

            App.getInstance().getPreferenceEditor().putBoolean(TAGs.LOGIN_STATUS, false).commit();
            App.getInstance().getPreferenceEditor().putString(TAGs.KEY_PAN_HASH, "").commit();
            Intent intent = new Intent(getActivity(), LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            getActivity().finish();
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("year", title);// year
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + pan_hass);
                headers.put("timestamp", Util.getTimestamp());
                return headers;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }


    private void addSubMenuInPaySlip() {
        final String url = RemoteConfigManager.getMenuMpsUrl();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, response -> {
            if (response.length() > 9) {
                try {
                    JSONArray ja = new JSONArray(response);
                    JSONObject jo = null;
                    ArrayList<String> month = new ArrayList<>();
                    month.add("Select Month");
                    for (int i = 0; i < ja.length(); i++) {
                        jo = ja.getJSONObject(i);
                        String title = "";
                        if (TextUtil.isEqual(jo.get("Month").toString(), ("01")))
                            title = "Jan";
                        if (TextUtil.isEqual(jo.get("Month").toString(), ("02")))
                            title = "Feb";
                        if (TextUtil.isEqual(jo.get("Month").toString(), ("03")))
                            title = "Mar";
                        if (TextUtil.isEqual(jo.get("Month").toString(), ("04")))
                            title = "Apr";
                        if (TextUtil.isEqual(jo.get("Month").toString(), ("05")))
                            title = "May";
                        if (TextUtil.isEqual(jo.get("Month").toString(), ("06")))
                            title = "Jun";
                        if (TextUtil.isEqual(jo.get("Month").toString(), ("07")))
                            title = "Jul";
                        if (TextUtil.isEqual(jo.get("Month").toString(), ("08")))
                            title = "Aug";
                        if (TextUtil.isEqual(jo.get("Month").toString(), ("09")))
                            title = "Sep";
                        if (TextUtil.isEqual(jo.get("Month").toString(), ("10")))
                            title = "Oct";
                        if (TextUtil.isEqual(jo.get("Month").toString(), ("11")))
                            title = "Nov";
                        if (TextUtil.isEqual(jo.get("Month").toString(), ("12")))
                            title = "Dec";


                        title = title + " " + jo.get("Year");
                        month.add(title);
                    }
                    // Creating adapter for spinner
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, month);
                    // Drop down layout style - list view with radio button
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    // attaching data adapter to spinner
                    spinnerpayslip.setAdapter(dataAdapter);
                    spinnerpayslip.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (i == 0) {
                            } else {
                                String monthString = adapterView.getItemAtPosition(i).toString();
                                String[] split = monthString.split(" ");
                                monthStringSplit = split[0];
                                yearString = split[1];
                                buttonViewPaySlip.setEnabled(true);
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });


                } catch (Exception e) {
                }
            }
        }, error -> {
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("schemeFrame", pan_hass);
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }


    private void pdfServerCall(final String month, final String year, boolean type) {
        final ProgressDialog progressDialog = DialogUtil.createProgressDialog(getActivity(), "Loading", "Please wait...");
        progressDialog.show();
        String url = RemoteConfigManager.getPDFDownloadUrl();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();

                ModelForPaySlip modelForPaySlip = new Gson().fromJson(response, ModelForPaySlip.class);
                ModelForPaySlip.PaypdfBean paypdf = modelForPaySlip.getPaypdf();

                if (type) {
                    if (paypdf.getCode() == 1 && !paypdf.getPayPdf().equals("")) {
                        open_mps_pdf(RemoteConfigManager.getFromDownloadBaseUrl(), paypdf.getPayPdf(), "Pay Slip");
                    } else {
                        DialogUtil.createAlertDialog(getActivity(), "Error", paypdf.getMsg(), "Okay")
                                .show();
                    }
                } else {
                    if (paypdf.getCode() == 1 && !paypdf.getPayPdf().equals("")) {
                        new DownloadTask(getActivity(), RemoteConfigManager.getFromDownloadBaseUrl(), paypdf.getPayPdf().replaceAll(" ", ""), "Pay Slip");
                    } else {
                        DialogUtil.createAlertDialog(getActivity(), "Error", paypdf.getMsg(), "Okay")
                                .show();
                    }

                }
            }
        }, error -> {
            progressDialog.dismiss();

            App.getInstance().getPreferenceEditor().putBoolean(TAGs.LOGIN_STATUS, false).commit();
            App.getInstance().getPreferenceEditor().putString(TAGs.KEY_PAN_HASH, "").commit();
            Intent intent = new Intent(getActivity(), LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            getActivity().finish();

        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("month", month);// month
                params.put("year", year);// year
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + pan_hass);
                headers.put("timestamp", Util.getTimestamp());
                return headers;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }


    private void open_mps_pdf(String baseurl, String pdfUrl, String title) {
  /*      String url = baseurl + pdfUrl;
        Uri uri = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);*/
        String url = baseurl + pdfUrl;
        DocumentFragment documentFragment = new DocumentFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constant.PDF_URL, url);
        documentFragment.setArguments(bundle);
        MainActivityFragment.instansiate(documentFragment, getActivity(), DocumentFragment.TAG, title);


    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.buttonViewPaySlip, R.id.buttonViewform, R.id.buttonDownloadPaySlip, R.id.buttonDownloadform})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonViewPaySlip:
                callPaySlipPDF(true);
                break;
            case R.id.buttonViewform:
                viewForm16(yearForm, true);
                break;
            case R.id.buttonDownloadPaySlip:
                if (Build.VERSION.SDK_INT >= 23) {
                    if (checkPermissionCheck()) {
                        // Code for above or equal 23 API Oriented Device
                        // Your Permission granted already .Do next code
                        callPaySlipPDF(false);
                    } else {
                        requestPermission(); // Code for permission
                    }
                } else {
                    callPaySlipPDF(false);
                }
                break;
            case R.id.buttonDownloadform:
                if (Build.VERSION.SDK_INT >= 23) {
                    if (checkPermissionCheck()) {
                        // Code for above or equal 23 API Oriented Device
                        // Your Permission granted already .Do next code
                        viewForm16(yearForm, false);
                    } else {
                        requestPermission(); // Code for permission
                    }
                } else {
                    viewForm16(yearForm, false);
                }
                break;
        }
    }


    public void callPaySlipPDF(Boolean type) {
        switch (monthStringSplit.toLowerCase()) {
            case "jan":
                pdfServerCall("01", yearString, type);
                break;
            case "feb":
                pdfServerCall("02", yearString, type);
                break;
            case "mar":
                pdfServerCall("03", yearString, type);
                break;
            case "apr":
                pdfServerCall("04", yearString, type);
                break;
            case "may":
                pdfServerCall("05", yearString, type);
                break;
            case "jun":
                pdfServerCall("06", yearString, type);
                break;
            case "jul":
                pdfServerCall("07", yearString, type);
                break;
            case "aug":
                pdfServerCall("08", yearString, type);
                break;
            case "sep":
                pdfServerCall("09", yearString, type);
                break;
            case "oct":
                pdfServerCall("10", yearString, type);
                break;
            case "nov":
                pdfServerCall("11", yearString, type);
                break;
            case "dec":
                pdfServerCall("12", yearString, type);
                break;
        }
    }


    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(getActivity(), "Write External Storage permission allows us to do store images. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("value", "Permission Granted, Now you can use local drive .");
                } else {
                    Log.e("value", "Permission Denied, You cannot use local drive .");
                }
                break;
        }
    }

    private boolean checkPermissionCheck() {
        int result = ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }


}
