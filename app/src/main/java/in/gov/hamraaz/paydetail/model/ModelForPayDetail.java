package in.gov.hamraaz.paydetail.model;

import com.google.gson.annotations.SerializedName;

public class ModelForPayDetail {

    /**
     * Acqitance_rolls : 0
     * AFPP_BBF : 47227
     * AFPPF_refund : 0
     * AFPPF_subscription : 3000
     * AGIF : 2500
     * Amt_credit_to_bank : 40789
     * Amt_in_words : 0
     * Arr_Rec_pay_alwc : 0
     * Band_pay : 33300
     * Birth_dt : 28062005
     * Bonus_on_CR_bal : 0
     * Cl_pay : 450
     * CLOSING_BAL : 50227
     * Credit_Bal_Released : 0
     * DA : 5052
     * E_Ticketing : 0
     * FAMO : 0
     * fd_id : 0
     * fd_noti_dt : 0
     * GP_X_pay : 0
     * Grade_pay : 0
     * Income_tax_EC : 0
     * Loan_advance : 0
     * LRA : 597
     * Month : 6
     * MS_pay : 5200
     * Op_balance : 0
     * Op_balance_debit : 0
     * Other_adj : 0
     * Other_Adj_Debit : 0
     * PLI : 0
     * PMHA : 90
     * Recurr_alwc : 0
     * RO_pay_alwc : 0
     * Status : 0
     * SUBSCRIPTION : 3000
     * p_hash : C43CD813025D2734EEE7684B9FD2789AE2BB7396
     * Tolal_credit : 48289
     * Total_Debit : 48289
     * TPTL : 3600
     * WITHDRAWAL : 0
     * Year : 2019
     * PAN_NO : AJPPV6330C
     * MONTH_ENDING : 06/2019
     */

    @SerializedName("Acqitance_rolls")
    private int Acqitance_rolls;


    @SerializedName("AFPP_BBF")
    private int AFPP_BBF;


    @SerializedName("AFPPF_refund")
    private int AFPPF_refund;


    @SerializedName("AFPPF_subscription")
    private int AFPPF_subscription;


    @SerializedName("AGIF")
    private int AGIF;


    @SerializedName("Amt_credit_to_bank")
    private int Amt_credit_to_bank;


    @SerializedName("Amt_in_words")
    private int Amt_in_words;


    @SerializedName("Arr_Rec_pay_alwc")
    private int Arr_Rec_pay_alwc;


    @SerializedName("Band_pay")
    private int Band_pay;


    @SerializedName("Birth_dt")
    private String Birth_dt;


    @SerializedName("Bonus_on_CR_bal")
    private int Bonus_on_CR_bal;


    @SerializedName("Cl_pay")
    private int Cl_pay;


    @SerializedName("CLOSING_BAL")
    private int CLOSING_BAL;


    @SerializedName("Credit_Bal_Released")
    private int Credit_Bal_Released;


    @SerializedName("DA")
    private int DA;


    @SerializedName("E_Ticketing")
    private int E_Ticketing;


    @SerializedName("FAMO")
    private int FAMO;


    @SerializedName("fd_id")
    private int fd_id;


    @SerializedName("fd_noti_dt")
    private int fd_noti_dt;


    @SerializedName("GP_X_pay")
    private int GP_X_pay;


    @SerializedName("Grade_pay")
    private int Grade_pay;


    @SerializedName("Income_tax_EC")
    private int Income_tax_EC;


    @SerializedName("Loan_advance")
    private int Loan_advance;


    @SerializedName("LRA")
    private int LRA;


    @SerializedName("Month")
    private int Month;


    @SerializedName("MS_pay")
    private int MS_pay;


    @SerializedName("Op_balance")
    private int Op_balance;


    @SerializedName("Op_balance_debit")
    private int Op_balance_debit;


    @SerializedName("Other_adj")
    private int Other_adj;


    @SerializedName("Other_Adj_Debit")
    private int Other_Adj_Debit;


    @SerializedName("PLI")
    private int PLI;


    @SerializedName("PMHA")
    private int PMHA;


    @SerializedName("Recurr_alwc")
    private int Recurr_alwc;


    @SerializedName("RO_pay_alwc")
    private int RO_pay_alwc;


    @SerializedName("Status")
    private int Status;


    @SerializedName("SUBSCRIPTION")
    private int SUBSCRIPTION;


    @SerializedName("p_hash")
    private String p_hash;


    @SerializedName("Tolal_credit")
    private int Tolal_credit;


    @SerializedName("Total_Debit")
    private int Total_Debit;


    @SerializedName("TPTL")
    private int TPTL;


    @SerializedName("WITHDRAWAL")
    private int WITHDRAWAL;


    @SerializedName("Year")
    private int Year;


    @SerializedName("PAN_NO")
    private String PAN_NO;


    @SerializedName("MONTH_ENDING")
    private String MONTH_ENDING;

    public int getAcqitance_rolls() {
        return Acqitance_rolls;
    }

    public void setAcqitance_rolls(int Acqitance_rolls) {
        this.Acqitance_rolls = Acqitance_rolls;
    }

    public int getAFPP_BBF() {
        return AFPP_BBF;
    }

    public void setAFPP_BBF(int AFPP_BBF) {
        this.AFPP_BBF = AFPP_BBF;
    }

    public int getAFPPF_refund() {
        return AFPPF_refund;
    }

    public void setAFPPF_refund(int AFPPF_refund) {
        this.AFPPF_refund = AFPPF_refund;
    }

    public int getAFPPF_subscription() {
        return AFPPF_subscription;
    }

    public void setAFPPF_subscription(int AFPPF_subscription) {
        this.AFPPF_subscription = AFPPF_subscription;
    }

    public int getAGIF() {
        return AGIF;
    }

    public void setAGIF(int AGIF) {
        this.AGIF = AGIF;
    }

    public int getAmt_credit_to_bank() {
        return Amt_credit_to_bank;
    }

    public void setAmt_credit_to_bank(int Amt_credit_to_bank) {
        this.Amt_credit_to_bank = Amt_credit_to_bank;
    }

    public int getAmt_in_words() {
        return Amt_in_words;
    }

    public void setAmt_in_words(int Amt_in_words) {
        this.Amt_in_words = Amt_in_words;
    }

    public int getArr_Rec_pay_alwc() {
        return Arr_Rec_pay_alwc;
    }

    public void setArr_Rec_pay_alwc(int Arr_Rec_pay_alwc) {
        this.Arr_Rec_pay_alwc = Arr_Rec_pay_alwc;
    }

    public int getBand_pay() {
        return Band_pay;
    }

    public void setBand_pay(int Band_pay) {
        this.Band_pay = Band_pay;
    }

    public String getBirth_dt() {
        return Birth_dt;
    }

    public void setBirth_dt(String Birth_dt) {
        this.Birth_dt = Birth_dt;
    }

    public int getBonus_on_CR_bal() {
        return Bonus_on_CR_bal;
    }

    public void setBonus_on_CR_bal(int Bonus_on_CR_bal) {
        this.Bonus_on_CR_bal = Bonus_on_CR_bal;
    }

    public int getCl_pay() {
        return Cl_pay;
    }

    public void setCl_pay(int Cl_pay) {
        this.Cl_pay = Cl_pay;
    }

    public int getCLOSING_BAL() {
        return CLOSING_BAL;
    }

    public void setCLOSING_BAL(int CLOSING_BAL) {
        this.CLOSING_BAL = CLOSING_BAL;
    }

    public int getCredit_Bal_Released() {
        return Credit_Bal_Released;
    }

    public void setCredit_Bal_Released(int Credit_Bal_Released) {
        this.Credit_Bal_Released = Credit_Bal_Released;
    }

    public int getDA() {
        return DA;
    }

    public void setDA(int DA) {
        this.DA = DA;
    }

    public int getE_Ticketing() {
        return E_Ticketing;
    }

    public void setE_Ticketing(int E_Ticketing) {
        this.E_Ticketing = E_Ticketing;
    }

    public int getFAMO() {
        return FAMO;
    }

    public void setFAMO(int FAMO) {
        this.FAMO = FAMO;
    }

    public int getFd_id() {
        return fd_id;
    }

    public void setFd_id(int fd_id) {
        this.fd_id = fd_id;
    }

    public int getFd_noti_dt() {
        return fd_noti_dt;
    }

    public void setFd_noti_dt(int fd_noti_dt) {
        this.fd_noti_dt = fd_noti_dt;
    }

    public int getGP_X_pay() {
        return GP_X_pay;
    }

    public void setGP_X_pay(int GP_X_pay) {
        this.GP_X_pay = GP_X_pay;
    }

    public int getGrade_pay() {
        return Grade_pay;
    }

    public void setGrade_pay(int Grade_pay) {
        this.Grade_pay = Grade_pay;
    }

    public int getIncome_tax_EC() {
        return Income_tax_EC;
    }

    public void setIncome_tax_EC(int Income_tax_EC) {
        this.Income_tax_EC = Income_tax_EC;
    }

    public int getLoan_advance() {
        return Loan_advance;
    }

    public void setLoan_advance(int Loan_advance) {
        this.Loan_advance = Loan_advance;
    }

    public int getLRA() {
        return LRA;
    }

    public void setLRA(int LRA) {
        this.LRA = LRA;
    }

    public int getMonth() {
        return Month;
    }

    public void setMonth(int Month) {
        this.Month = Month;
    }

    public int getMS_pay() {
        return MS_pay;
    }

    public void setMS_pay(int MS_pay) {
        this.MS_pay = MS_pay;
    }

    public int getOp_balance() {
        return Op_balance;
    }

    public void setOp_balance(int Op_balance) {
        this.Op_balance = Op_balance;
    }

    public int getOp_balance_debit() {
        return Op_balance_debit;
    }

    public void setOp_balance_debit(int Op_balance_debit) {
        this.Op_balance_debit = Op_balance_debit;
    }

    public int getOther_adj() {
        return Other_adj;
    }

    public void setOther_adj(int Other_adj) {
        this.Other_adj = Other_adj;
    }

    public int getOther_Adj_Debit() {
        return Other_Adj_Debit;
    }

    public void setOther_Adj_Debit(int Other_Adj_Debit) {
        this.Other_Adj_Debit = Other_Adj_Debit;
    }

    public int getPLI() {
        return PLI;
    }

    public void setPLI(int PLI) {
        this.PLI = PLI;
    }

    public int getPMHA() {
        return PMHA;
    }

    public void setPMHA(int PMHA) {
        this.PMHA = PMHA;
    }

    public int getRecurr_alwc() {
        return Recurr_alwc;
    }

    public void setRecurr_alwc(int Recurr_alwc) {
        this.Recurr_alwc = Recurr_alwc;
    }

    public int getRO_pay_alwc() {
        return RO_pay_alwc;
    }

    public void setRO_pay_alwc(int RO_pay_alwc) {
        this.RO_pay_alwc = RO_pay_alwc;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int Status) {
        this.Status = Status;
    }

    public int getSUBSCRIPTION() {
        return SUBSCRIPTION;
    }

    public void setSUBSCRIPTION(int SUBSCRIPTION) {
        this.SUBSCRIPTION = SUBSCRIPTION;
    }

    public String getP_hash() {
        return p_hash;
    }

    public void setP_hash(String p_hash) {
        this.p_hash = p_hash;
    }

    public int getTolal_credit() {
        return Tolal_credit;
    }

    public void setTolal_credit(int Tolal_credit) {
        this.Tolal_credit = Tolal_credit;
    }

    public int getTotal_Debit() {
        return Total_Debit;
    }

    public void setTotal_Debit(int Total_Debit) {
        this.Total_Debit = Total_Debit;
    }

    public int getTPTL() {
        return TPTL;
    }

    public void setTPTL(int TPTL) {
        this.TPTL = TPTL;
    }

    public int getWITHDRAWAL() {
        return WITHDRAWAL;
    }

    public void setWITHDRAWAL(int WITHDRAWAL) {
        this.WITHDRAWAL = WITHDRAWAL;
    }

    public int getYear() {
        return Year;
    }

    public void setYear(int Year) {
        this.Year = Year;
    }

    public String getPAN_NO() {
        return PAN_NO;
    }

    public void setPAN_NO(String PAN_NO) {
        this.PAN_NO = PAN_NO;
    }

    public String getMONTH_ENDING() {
        return MONTH_ENDING;
    }

    public void setMONTH_ENDING(String MONTH_ENDING) {
        this.MONTH_ENDING = MONTH_ENDING;
    }
}
