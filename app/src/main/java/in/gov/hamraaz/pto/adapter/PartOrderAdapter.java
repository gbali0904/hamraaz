package in.gov.hamraaz.pto.adapter;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.gov.hamraaz.R;
import in.gov.hamraaz.pto.ModelForPartOrder;

public class PartOrderAdapter extends RecyclerView.Adapter {
    private final FragmentActivity activity;

    private List<ModelForPartOrder> modelForPartOrders;

    public PartOrderAdapter(FragmentActivity activity) {
        this.activity = activity;
        modelForPartOrders = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder holder = null;
        View v;
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.part_order_adapter, parent, false);
        holder = new ItemViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ItemViewHolder) holder).bind(position);
    }


    @Override
    public int getItemCount() {
        return modelForPartOrders.size();
    }

    public void setData(List<ModelForPartOrder> modelForPartOrders) {
        this.modelForPartOrders = modelForPartOrders;
        notifyDataSetChanged();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.Ser)
        TextView Ser;
        @BindView(R.id.PTONO)
        TextView PTONO;
        @BindView(R.id.ItemNo)
        TextView ItemNo;
        @BindView(R.id.DESC)
        TextView DESC;
        @BindView(R.id.RORECDDT)
        TextView RORECDDT;
        @BindView(R.id.ROSTATUS)
        TextView ROSTATUS;
        @BindView(R.id.ROREJREASON)
        TextView ROREJREASON;
        @BindView(R.id.PAOSTATUS)
        TextView PAOSTATUS;
        @BindView(R.id.PAOREJREASON)
        TextView PAOREJREASON;
        public ItemViewHolder(View v) {

            super(v);
            ButterKnife.bind(this, v);
        }
        public void bind(int position) {
            ModelForPartOrder modelForPartOrder = modelForPartOrders.get(position);
            int id=++position;
            Ser.setText(""+id);
            PTONO.setText(""+modelForPartOrder.getPTO_NO());
            ItemNo.setText(""+modelForPartOrder.getITEM_NO());
            DESC.setText(""+modelForPartOrder.getPTO_Desc());
            RORECDDT.setText(""+modelForPartOrder.getRO_RECD_DT());
            ROSTATUS.setText(""+modelForPartOrder.getRO_STATUS());
            ROREJREASON.setText(""+modelForPartOrder.getRO_REJ_REASON());
            PAOSTATUS.setText(""+modelForPartOrder.getPAO_STATUS());
            PAOREJREASON.setText(""+modelForPartOrder.getPAO_REJ_REASON());

        }
    }
}
