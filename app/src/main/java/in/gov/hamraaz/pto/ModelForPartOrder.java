package in.gov.hamraaz.pto;

public class ModelForPartOrder {

    /**
     * PTO_NO : 0/0199/2019
     * ITEM_NO : 0018
     * PTO_Desc : AWL
     * RO_RECD_DT : 21/05/2019
     * RO_STATUS : ACCEPT
     * RO_REJ_REASON : ff
     * RO_UPDATE_DT : ff
     * PAO_DESP_DT : ff
     * PAO_ADJUST_DT : ff
     * PAO_STATUS : fff
     * PAO_REJ_REASON : cccc
     * Entry_Dt : 24/06/2019
     */

    private String PTO_NO;
    private String ITEM_NO;
    private String PTO_Desc;
    private String RO_RECD_DT;
    private String RO_STATUS;
    private String RO_REJ_REASON;
    private String RO_UPDATE_DT;
    private String PAO_DESP_DT;
    private String PAO_ADJUST_DT;
    private String PAO_STATUS;
    private String PAO_REJ_REASON;
    private String Entry_Dt;

    public String getPTO_NO() {
        return PTO_NO;
    }

    public void setPTO_NO(String PTO_NO) {
        this.PTO_NO = PTO_NO;
    }

    public String getITEM_NO() {
        return ITEM_NO;
    }

    public void setITEM_NO(String ITEM_NO) {
        this.ITEM_NO = ITEM_NO;
    }

    public String getPTO_Desc() {
        return PTO_Desc;
    }

    public void setPTO_Desc(String PTO_Desc) {
        this.PTO_Desc = PTO_Desc;
    }

    public String getRO_RECD_DT() {
        return RO_RECD_DT;
    }

    public void setRO_RECD_DT(String RO_RECD_DT) {
        this.RO_RECD_DT = RO_RECD_DT;
    }

    public String getRO_STATUS() {
        return RO_STATUS;
    }

    public void setRO_STATUS(String RO_STATUS) {
        this.RO_STATUS = RO_STATUS;
    }

    public String getRO_REJ_REASON() {
        return RO_REJ_REASON;
    }

    public void setRO_REJ_REASON(String RO_REJ_REASON) {
        this.RO_REJ_REASON = RO_REJ_REASON;
    }

    public String getRO_UPDATE_DT() {
        return RO_UPDATE_DT;
    }

    public void setRO_UPDATE_DT(String RO_UPDATE_DT) {
        this.RO_UPDATE_DT = RO_UPDATE_DT;
    }

    public String getPAO_DESP_DT() {
        return PAO_DESP_DT;
    }

    public void setPAO_DESP_DT(String PAO_DESP_DT) {
        this.PAO_DESP_DT = PAO_DESP_DT;
    }

    public String getPAO_ADJUST_DT() {
        return PAO_ADJUST_DT;
    }

    public void setPAO_ADJUST_DT(String PAO_ADJUST_DT) {
        this.PAO_ADJUST_DT = PAO_ADJUST_DT;
    }

    public String getPAO_STATUS() {
        return PAO_STATUS;
    }

    public void setPAO_STATUS(String PAO_STATUS) {
        this.PAO_STATUS = PAO_STATUS;
    }

    public String getPAO_REJ_REASON() {
        return PAO_REJ_REASON;
    }

    public void setPAO_REJ_REASON(String PAO_REJ_REASON) {
        this.PAO_REJ_REASON = PAO_REJ_REASON;
    }

    public String getEntry_Dt() {
        return Entry_Dt;
    }

    public void setEntry_Dt(String Entry_Dt) {
        this.Entry_Dt = Entry_Dt;
    }
}
