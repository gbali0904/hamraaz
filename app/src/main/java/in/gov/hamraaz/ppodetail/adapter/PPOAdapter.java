package in.gov.hamraaz.ppodetail.adapter;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.gov.hamraaz.Notification.pdfview.DocumentFragment;
import in.gov.hamraaz.R;
import in.gov.hamraaz.Utils.Constant;
import in.gov.hamraaz.Utils.RemoteConfigManager;
import in.gov.hamraaz.fragment.MainActivityFragment;
import in.gov.hamraaz.ppodetail.ModelForPPODetail;

public class PPOAdapter extends RecyclerView.Adapter {
    private final FragmentActivity activity;

    private List<ModelForPPODetail.PPODetailsBean> ppo_details;

    public PPOAdapter(FragmentActivity activity) {
        this.activity = activity;
        ppo_details = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_ppo_adapter, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((MyViewHolder) holder).bindData(ppo_details.get(position));
    }

    @Override
    public int getItemCount() {
        return ppo_details.size();
    }

    public void setData(List<ModelForPPODetail.PPODetailsBean> ppo_details) {
        this.ppo_details = ppo_details;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtItemPPONo)
        TextView txtItemPPONo;
        @BindView(R.id.clickHere)
        TextView clickHere;
        @BindView(R.id.btnMainLodgeGrievance)
        CardView btnMainLodgeGrievance;

        public MyViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        public void bindData(final ModelForPPODetail.PPODetailsBean ppoDetailsBean) {

            txtItemPPONo.setText(" : " + ppoDetailsBean.getPpo_no());
            clickHere.setOnClickListener(view -> {

                DocumentFragment documentFragment = new DocumentFragment();
                Bundle bundle = new Bundle();
                bundle.putString(Constant.PDF_URL, RemoteConfigManager.getPdfBaseUrl() + ppoDetailsBean.getPdfurl());
                documentFragment.setArguments(bundle);
                MainActivityFragment.instansiate(documentFragment, activity, DocumentFragment.TAG, "PPO Document");

            });

        }
    }
}
