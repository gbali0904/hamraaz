package in.gov.hamraaz.ppodetail;

import java.util.List;

public class ModelForPPODetail {


    private List<PPODetailsBean> PPO_Details;

    public List<PPODetailsBean> getPPO_Details() {
        return PPO_Details;
    }

    public void setPPO_Details(List<PPODetailsBean> PPO_Details) {
        this.PPO_Details = PPO_Details;
    }

    public static class PPODetailsBean {
        /**
         * ppo_no : 194201901651
         * pdfurl : /HtIY9UyrNHG/ePPO_PDF_Files/PSO_0004469_194201901651_0500.pdf
         */

        private String ppo_no;
        private String pdfurl;

        public String getPpo_no() {
            return ppo_no;
        }

        public void setPpo_no(String ppo_no) {
            this.ppo_no = ppo_no;
        }

        public String getPdfurl() {
            return pdfurl;
        }

        public void setPdfurl(String pdfurl) {
            this.pdfurl = pdfurl;
        }
    }
}
