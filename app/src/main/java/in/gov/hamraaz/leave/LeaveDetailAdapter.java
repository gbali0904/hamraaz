package in.gov.hamraaz.leave;

import android.graphics.Typeface;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.gov.hamraaz.R;
import in.gov.hamraaz.leave.model.ModelForLeave;

public class LeaveDetailAdapter extends RecyclerView.Adapter {
    private final FragmentActivity activity;

    private List<ModelForLeave.LeaveLeaveBean.LeaveBean> modelForLeaves;

    public LeaveDetailAdapter(FragmentActivity activity) {
        this.activity = activity;
        modelForLeaves = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        View v;
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.leave_adapter, parent, false);
        holder = new ItemViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ((ItemViewHolder) holder).bind(position);
    }

    @Override
    public int getItemCount() {
        return modelForLeaves.size();
    }

    public void setData(List<ModelForLeave.LeaveLeaveBean.LeaveBean> modelForLeaves) {
        this.modelForLeaves = modelForLeaves;
        notifyDataSetChanged();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.year)
        TextView year;
        @BindView(R.id.day)
        TextView day;
        @BindView(R.id.ptono)
        TextView ptono;
        @BindView(R.id.ptodate)
        TextView ptodate;
        public ItemViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);



        }

        public void bind(int position) {

            ModelForLeave.LeaveLeaveBean.LeaveBean modelForLeave = modelForLeaves.get(position);
            year.setText(""+modelForLeave.getFd_year());
            day.setText(""+modelForLeave.getFd_days());
            ptono.setText(""+modelForLeave.getPTO_NO());
            ptodate.setText(""+modelForLeave.getPto_dt());
            if (modelForLeaves.size()-1 == position){
                year.setTypeface(year.getTypeface(), Typeface.BOLD);
                day.setTypeface(day.getTypeface(), Typeface.BOLD);
                ptono.setTypeface(ptono.getTypeface(), Typeface.BOLD);
                ptodate.setTypeface(ptodate.getTypeface(), Typeface.BOLD);
            }
        }
    }
}
