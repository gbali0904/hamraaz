package in.gov.hamraaz.leave.model;

import java.util.List;

public class ModelForLeave  {
    private LeaveLeaveBean leaveLtc;
    public LeaveLeaveBean getLeaveLtc() {
        return leaveLtc;
    }
    public void setLeaveLtc(LeaveLeaveBean leaveLtc) {
        this.leaveLtc = leaveLtc;
    }

    public static class LeaveLeaveBean {
        private List<LeaveBean> Leave;
        private List<LeaveBean> Ltc;

        public List<LeaveBean> getLeave() {
            return Leave;
        }

        public void setLeave(List<LeaveBean> Leave) {
            this.Leave = Leave;
        }

        public List<LeaveBean> getLtc() {
            return Ltc;
        }

        public void setLtc(List<LeaveBean> Ltc) {
            this.Ltc = Ltc;
        }

        public static class LeaveBean {
            /**
             * fd_year : 2006
             * fd_days : 30
             * PTO_NO : 0/0096/0051/2007
             * pto_dt :
             */
            private String fd_year;
            private int fd_days;
            private String PTO_NO;
            private String pto_dt;

            public String getFd_year() {
                return fd_year;
            }

            public void setFd_year(String fd_year) {
                this.fd_year = fd_year;
            }

            public int getFd_days() {
                return fd_days;
            }

            public void setFd_days(int fd_days) {
                this.fd_days = fd_days;
            }

            public String getPTO_NO() {
                return PTO_NO;
            }

            public void setPTO_NO(String PTO_NO) {
                this.PTO_NO = PTO_NO;
            }

            public String getPto_dt() {
                return pto_dt;
            }

            public void setPto_dt(String pto_dt) {
                this.pto_dt = pto_dt;
            }
        }
    }
}
