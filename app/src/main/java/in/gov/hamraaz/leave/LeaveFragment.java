package in.gov.hamraaz.leave;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import in.gov.hamraaz.App;
import in.gov.hamraaz.R;
import in.gov.hamraaz.Utils.Constant;
import in.gov.hamraaz.Utils.DialogUtil;
import in.gov.hamraaz.Utils.RemoteConfigManager;
import in.gov.hamraaz.Utils.Util;
import in.gov.hamraaz.leave.model.ModelForLeave;
import in.gov.hamraaz.login.LoginActivity;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * A simple {@link Fragment} subclass.
 */
public class LeaveFragment extends Fragment {


    public static final String TAG = LeaveFragment.class.getSimpleName();
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    @BindView(R.id.horizontalScrollView)
    HorizontalScrollView horizontalScrollView;
    @BindView(R.id.noLeaveFound)
    TextView noLeaveFound;
    @BindView(R.id.recyclerview1)
    RecyclerView recyclerview1;
    @BindView(R.id.horizontalScrollView1)
    HorizontalScrollView horizontalScrollView1;
    @BindView(R.id.noLtcFound)
    TextView noLtcFound;
    Unbinder unbinder;

    private LeaveDetailAdapter leaveDetailAdapter;
    private String pan_hass;
    private LeaveDetailAdapterLTC leaveDetailAdapter1;
    private String url;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_leave, container, false);
        unbinder = ButterKnife.bind(this, view);
        leaveDetailAdapter = new LeaveDetailAdapter(getActivity());
        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(getActivity());
        recyclerview.setLayoutManager(horizontalLayoutManagaer);
        recyclerview.setAdapter(leaveDetailAdapter);


        LinearLayoutManager horizontalLayoutManagaer1 = new LinearLayoutManager(getActivity());
        leaveDetailAdapter1 = new LeaveDetailAdapterLTC(getActivity());
        recyclerview1.setLayoutManager(horizontalLayoutManagaer1);
        recyclerview1.setAdapter(leaveDetailAdapter1);

        Bundle bundle = getArguments();
        pan_hass = bundle.getString(Constant.KEY_PAN_HASH);

        if (!Util.checkInternet(getActivity())) {
            url = "https://10.0.0.161/API_Hamraaz_App/api/DataSend/SendDtLeaveLTC";
        } else {
            url = RemoteConfigManager.getLeaveUrl();
        }
        makeServerCall(url, pan_hass);
        return view;
    }


    void makeServerCall(String url, final String uid) {
        final ProgressDialog progress = DialogUtil.createProgressDialog(getActivity(), "Loading", "Please wait");
        progress.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("this", "Response" + response);
                if (response.length() > 0) {
                    progress.dismiss();
                    ModelForLeave modelForLeave = new Gson().fromJson(response, ModelForLeave.class);
                    ModelForLeave.LeaveLeaveBean leaveLtc = modelForLeave.getLeaveLtc();
                    List<ModelForLeave.LeaveLeaveBean.LeaveBean> leave = leaveLtc.getLeave();
                    List<ModelForLeave.LeaveLeaveBean.LeaveBean> ltc = leaveLtc.getLtc();
                    if (!leave.isEmpty()) {
                        horizontalScrollView.setVisibility(View.VISIBLE);
                        noLeaveFound.setVisibility(View.GONE);
                        leaveDetailAdapter.setData(leave);
                    } else {
                        horizontalScrollView.setVisibility(View.GONE);
                        noLeaveFound.setVisibility(View.VISIBLE);
                    }
                    if (!ltc.isEmpty()) {

                        horizontalScrollView1.setVisibility(View.VISIBLE);
                        noLtcFound.setVisibility(View.GONE);
                        leaveDetailAdapter1.setData(ltc);
                    } else {
                        horizontalScrollView1.setVisibility(View.GONE);
                        noLtcFound.setVisibility(View.VISIBLE);
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();

                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                getActivity().finish();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + uid);
                headers.put("timestamp", Util.getTimestamp());
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
