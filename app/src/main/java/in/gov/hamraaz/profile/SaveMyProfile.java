package in.gov.hamraaz.profile;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import in.gov.hamraaz.Account.model.OtpModel;
import in.gov.hamraaz.App;
import in.gov.hamraaz.R;
import in.gov.hamraaz.Utils.DialogUtil;
import in.gov.hamraaz.Utils.EncryptionUtil;
import in.gov.hamraaz.Utils.RemoteConfigManager;
import in.gov.hamraaz.Utils.TAGs;
import in.gov.hamraaz.Utils.Util;
import in.gov.hamraaz.login.LoginActivity;

public class SaveMyProfile {
    private static OnItemClicked onClickItem;
    TextInputEditText edtOTP;
    TextInputLayout tilOTP;
    Button btnOTPSubmit;
    LinearLayout llOTPSubmitLayout;
    TextView textDialog;
    private ProgressDialog progressDialog;
    private AlertDialog alertDialog;
    private TextInputEditText editNewMobile;
    private TextInputLayout newMobile;
    private String target;
    private Button btnOTPCancel;

    public void showDialog(final Activity activity, String otpNoOrEmail, final String target, final String otp_mode, final String txn, final String pan_hash, String msg) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        dialogBuilder.setCancelable(false);
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialog = inflater.inflate(R.layout.dialog, null);
        dialogBuilder.setView(dialog);


        edtOTP = dialog.findViewById(R.id.edtOTP);
        newMobile = dialog.findViewById(R.id.newMobile);


        editNewMobile = dialog.findViewById(R.id.editNewMobile);

        editNewMobile.setText(otpNoOrEmail);


        tilOTP = dialog.findViewById(R.id.tilOTP);
        btnOTPSubmit = dialog.findViewById(R.id.btnOTPSubmit);
        btnOTPCancel = dialog.findViewById(R.id.btnOTPCancel);
        textDialog = dialog.findViewById(R.id.text_dialog);
        textDialog.setText("" + msg);
        alertDialog = dialogBuilder.create();
        alertDialog.show();

        btnOTPSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String otp = edtOTP.getText().toString();
                if (TextUtils.isEmpty(otp) || otp.length() != 6) {
                    edtOTP.setError("Please enter your 6 digit OTP");
                } else {
                    submitOTPServerCall(activity, otp_mode, txn, target, pan_hash);
                }


            }
        });
        btnOTPCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                onClickItem.onItemClick();
            }
        });


    }


    private void submitOTPServerCall(final Activity activity, final String otpMode, final String txn, final String target, final String pan_hash) {
        progressDialog = DialogUtil.createProgressDialog(activity, "Request OTP", "Please wait while we verifying your OTP");
        progressDialog.show();

        String url = RemoteConfigManager.getSubmitOtpUrl();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, response -> {
            progressDialog.cancel();
            OtpModel otpModel = new Gson().fromJson(response, OtpModel.class);
            if (otpModel.getSk().getMsg().equals("Success")) {
                alertDialog.dismiss();
                saveMyProfileData(activity, otpMode, target, pan_hash);

            } else {
                DialogUtil.createAlertDialog(activity, "Error", response, "Okay")
                        .show();
            }

        }, error -> {
            progressDialog.cancel();

            Intent intent = new Intent(activity, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            activity.startActivity(intent);
            activity.finish();
        }) {

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + pan_hash);
                headers.put("timestamp", Util.getTimestamp());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                String otp = edtOTP.getText().toString().trim();
                params.put("txn", txn);
                params.put("otp_mode", otpMode);
                params.put("otp_hash", otp);
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(stringRequest);
    }

    private void saveMyProfileData(final Activity activity, final String otp_mode, final String target, final String pan_hash) {
        String url = RemoteConfigManager.getProfileUpdateUrl();

        Log.e("this", "Url::" + url);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.cancel();

                Log.e("this", "Url::" + response);
                DialogUtil.createAlertDialog(activity, "Congratulations", response, "Okay", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onClickItem.onItemClick();
                    }
                }).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.cancel();

                Intent intent = new Intent(activity, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                activity.startActivity(intent);
                activity.finish();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("schemeFrame", App.getInstance().getPreference().getString(TAGs.KEY_PAN_HASH_VALUE, ""));
                params.put("target", target);
                params.put("otp_mode", otp_mode);

                Log.e("this", "Url::" + new Gson().toJson(params));
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(stringRequest);

    }


    //make interface like this
    public interface OnItemClicked {
        void onItemClick();
    }

    public static void setOnClick(OnItemClicked onClick) {
        onClickItem = onClick;
    }

}
