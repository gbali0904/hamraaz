package in.gov.hamraaz.profile;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import in.gov.hamraaz.Account.OTPActivity;
import in.gov.hamraaz.App;
import in.gov.hamraaz.R;
import in.gov.hamraaz.Utils.Constant;
import in.gov.hamraaz.Utils.DialogUtil;
import in.gov.hamraaz.Utils.EncryptionUtil;
import in.gov.hamraaz.Utils.RemoteConfigManager;
import in.gov.hamraaz.Utils.TAGs;
import in.gov.hamraaz.Utils.Util;
import in.gov.hamraaz.login.LoginActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyProfileFragment extends Fragment {


    public static final String TAG = MyProfileFragment.class.getSimpleName();
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.mobileNo)
    EditText mobileNo;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.doe)
    TextView doe;
    @BindView(R.id.roName)
    TextView roName;
    Unbinder unbinder;
    @BindView(R.id.editMobile)
    ImageView editMobile;
    @BindView(R.id.editEmail)
    ImageView editEmail;
    @BindView(R.id.updatemobile)
    Button updatemobile;
    @BindView(R.id.updateEmail)
    Button updateEmail;
    private String pan_hass;
    private String selectedMode;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_profile, container, false);
        unbinder = ButterKnife.bind(this, view);

        toggleMobileEnable(false);
        toggleEmailEnable(false);


        mobileNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (updatemobile.isEnabled()) {
                    toggleMobileEnable(false);
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().length() == 10) {
                    toggleMobileEnable(true);
                }
            }
        });
        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (email.isEnabled()) {
                    toggleEmailEnable(false);
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {
                if (App.isValidEmail(editable.toString())) {
                    toggleEmailEnable(true);
                }


            }
        });
        Bundle bundle = getArguments();
        pan_hass = bundle.getString(Constant.KEY_PAN_HASH);
        makeServerCall(RemoteConfigManager.getMyProfile(), pan_hass);
        SaveMyProfile.setOnClick(new SaveMyProfile.OnItemClicked() {
            @Override
            public void onItemClick() {
                makeServerCall(RemoteConfigManager.getMyProfile(), pan_hass);
            }
        });
        return view;
    }

    private void toggleMobileEnable(boolean isEnabled) {
        updatemobile.setEnabled(isEnabled);
        float alpha = (isEnabled) ? 1.00f : 0.27f;
        updatemobile.setAlpha(alpha);
    } private void toggleEmailEnable(boolean isEnabled) {
        updateEmail.setEnabled(isEnabled);
        float alpha = (isEnabled) ? 1.00f : 0.27f;
        updateEmail.setAlpha(alpha);
    }

    void makeServerCall(String url, final String uid) {
        final ProgressDialog progress = DialogUtil.createProgressDialog(getActivity(), "Loading", "Please wait");
        progress.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.length() > 0) {
                    progress.dismiss();
                    Type collectionType = new TypeToken<List<ModelForMyProfile>>() {
                    }.getType();
                    List<ModelForMyProfile> modelForMyProfiles = new Gson().fromJson(response, collectionType);

                    if (!modelForMyProfiles.isEmpty()) {
                        ModelForMyProfile modelForMyProfile = modelForMyProfiles.get(0);
                        name.setText("" + modelForMyProfile.getIndl_name());
                        mobileNo.setText("" + modelForMyProfile.getMobile_No());
                        email.setText("" + modelForMyProfile.getEmail());
                        doe.setText("" + modelForMyProfile.getDoe());
                        roName.setText("" + modelForMyProfile.getRo_name());
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();

                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                getActivity().finish();
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("schemeFrame", App.getInstance().getPreference().getString(TAGs.KEY_SCHEME_FRAME, ""));
                return params;
            }
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer "+ pan_hass);
                headers.put("timestamp", Util.getTimestamp());
                return headers;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void callApi(EditText edtText, Button button) {
        edtText.setEnabled(false);
        button.setVisibility(View.GONE);
        String txt = edtText.getText().toString().trim();
        requestOTPServerCall(txt,EncryptionUtil.getHashValue(txt, EncryptionUtil.USER_HASH_SALT));

    }


    private void requestOTPServerCall(final String otpNoOrEmail, final String hashValue) {
        final ProgressDialog progressDialog = DialogUtil.createProgressDialog(getActivity(), "Request OTP", "Please wait while we are sending OTP to your mobile");
        progressDialog.show();

        final String target = getTarget();
        final String otp_mode = getOptMode();

        String url = RemoteConfigManager.getRequestMyProfileOtpUrl();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            private String error_msz;
            @Override
            public void onResponse(String response) {
                progressDialog.cancel();
                if (response.startsWith("Txn")) {
                    final String txn = response.substring(4, 20);
                    String successMessage = response.substring(21);
                    SaveMyProfile alert = new SaveMyProfile();
                    alert.showDialog(getActivity(),otpNoOrEmail,target,otp_mode,txn, pan_hass,"Update");
                } else {
                    if (otp_mode.equals("1"))
                    {
                        error_msz=  "Kindly update your mobile number in record office to avail hamraaz application facilities";
                    }else {
                        error_msz=  "Kindly update your Email ID in record office to avail hamraaz application facilities";
                    }
                    DialogUtil.createAlertDialog(getActivity(), "Error", error_msz, "Okay").show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.cancel();
                DialogUtil.createAlertDialog(getActivity(), "Error", "Something went wrong Please try again", "Okay").show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("target", target); // hash of entered OTP
                params.put("otp_mode", otp_mode); // txn type 1= mobile, 2 = Email
                params.put("schemeFrame", hashValue);

                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);

    }

    private String getOptMode() {
        if (selectedMode == OTPMode.EMIAL.toString()) {
            return "2";
        } else {
            return "1";
        }
    }

    private String getTarget() {
        if (selectedMode == OTPMode.EMIAL.toString()) {
            return email.getText().toString().trim();
        } else {
            String mobileNumber = mobileNo.getText().toString();
            return mobileNumber.replace("9", "_").replace("7", "-");
        }
    }


    @OnClick({R.id.editMobile, R.id.editEmail,R.id.updatemobile, R.id.updateEmail})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.editMobile:
                updatemobile.setVisibility(View.VISIBLE);
                updateEmail.setVisibility(View.GONE);
                mobileNo.setEnabled(true);
                email.setEnabled(false);


                break;
            case R.id.editEmail:
                updatemobile.setVisibility(View.GONE);
                updateEmail.setVisibility(View.VISIBLE);
                mobileNo.setEnabled(false);
                email.setEnabled(true);

                break;

            case R.id.updatemobile:
                String edit_mobileNo = mobileNo.getText().toString();
                if (TextUtils.isEmpty(edit_mobileNo)) {
                    mobileNo.setError("This Field id Mandatory");
                } else {
                    selectedMode = OTPMode.MOBILE.toString();
                    callApi(mobileNo,updatemobile);
                }

                break;
            case R.id.updateEmail:

                String edit_email = email.getText().toString();
                if (TextUtils.isEmpty(edit_email)) {
                    email.setError("This Field id Mandatory");
                } else {
                    selectedMode = OTPMode.EMIAL.toString();
                    callApi(email,updateEmail);
                }
                break;
        }
    }
    public enum OTPMode {
        EMIAL, MOBILE
    }

}
