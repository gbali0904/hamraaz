package in.gov.hamraaz.profile;

public class ModelForMyProfile {

    /**
     * indl_name : VENKATA BHUPAL REDDY
     A
     * Mobile_No : 7898276793
     * email : venkat.reddy9866@gmail.com
     * doe : 28062005
     * Ro_name : Signals Records
     */

    private String indl_name;
    private String Mobile_No;
    private String email;
    private String doe;
    private String Ro_name;

    public String getIndl_name() {
        return indl_name;
    }

    public void setIndl_name(String indl_name) {
        this.indl_name = indl_name;
    }

    public String getMobile_No() {
        return Mobile_No;
    }

    public void setMobile_No(String Mobile_No) {
        this.Mobile_No = Mobile_No;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDoe() {
        return doe;
    }

    public void setDoe(String doe) {
        this.doe = doe;
    }

    public String getRo_name() {
        return Ro_name;
    }

    public void setRo_name(String Ro_name) {
        this.Ro_name = Ro_name;
    }
}
