package in.gov.hamraaz.pao_pto_status;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import in.gov.hamraaz.R;
import in.gov.hamraaz.Utils.Constant;
import in.gov.hamraaz.Utils.DialogUtil;
import in.gov.hamraaz.Utils.RemoteConfigManager;
import in.gov.hamraaz.Utils.Util;
import in.gov.hamraaz.login.LoginActivity;
import in.gov.hamraaz.pao_pto_status.adapter.PaoPtoStatusAdapter;
import in.gov.hamraaz.pao_pto_status.model.PaoPtoStatus;
import in.gov.hamraaz.pao_pto_status.model.PaoPtoStatusColumn;
import in.gov.hamraaz.pao_pto_status.model.PaoPtoStatusModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class PaoPtoStatusFragment extends Fragment {

    @BindView(R.id.recyclerviewheaders)
    RecyclerView recyclerviewheaders;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    private Unbinder unbinder;
    private String pan_hass;
    PaoPtoStatusAdapter paoPtoStatusAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pao_pto_status, container, false);
        unbinder = ButterKnife.bind(this, view);
        Bundle bundle = getArguments();
        pan_hass = bundle.getString(Constant.KEY_PAN_HASH);

        paoPtoStatusAdapter = new PaoPtoStatusAdapter(getActivity());
        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerviewheaders.setLayoutManager(horizontalLayoutManagaer);
        recyclerviewheaders.setAdapter(paoPtoStatusAdapter);

        makeServerCall(RemoteConfigManager.getPaoPtoStatusUrl());
        return view;
    }

    private void makeServerCall(String url) {
        final ProgressDialog progress = DialogUtil.createProgressDialog(getActivity(), "Loading", "Please wait");
        progress.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.length() > 0) {
                    progress.dismiss();
                    PaoPtoStatusModel paoPtoStatusModel = new Gson().fromJson(response, PaoPtoStatusModel.class);
                    List<PaoPtoStatus> paoPtoStatus = paoPtoStatusModel.getPaoPtoStatusSkelton().getPaoPtoStatus();
                    List<PaoPtoStatusColumn> paoPtoStatusColumn = paoPtoStatusModel.getPaoPtoStatusSkelton().getPaoPtoStatusColumn();

                    paoPtoStatusAdapter.setData(paoPtoStatus);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();

                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                getActivity().finish();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + pan_hass);
                headers.put("timestamp", Util.getTimestamp());
                return headers;
            }


        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}