
package in.gov.hamraaz.pao_pto_status.model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class PaoPtoStatus {

    @SerializedName("column_a")
    private String mColumnA;
    @SerializedName("column_b")
    private String mColumnB;
    @SerializedName("column_c")
    private String mColumnC;
    @SerializedName("description")
    private String mDescription;
    @SerializedName("From_Date")
    private String mFromDate;
    @SerializedName("input_qe")
    private String mInputQe;
    @SerializedName("Item_No")
    private String mItemNo;
    @SerializedName("PAO_Status")
    private String mPAOStatus;
    @SerializedName("PTO_Date")
    private String mPTODate;
    @SerializedName("PTO_Group")
    private String mPTOGroup;
    @SerializedName("PTO_No")
    private String mPTONo;
    @SerializedName("reason")
    private String mReason;
    @SerializedName("To_Date")
    private String mToDate;

    public String getColumnA() {
        return mColumnA;
    }

    public void setColumnA(String columnA) {
        mColumnA = columnA;
    }

    public String getColumnB() {
        return mColumnB;
    }

    public void setColumnB(String columnB) {
        mColumnB = columnB;
    }

    public String getColumnC() {
        return mColumnC;
    }

    public void setColumnC(String columnC) {
        mColumnC = columnC;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getFromDate() {
        return mFromDate;
    }

    public void setFromDate(String fromDate) {
        mFromDate = fromDate;
    }

    public String getInputQe() {
        return mInputQe;
    }

    public void setInputQe(String inputQe) {
        mInputQe = inputQe;
    }

    public String getItemNo() {
        return mItemNo;
    }

    public void setItemNo(String itemNo) {
        mItemNo = itemNo;
    }

    public String getPAOStatus() {
        return mPAOStatus;
    }

    public void setPAOStatus(String pAOStatus) {
        mPAOStatus = pAOStatus;
    }

    public String getPTODate() {
        return mPTODate;
    }

    public void setPTODate(String pTODate) {
        mPTODate = pTODate;
    }

    public String getPTOGroup() {
        return mPTOGroup;
    }

    public void setPTOGroup(String pTOGroup) {
        mPTOGroup = pTOGroup;
    }

    public String getPTONo() {
        return mPTONo;
    }

    public void setPTONo(String pTONo) {
        mPTONo = pTONo;
    }

    public String getReason() {
        return mReason;
    }

    public void setReason(String reason) {
        mReason = reason;
    }

    public String getToDate() {
        return mToDate;
    }

    public void setToDate(String toDate) {
        mToDate = toDate;
    }

}
