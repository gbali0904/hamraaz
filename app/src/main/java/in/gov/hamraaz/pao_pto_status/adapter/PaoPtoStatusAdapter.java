package in.gov.hamraaz.pao_pto_status.adapter;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.gov.hamraaz.R;
import in.gov.hamraaz.pao_pto_status.model.PaoPtoStatus;

public class PaoPtoStatusAdapter extends RecyclerView.Adapter {
    private final FragmentActivity activity;
    private List<PaoPtoStatus> paoPtoStatus;

    public PaoPtoStatusAdapter(FragmentActivity activity) {
        this.activity = activity;
        paoPtoStatus = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        View v;
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.poa_pto_adapter, parent, false);
        holder = new ItemViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ItemViewHolder) holder).bind(position);
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    public void setData(List<PaoPtoStatus> paoPtoStatus) {
        this.paoPtoStatus = paoPtoStatus;
        notifyDataSetChanged();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ser)
        TextView ser;

        public ItemViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        public void bind(int position) {
        }
    }
}
