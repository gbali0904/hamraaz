
package in.gov.hamraaz.pao_pto_status.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class PaoPtoStatusSkelton {

    @SerializedName("pao_pto_status")
    private List<PaoPtoStatus> mPaoPtoStatus;
    @SerializedName("pao_pto_status_column")
    private List<PaoPtoStatusColumn> mPaoPtoStatusColumn;

    public List<PaoPtoStatus> getPaoPtoStatus() {
        return mPaoPtoStatus;
    }

    public void setPaoPtoStatus(List<PaoPtoStatus> paoPtoStatus) {
        mPaoPtoStatus = paoPtoStatus;
    }

    public List<PaoPtoStatusColumn> getPaoPtoStatusColumn() {
        return mPaoPtoStatusColumn;
    }

    public void setPaoPtoStatusColumn(List<PaoPtoStatusColumn> paoPtoStatusColumn) {
        mPaoPtoStatusColumn = paoPtoStatusColumn;
    }

}
