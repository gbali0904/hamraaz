
package in.gov.hamraaz.pao_pto_status.model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class PaoPtoStatusModel {

    @SerializedName("pao_pto_status_skelton")
    private PaoPtoStatusSkelton mPaoPtoStatusSkelton;

    public PaoPtoStatusSkelton getPaoPtoStatusSkelton() {
        return mPaoPtoStatusSkelton;
    }

    public void setPaoPtoStatusSkelton(PaoPtoStatusSkelton paoPtoStatusSkelton) {
        mPaoPtoStatusSkelton = paoPtoStatusSkelton;
    }

}
