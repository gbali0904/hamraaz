package in.gov.hamraaz.Account;

public class ModelForOTP {


    /**
     * sk : {"code":0,"error":true,"txn":"6IRZOW9KZTHCK6CH","msg":"OTP has been sent to: '85XXXXX457' "}
     */

    private SkBean sk;

    public SkBean getSk() {
        return sk;
    }

    public void setSk(SkBean sk) {
        this.sk = sk;
    }

    public static class SkBean {
        /**
         * code : 0
         * error : true
         * txn : 6IRZOW9KZTHCK6CH
         * msg : OTP has been sent to: '85XXXXX457'
         */

        private int code;
        private boolean error;
        private String txn;
        private String msg;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public boolean isError() {
            return error;
        }

        public void setError(boolean error) {
            this.error = error;
        }

        public String getTxn() {
            return txn;
        }

        public void setTxn(String txn) {
            this.txn = txn;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }
    }
}
