package in.gov.hamraaz.Account;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.gov.hamraaz.Account.model.OtpModel;
import in.gov.hamraaz.R;
import in.gov.hamraaz.Utils.DialogUtil;
import in.gov.hamraaz.Utils.EncryptionUtil;
import in.gov.hamraaz.Utils.RemoteConfigManager;
import in.gov.hamraaz.Utils.Util;
import in.gov.hamraaz.changepassword.ChangeDefaultPassword;

public class OTPActivity extends AppCompatActivity {

    public static final String STATUS_MODE = "status_mode";
    public static final String KEY_PAN_HASH = "pan_hash";
    @BindView(R.id.rgOTPRegisterThrough) RadioGroup rgOTPRegisterThrough;
    @BindView(R.id.edtOTPEmail) TextInputEditText edtOTPEmail;
    @BindView(R.id.tilOTPEmail) TextInputLayout tilOTPEmail;
    @BindView(R.id.edtOTPMobile) TextInputEditText edtOTPMobile;
    @BindView(R.id.tilOTPMobile) TextInputLayout tilOTPMobile;
    @BindView(R.id.edtOTP) TextInputEditText edtOTP;
    @BindView(R.id.tilOTP) TextInputLayout tilOTP;
    @BindView(R.id.btnOTPSubmit) Button btnOTPSubmit;
    @BindView(R.id.rbOTPEmail) RadioButton rbOTPEmail;
    @BindView(R.id.rbOTPMobile) RadioButton rbOTPMobile;
    @BindView(R.id.btnOTPRequest) Button btnOTPRequest;
    @BindView(R.id.llOTPRequestLayout) LinearLayout llOTPRequestLayout;
    @BindView(R.id.llOTPSubmitLayout) LinearLayout llOTPSubmitLayout;
    @BindView(R.id.toolbar) Toolbar toolbar;
    private String selectedMode;
    private String txn = "";
    private String panHash = "";
    private String status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        ButterKnife.bind(this);

        if(null != getIntent().getExtras()){
            panHash = getIntent().getExtras().getString(KEY_PAN_HASH);
            status = getIntent().getExtras().getString(STATUS_MODE);
        }

        initUI();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            default:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initUI() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("OTP");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //show OTP Request view
        toggleOTPRequestView(true);
        // setup OTP Mode
        toggleOTPMode(rgOTPRegisterThrough.getCheckedRadioButtonId());
        // disable OTP Request button
        toggleButtonEnable(btnOTPRequest, false);
        // disable OTP Submit button
        toggleButtonEnable(btnOTPSubmit, false);

        rgOTPRegisterThrough.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // set mobile and email field visibility according to OTP mode
                toggleOTPMode(checkedId);
            }
        });

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //toggle buttons on the basis of validation
                if (llOTPRequestLayout.getVisibility() == View.VISIBLE) {
                    toggleButtonEnable(btnOTPRequest, isAllFieldsValid());
                } else {
                    toggleButtonEnable(btnOTPSubmit, isAllFieldsValid());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
        edtOTPMobile.addTextChangedListener(textWatcher);
        edtOTPEmail.addTextChangedListener(textWatcher);
        edtOTP.addTextChangedListener(textWatcher);

    }

    private void toggleOTPRequestView(boolean isToShowRequestView) {
        if (isToShowRequestView) {
            llOTPRequestLayout.setVisibility(View.VISIBLE);
            llOTPSubmitLayout.setVisibility(View.GONE);
        } else {
            llOTPRequestLayout.setVisibility(View.GONE);
            llOTPSubmitLayout.setVisibility(View.VISIBLE);
        }
    }

    private void toggleButtonEnable(Button btnOTPRequest, boolean isEnable) {
        if (isEnable) {
            btnOTPRequest.setEnabled(true);
            btnOTPRequest.setAlpha(1.00f);
        } else {
            btnOTPRequest.setEnabled(false);
            btnOTPRequest.setAlpha(0.36f);
        }
    }

    private void toggleOTPMode(int checkedId) {
        switch (checkedId) {
            case R.id.rbOTPEmail:
                edtOTPEmail.setVisibility(View.VISIBLE);
                edtOTPMobile.setVisibility(View.GONE);
                selectedMode = OTPMode.EMIAL.toString();
                break;
            case R.id.rbOTPMobile:
                edtOTPEmail.setVisibility(View.GONE);
                edtOTPMobile.setVisibility(View.VISIBLE);
                selectedMode = OTPMode.MOBILE.toString();
                break;
        }
    }

    private boolean isAllFieldsValid() {
        String email = edtOTPEmail.getText().toString().trim();
        String mobile = edtOTPMobile.getText().toString().trim();
        String otp = edtOTP.getText().toString().trim();

        if (llOTPRequestLayout.getVisibility() == View.VISIBLE) {// OTP Request layout fields validation
            if (selectedMode.equalsIgnoreCase(OTPMode.EMIAL.toString())) {// validated email
                if (TextUtils.isEmpty(email) || !(Patterns.EMAIL_ADDRESS.matcher(email)
                        .matches())) {
                    edtOTPEmail.setError("Please enter a valid email address");
                    return false;
                }
            } else {//validate mobile number
                if (TextUtils.isEmpty(mobile) || mobile.length() != 10) {
                    edtOTPMobile.setError("Please enter 10 digit mobile number");
                    return false;
                }
            }
        } else {// OTP Submit layout fields validation
            if (TextUtils.isEmpty(otp) || otp.length() != 6) {
                edtOTP.setError("Please enter your 6 digit OTP");
                return false;
            }
        }

        return true;
    }

    @OnClick({R.id.btnOTPRequest, R.id.btnOTPSubmit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnOTPRequest:
                if (isAllFieldsValid()) {
                    requestOTPServerCall();
                }
                break;
            case R.id.btnOTPSubmit:
                if (isAllFieldsValid()) {
                    submitOTPServerCall();
                }
                break;
        }
    }

    private void submitOTPServerCall() {
        final ProgressDialog progressDialog = DialogUtil.createProgressDialog(OTPActivity.this, "Request OTP", "Please wait while we verifying your OTP");
        progressDialog.show();

        String url = RemoteConfigManager.getSubmitOtpUrl();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("this","Submit Response ::"+response);

                progressDialog.cancel();
                OtpModel otpModel = new Gson().fromJson(response, OtpModel.class);
                if (otpModel.getSk().getMsg().equals("Success")) {
                    Intent intent = new Intent(OTPActivity.this, ChangeDefaultPassword.class);
                    intent.putExtra(ChangeDefaultPassword.KEY_PAN_HASH, panHash);
                    intent.putExtra(ChangeDefaultPassword.STATUS_MODE, status);
                    startActivity(intent);
                } else {
                    DialogUtil.createAlertDialog(OTPActivity.this, "Error", response, "Okay")
                            .show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.cancel();
                DialogUtil.createAlertDialog(OTPActivity.this, "Error", "Something went wrong Please try again", "Okay")
                        .show();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + panHash);
                headers.put("timestamp", Util.getTimestamp());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                String otp = edtOTP.getText().toString().trim();
                String otpMode = getOptMode();
                params.put("txn", txn);
                params.put("otp_hash", otp);
                params.put("otp_mode", otpMode);

                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void requestOTPServerCall() {
        final ProgressDialog progressDialog = DialogUtil.createProgressDialog(OTPActivity.this, "Request OTP", "Please wait while we are sending OTP to your mobile");
        progressDialog.show();

        final String url = RemoteConfigManager.getReqOTP();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                Log.e("this","Request OTP Response ::"+response);
                progressDialog.cancel();
                ModelForOTP modelForOTP = new Gson().fromJson(response, ModelForOTP.class);

                if (modelForOTP.getSk().getCode()==0) {
                    txn = modelForOTP.getSk().getTxn();
                    String successMessage = modelForOTP.getSk().getMsg();
                    DialogUtil.createAlertDialog(OTPActivity.this, "Congratulations", successMessage, "Okay", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            toggleOTPRequestView(false);
                        }
                    }).show();
                } else  {
                    DialogUtil.createAlertDialog(OTPActivity.this, "Error", modelForOTP.getSk().getMsg(), "Okay")
                            .show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.cancel();
                DialogUtil.createAlertDialog(OTPActivity.this, "Error", "Something went wrong Please try again", "Okay")
                        .show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                String target = getTarget();
                String otp_mode = getOptMode();
                params.put("target", target); // hash of entered OTP
                params.put("otp_mode", otp_mode); // txn type 1= mobile, 2 = Email
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer "+ panHash);
                headers.put("timestamp", Util.getTimestamp());
                return headers;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private String getOptMode() {
        if (selectedMode == OTPMode.EMIAL.toString()) {

            // pan_hash = edtOTPEmail.getText().toString().trim();
            return "2";
        } else {
            // pan_hash = edtOTPMobile.getText().toString().trim();
            return "1";
        }
    }

    private String getTarget() {
        if (selectedMode == OTPMode.EMIAL.toString()) {
            return edtOTPEmail.getText().toString().trim();
        } else {
            String mobileNumber = edtOTPMobile.getText().toString();
            return mobileNumber.replace("9", "_").replace("7", "-");
        }
    }

    public static void startActivity(Context context, String panHash) {
        Intent intent = new Intent(context, OTPActivity.class);
        intent.putExtra(KEY_PAN_HASH, panHash);
        context.startActivity(intent);
    }

    public enum OTPMode {
        EMIAL, MOBILE
    }
}
