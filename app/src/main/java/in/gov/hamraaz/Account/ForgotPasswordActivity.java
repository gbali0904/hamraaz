package in.gov.hamraaz.Account;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.gov.hamraaz.R;
import in.gov.hamraaz.Utils.DialogUtil;
import in.gov.hamraaz.Utils.EncryptionUtil;
import in.gov.hamraaz.Utils.RemoteConfigManager;
import in.gov.hamraaz.Utils.Util;
import in.gov.hamraaz.changepassword.ChangeDefaultPassword;

public class ForgotPasswordActivity extends AppCompatActivity {

    private static final String KEY_PAN_HASH = "pan_hash";
    @BindView(R.id.edtForgotPassPan) TextInputEditText edtForgotPassPan;
    @BindView(R.id.tilForgotPassPan) TextInputLayout tilForgotPassPan;
    @BindView(R.id.txtForgotPassSecurityQuestion) TextView txtForgotPassSecurityQuestion;
    @BindView(R.id.edtForgotPassSecurityAnswer) TextInputEditText edtForgotPassSecurityAnswer;
    @BindView(R.id.tilForgotPassSecurityAnswer) TextInputLayout tilForgotPassSecurityAnswer;
    @BindView(R.id.btnForgotPassSubmit) Button btnForgotPassSubmit;
    @BindView(R.id.btnForgotPassForgotSecurityAnswer) Button btnForgotPassForgotSecurityAnswer;
    @BindView(R.id.llForgotPassSecurityLayout) LinearLayout llForgotPassSecurityLayout;
    @BindView(R.id.toolbar) Toolbar toolbar;
    private String serucityQuestion;
    private Object panHash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Forgot password");

        if(null != getIntent().getExtras()){
            panHash = getIntent().getExtras().get(KEY_PAN_HASH);
        }

        initUI();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            default:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initUI() {
        toggleSecurityLayout(false);

        edtForgotPassPan.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence panNumber, int start, int before, int count) {
                if (panNumber.length() == 10) {
                    forgotPassCheckPanServerCall();
                } else {
                    edtForgotPassPan.setError("Please enter a valid 10 digit PAN number.");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edtForgotPassSecurityAnswer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence securityAnswer, int start, int before, int count) {
                if (!(TextUtils.isEmpty(securityAnswer))) {
                    toggleSecurityAnswerSubmitButton(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    //TODO New API call
    private void forgotPassCheckPanServerCall() {
        final ProgressDialog progressDialog = DialogUtil.createProgressDialog(ForgotPasswordActivity.this, "Verifying PAN", "Please wait while we are verifying your PAN Details");
        progressDialog.show();

        String url = RemoteConfigManager.getForgotPassCheckPanUrl();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("this","response::"+response);
                progressDialog.cancel();
                if (response.contains("Success")) {
                    serucityQuestion = response.substring(8);
                    toggleSecurityLayout(true);
                } else {
                    DialogUtil.createAlertDialog(ForgotPasswordActivity.this, "Error", response, "Okay")
                            .show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.cancel();
                DialogUtil.createAlertDialog(ForgotPasswordActivity.this, "Error", "Something went wrong Please try again", "Okay")
                        .show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                String pasHash = EncryptionUtil.getHashValue(edtForgotPassPan.getText()
                        .toString().toUpperCase(), EncryptionUtil.USER_HASH_SALT);
                params.put("pan", pasHash); // pancard number hash
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void toggleSecurityLayout(boolean isToShowSecurityLayout) {
        if (isToShowSecurityLayout) {
            toggleSecurityAnswerSubmitButton(false);
            llForgotPassSecurityLayout.setVisibility(View.VISIBLE);
            txtForgotPassSecurityQuestion.setText(serucityQuestion);
        } else {
            llForgotPassSecurityLayout.setVisibility(View.GONE);
        }
    }

    private void toggleSecurityAnswerSubmitButton(boolean isToShow) {
        if (isToShow) {
            btnForgotPassSubmit.setAlpha(1.0f);
            btnForgotPassSubmit.setEnabled(true);
        } else {
            btnForgotPassSubmit.setAlpha(0.75f);
            btnForgotPassSubmit.setEnabled(false);
        }
    }

    @OnClick({R.id.btnForgotPassSubmit, R.id.btnForgotPassForgotSecurityAnswer})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnForgotPassSubmit:
                verifySercurityAnswer();
                break;
            case R.id.btnForgotPassForgotSecurityAnswer:
                Intent intent = new Intent(ForgotPasswordActivity.this, OTPActivity.class);
                String pasHash = EncryptionUtil.getHashValue(edtForgotPassPan.getText()
                        .toString().toUpperCase().trim(), EncryptionUtil.USER_HASH_SALT);
                intent.putExtra(OTPActivity.STATUS_MODE, "2_ch_forgot");
                intent.putExtra(OTPActivity.KEY_PAN_HASH,pasHash);
                startActivity(intent);
                break;
        }
    }

    private void verifySercurityAnswer() {
        final ProgressDialog progressDialog = DialogUtil.createProgressDialog(ForgotPasswordActivity.this, "Verifying security answer", "Please wait while we are verifying your security answer");
        progressDialog.show();

        String url = RemoteConfigManager.verifySecurityAnswerUrl();
        Log.e("this","url::"+url);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.cancel();
                Log.e("this","response::"+new Gson().toJson(response));

                if (response.contains("Success")) {
                    Intent intent = new Intent(ForgotPasswordActivity.this, ChangeDefaultPassword.class);
                    intent.putExtra(ChangeDefaultPassword.STATUS_MODE, "2_ch_forgot");
                    intent.putExtra(ChangeDefaultPassword.KEY_PAN_HASH, EncryptionUtil.getHashValue(edtForgotPassPan.getText().toString().toUpperCase().trim(), EncryptionUtil.USER_HASH_SALT) );
                    startActivity(intent);
                } else {
                    DialogUtil.createAlertDialog(ForgotPasswordActivity.this, "Error", response, "Okay")
                            .show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.cancel();
                DialogUtil.createAlertDialog(ForgotPasswordActivity.this, "Error", "Something went wrong Please try again", "Okay")
                        .show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + panHash);
                headers.put("timestamp", Util.getTimestamp());
                return headers;
            }
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                String pasHash = EncryptionUtil.getHashValue(edtForgotPassPan.getText()
                        .toString().toUpperCase().trim(), EncryptionUtil.USER_HASH_SALT);
                String securityAnswer = edtForgotPassSecurityAnswer.getText().toString().trim();
                params.put("pan", pasHash); // pancard number hash
                params.put("a", securityAnswer); // security answer
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
