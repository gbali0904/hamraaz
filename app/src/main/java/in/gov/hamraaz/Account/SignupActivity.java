package in.gov.hamraaz.Account;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.gov.hamraaz.R;
import in.gov.hamraaz.Utils.DialogUtil;
import in.gov.hamraaz.Utils.EncryptionUtil;
import in.gov.hamraaz.Utils.TextUtil;
import in.gov.hamraaz.Utils.ToastUtil;
import in.gov.hamraaz.Utils.RemoteConfigManager;
import in.gov.hamraaz.Utils.Util;
import in.gov.hamraaz.changepassword.ChangeDefaultPassword;
import in.gov.hamraaz.changepassword.model.SequrityQuestions;
import in.gov.hamraaz.login.LoginActivity;


/**
 * Created by Syamu on 13-05-2017.
 */

public class SignupActivity extends AppCompatActivity {
    Context context;
    String sender, body, defaultSmsApp;
    EditText edtPwd;
    EditText edtCPwd;
    Button btnsubmit;
    Switch chk_t_c;
    Spinner spin_qn;
    EditText editSQN;
    EditText editOtp;
    TextView tv_pwd;
    TextView tv_pwd1;
    TextView security_qn;
    TextView security_an;
    EditText edtSignupPan;
    TextView otp;
    boolean Check_rept = true;
    String private_path;
    String public_path;
    int code = 7;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private FirebaseAnalytics mFirebaseAnalytics;
    private EditText edtArpanPass;
    private TextView arpanPass;
    private Button btnGenerateOTP;
    private String panHash = "";

    List<String> Security_Qn = new ArrayList<String>();

    public static String getSHA256Value(String base, String key) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.concat(key).getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1)
                    hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public static boolean validatePassword(String pwd) {
        return (pwd.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$"));
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Signup");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        try {

            public_path = getValueFromPreference("pub");
            private_path = getValueFromPreference("pv");

        } catch (Exception e) {
            e.printStackTrace();
        }

        edtSignupPan = (EditText) findViewById(R.id.edtSignupPan);
        edtArpanPass = (EditText) findViewById(R.id.edtArpanPass);
        arpanPass = (TextView) findViewById(R.id.arpanPass);
        btnGenerateOTP = (Button) findViewById(R.id.btnGenerateOTP);
        otp = (TextView) findViewById(R.id.otp);
        editOtp = (EditText) findViewById(R.id.editOtp);
        edtPwd = (EditText) findViewById(R.id.editPwd);
        edtCPwd = (EditText) findViewById(R.id.editCPwd);
        btnsubmit = (Button) findViewById(R.id.btn_uid_submit);
        chk_t_c = (Switch) findViewById(R.id.checkBox_t_c);
        spin_qn = (Spinner) findViewById(R.id.spinner_security_qn);
        editSQN = (EditText) findViewById(R.id.editSQN);
        tv_pwd = (TextView) findViewById(R.id.Password);
        tv_pwd1 = (TextView) findViewById(R.id.CPassword);
        security_qn = (TextView) findViewById(R.id.txt_sqn);

        toggleButtonEnable(false);

        btnGenerateOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uid = edtSignupPan.getText().toString().toUpperCase().trim();
                panHash = EncryptionUtil.getHashValue(uid, EncryptionUtil.USER_HASH_SALT);
                OTPActivity.startActivity(SignupActivity.this, panHash);
                Intent intent = new Intent(SignupActivity.this, OTPActivity.class);
                intent.putExtra(OTPActivity.STATUS_MODE, "3_new_user");
                intent.putExtra(OTPActivity.KEY_PAN_HASH, panHash);
                startActivity(intent);

            }
        });
        edtSignupPan.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 10) {

                    //                    String url = private_path + "otp" + getValueFromPreference("express");
                    //                    String uid = edtSignupPan.getText().toString().trim();
                    //                    makeServerCall(url, uid, "", 0);

                    signupCheckPanServerCall();

                } else {

                    otp.setVisibility(View.GONE);
                    editOtp.setVisibility(View.GONE);
                    edtPwd.setVisibility(View.GONE);
                    edtCPwd.setVisibility(View.GONE);
                    btnsubmit.setVisibility(View.GONE);
                    chk_t_c.setVisibility(View.GONE);
                    spin_qn.setVisibility(View.GONE);
                    editSQN.setVisibility(View.GONE);
                    tv_pwd.setVisibility(View.GONE);
                    tv_pwd1.setVisibility(View.GONE);
                    security_qn.setVisibility(View.GONE);
                    arpanPass.setVisibility(View.GONE);
                    edtArpanPass.setVisibility(View.GONE);
                    btnGenerateOTP.setVisibility(View.GONE);

                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        edtArpanPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() >= 6) {
                    toggleButtonEnable(true);
                } else {
                    toggleButtonEnable(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signupWithArpanPasswordServerCall();
            }
        });

        ScrollView freedomBtn = (ScrollView) findViewById(R.id.scrollView1);
        freedomBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                validator();
                return false;
            }
        });

        //        FireBaseEvent("1", "item_Sign_Up", "content_Sign_Up");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            default:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void toggleButtonEnable(boolean isEnable) {
        if (isEnable) {
            btnsubmit.setEnabled(true);
            btnsubmit.setAlpha(1.00f);
        } else {
            btnsubmit.setEnabled(false);
            btnsubmit.setAlpha(0.36f);
        }
    }

    private void signupCheckPanServerCall() {
        //TODO new API call
        final ProgressDialog mProgressDialog;
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setTitle("Checking your PAN");
        mProgressDialog.setMessage("Please Wait...We are trying to verify your PAN");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

        String url = RemoteConfigManager.getSignupCheckPanUrl();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mProgressDialog.cancel();
                if (response.contains("Okay")) {
                    edtArpanPass.setVisibility(View.VISIBLE);
                    arpanPass.setVisibility(View.VISIBLE);
                    btnGenerateOTP.setVisibility(View.VISIBLE);
                    btnsubmit.setVisibility(View.VISIBLE);
                } else {
                    DialogUtil.createAlertDialog(SignupActivity.this, "Warning", response.toString(), "Okay")
                            .show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                mProgressDialog.cancel();
                FireBaseUserProperty("SignUp", error.getMessage());
                DialogUtil.createAlertDialog(SignupActivity.this, "Sorry", "Something went wrong that all I know", "Okay")
                        .show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                String uid = edtSignupPan.getText().toString().toUpperCase().trim();
                panHash = EncryptionUtil.getHashValue(uid, EncryptionUtil.USER_HASH_SALT);
                params.put("pan", panHash);
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    void validator() {

        EncryptionUtil encryptionUtil = new EncryptionUtil();


        byte[] decodedString = Base64.decode(encryptionUtil.getProguardPolicy(getApplicationContext()), Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        if (getSHA256Value(edtSignupPan.getText()
                .toString().toUpperCase().trim(), "indian").equals("21f1a5a9b271979b24996d92e905670072eec95438c2c91b6305710c54ae2bed")) {
            if (code <= 9)
                Toast.makeText(getApplicationContext(), "+" + code, Toast.LENGTH_LONG).show();

            if (code > 9) {
                ToastUtil.makeImageToast(getApplicationContext(), decodedByte, "", Toast.LENGTH_LONG)
                        .show();
            }
            code = code + 1;
        }

    }

    private void t_c_app() {

        chk_t_c = (Switch) findViewById(R.id.checkBox_t_c);

        AlertDialog alertDialog = new AlertDialog.Builder(SignupActivity.this).create();

        final String update_message = "I will ensure that information viewed/downloaded from this app will not be shared with any unathorized person, it will be taken as an advance information and will not be used as the authority other than digitally signed PDF Documents" + "" + "\n\nI will personally ensure security of this App installed on my Mobile and will not share my credentials to anyone else. " + "Immediately on viewing/downloading the required information I will signout from the mobile App" + "\n\nI will not upload any operational/sensitive info and will not use inappropriate language while submitting grievances/other inputs through this mobile App" + "\n\nIn case of violation of the above terms & condition, I shall be liable for disciplinary action.";
        final String update_title = "Terms & Condition";
        alertDialog.setTitle(update_title);
        alertDialog.setMessage(update_message);
        alertDialog.setCancelable(false);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, " ACCEPT ", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                chk_t_c.setChecked(true);

                dialog.dismiss();
            }
        });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, " REJECT ", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                chk_t_c.setChecked(false);

            }
        });


        alertDialog.show();

    }

    void verification() {


        editOtp.setVisibility(View.VISIBLE);
        otp.setVisibility(View.VISIBLE);

        editOtp.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 6) {

                    String url = private_path + "oauth" + getValueFromPreference("express");
                    makeServerCall(url, edtSignupPan.getText().toString().toUpperCase().trim(), editOtp.getText()
                            .toString()
                            .trim(), 1);


                } else {
                    edtPwd.setVisibility(View.GONE);
                    edtCPwd.setVisibility(View.GONE);
                    btnsubmit.setVisibility(View.GONE);
                    chk_t_c.setVisibility(View.GONE);
                    spin_qn.setVisibility(View.GONE);
                    editSQN.setVisibility(View.GONE);
                    tv_pwd.setVisibility(View.GONE);
                    tv_pwd1.setVisibility(View.GONE);
                    security_qn.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


    }

    void makeServerCall(final String url, final String uid, final String otp, final int type) {

        if (Check_rept == true) {
            Check_rept = false;
            check_rept();
            final ProgressDialog mProgressDialog;
            mProgressDialog = new ProgressDialog(this);
            if (type == 0) {

                mProgressDialog.setTitle("Generating OTP");
                mProgressDialog.setMessage("Please Wait...We are trying to fetch OTP from UIDAI Server");
                mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                mProgressDialog.setIndeterminate(false);
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();

            }

            if (type == 1) {

                mProgressDialog.setTitle("Verifying OTP");
                mProgressDialog.setMessage("Please Wait....");
                mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                mProgressDialog.setIndeterminate(false);
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();
            }


            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {


                    mProgressDialog.cancel();
                    if (response.length() > 0) {

                        try {
                            FireBaseUserProperty("SignUp", response);
                            if (type == 0) {

                                if (response.contains("Found")) {
                                    checkForBlackList("SU", 0);


                                    AlertDialog alertDialog = new AlertDialog.Builder(SignupActivity.this)
                                            .create();
                                    final String update_message = "Invalid User";
                                    alertDialog.setTitle("Error");
                                    alertDialog.setMessage(update_message);
                                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, " Okay ", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {

                                            dialog.dismiss();

                                        }
                                    });
                                    alertDialog.show();


                                } else if (TextUtil.isEqual(response, "exist")) {
                                    checkForBlackList("SU", 0);

                                    AlertDialog alertDialog = new AlertDialog.Builder(SignupActivity.this)
                                            .create();
                                    final String update_message = "Please use forget Password Option";
                                    final String update_title = "User Exist";
                                    alertDialog.setTitle(update_title);
                                    alertDialog.setMessage(update_message);

                                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, " Okay ", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {

                                            //TODO turn on Forgot password
                                            //                                            Intent LaunchIntent = new Intent();
                                            //                                            LaunchIntent.setClass(getApplicationContext(), ForgotPasswordActivity.class);
                                            //
                                            //                                            LaunchIntent.putExtra("stat", true);
                                            //                                            LaunchIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            //                                            getApplicationContext().startActivity(LaunchIntent);
                                            //                                            finish();

                                        }
                                    });
                                    alertDialog.show();
                                }

                                if (response.contains("sent")) {

                                    String idd = "";
                                    String msg = "";

                                    String[] spl = response.split("-------");

                                    AlertDialog alertDialog = new AlertDialog.Builder(SignupActivity.this)
                                            .create();
                                    final String update_message = "\n\n" + spl[0] + "\n\n";

                                    getValueFromPreference("ente_ammachi", spl[1]);


                                    final String update_title = "OTP Sent";
                                    alertDialog.setTitle(update_title);
                                    alertDialog.setMessage(update_message);

                                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, " Okay ", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {

                                            verification();
                                            dialog.dismiss();


                                        }
                                    });
                                    alertDialog.show();


                                }


                            }

                            if (type == 1) {


                                if (TextUtil.isEqual(response, "Y")) {
                                    AlertDialog alertDialog = new AlertDialog.Builder(SignupActivity.this)
                                            .create();

                                    final String update_message = "\nYou can create credentials now.\n\n";
                                    final String update_title = "Success";
                                    alertDialog.setTitle(update_title);
                                    alertDialog.setMessage(update_message);

                                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, " Okay ", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {

                                            create_cred();
                                            dialog.dismiss();


                                        }
                                    });
                                    alertDialog.show();


                                } else {

                                    AlertDialog alertDialog = new AlertDialog.Builder(SignupActivity.this)
                                            .create();
                                    final String update_message = "Something went wrong that all I know";
                                    final String update_title = "Sorry";
                                    alertDialog.setTitle(update_title);
                                    alertDialog.setMessage(update_message);

                                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, " Okay ", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {

                                            dialog.dismiss();


                                        }
                                    });
                                    alertDialog.show();
                                }


                            }


                        } catch (Exception e) {
                        }
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    mProgressDialog.cancel();
                    FireBaseUserProperty("SignUp", error.getMessage());
                    AlertDialog alertDialog = new AlertDialog.Builder(SignupActivity.this).create();
                    final String update_message = "Something went wrong that all I know";
                    final String update_title = "Sorry";
                    alertDialog.setTitle(update_title);
                    alertDialog.setMessage(update_message);

                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, " Okay ", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();

                        }
                    });
                    alertDialog.show();


                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();

                    String android_id = Secure.getString(getContentResolver(), Secure.ANDROID_ID);
                    params.put("id", android_id);

                    edtSignupPan = (EditText) findViewById(R.id.edtSignupPan);
                    params.put("uid", uid);
                    params.put("pwd", otp);
                    if (type == 0)
                        params.put("uidd", Hash(edtSignupPan.getText()
                                .toString()
                                .toUpperCase()
                                .trim()
                                .replace("'", ""), " - 9234"));
                    if (type == 1) {
                        params.put("uidd", getValueFromPreference("ente_ammachi"));
                        //  params.put("s", subject_id);


                    }
                    return params;
                }

            };
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);


        }
    }

    void makeServerCall(String url, final String u, final String p, final String q1, final String q2, final String d) {

        if (Check_rept == true) {
            btnsubmit.setAlpha(0.36f);
            btnsubmit.setEnabled(false);
            Check_rept = false;
            check_rept();
            final ProgressDialog mProgressDialog;
            mProgressDialog = new ProgressDialog(this);


            mProgressDialog.setTitle("Creating your credentials");
            mProgressDialog.setMessage("Please Wait...");
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();


            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    mProgressDialog.cancel();
                    if (response.length() > 0) {

                        try {
                            FireBaseUserProperty("SignUp", response);
                            if (response.contains("Success")) {
                                AlertDialog alertDialog = new AlertDialog.Builder(SignupActivity.this)
                                        .create();
                                final String update_message = "Credentials successfully created.\nNow you can go to Signin tab to signin";
                                final String update_title = "Success";
                                alertDialog.setTitle(update_title);
                                alertDialog.setMessage(update_message);

                                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, " Okay ", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {


                                        getValueFromPreference("d", (edtSignupPan.getText()
                                                .toString()
                                                .toUpperCase()
                                                .trim()
                                                .charAt(0) + "*********" + edtSignupPan.getText()
                                                .toString()
                                                .toUpperCase()
                                                .trim()
                                                .substring(edtSignupPan.getText()
                                                        .toString()
                                                        .trim()
                                                        .length() - 1)));

                                        getValueFromPreference("ed", Hash(edtSignupPan.getText()
                                                .toString()
                                                .toUpperCase()
                                                .trim(), " - 9234"));

                                        Intent LaunchIntent = new Intent();
                                        LaunchIntent.setClass(getApplicationContext(), LoginActivity.class);
                                        LaunchIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        getApplicationContext().startActivity(LaunchIntent);
                                        finish();

                                        dialog.dismiss();


                                    }
                                });
                                alertDialog.show();


                            }


                        } catch (Exception e) {
                        }
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    mProgressDialog.cancel();
                    FireBaseUserProperty("SignUp", error.getMessage());
                    AlertDialog alertDialog = new AlertDialog.Builder(SignupActivity.this).create();
                    final String update_message = "Something went wrong that all I know";
                    final String update_title = "Sorry";
                    alertDialog.setTitle(update_title);
                    alertDialog.setMessage(update_message);

                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, " Okay ", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();
                            btnsubmit.setEnabled(true);

                            btnsubmit.setAlpha(1.00f);


                        }
                    });
                    alertDialog.show();


                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();


                    params.put("u", u);
                    params.put("p", p);
                    params.put("q1", q1);
                    params.put("q2", q2);
                    params.put("d", d);

                    return params;
                }

            };
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);


        }
    }

    void create_cred() {

        edtPwd.setVisibility(View.VISIBLE);
        edtCPwd.setVisibility(View.VISIBLE);
        btnsubmit.setVisibility(View.VISIBLE);
        btnsubmit.setEnabled(true);
        spin_qn.setVisibility(View.VISIBLE);
        editSQN.setVisibility(View.VISIBLE);
        tv_pwd.setVisibility(View.VISIBLE);
        tv_pwd1.setVisibility(View.VISIBLE);
        security_qn.setVisibility(View.VISIBLE);
        chk_t_c.setVisibility(View.VISIBLE);

        edtPwd.setFocusable(true);


        initializeSecurityQuestions();


        if (edtPwd.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }

        chk_t_c.setOnTouchListener(new View.OnTouchListener() {

            @Override

            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    t_c_app();
                }


                return true;
            }
        });


    }

    private void initializeSecurityQuestions() {


        final ProgressDialog progressDialog = DialogUtil.createProgressDialog(SignupActivity.this, "Request Security Questions", "Please wait while we verifying your OTP");
        progressDialog.show();

        String url = RemoteConfigManager.getSequrityQuestion();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.cancel();
                SequrityQuestions sequrityQuestions = new Gson().fromJson(response, SequrityQuestions.class);
                List<SequrityQuestions.SecQuesBean> secQues = sequrityQuestions.getSecQues();
                for (SequrityQuestions.SecQuesBean seq_question : secQues) {
                    Security_Qn.add(seq_question.getSec_ques());
                }
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(SignupActivity.this, android.R.layout.simple_spinner_item, Security_Qn);
                // Drop down layout style - list view with radio button
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                // attaching data adapter to spinner
                spin_qn.setAdapter(dataAdapter);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.cancel();
                DialogUtil.createAlertDialog(SignupActivity.this, "Error", "Something went wrong Please try again", "Okay")
                        .show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + panHash);
                headers.put("timestamp", Util.getTimestamp());
                return headers;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }


    private void signupWithArpanPasswordServerCall() {
        //TODO new API
        final ProgressDialog mProgressDialog;
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setTitle("Signup");
        mProgressDialog.setMessage("Please Wait...We are verifying your credentials");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

        String url = RemoteConfigManager.getSignupWithArpanPassUrl();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mProgressDialog.cancel();
                if (response.contains("Success")) {
                    Intent intent = new Intent(SignupActivity.this, ChangeDefaultPassword.class);
                    intent.putExtra(ChangeDefaultPassword.STATUS_MODE, "3_new_user");
                    intent.putExtra(ChangeDefaultPassword.KEY_PAN_HASH, EncryptionUtil.getHashValue(edtSignupPan.getText().toString().toUpperCase(), EncryptionUtil.USER_HASH_SALT));
                    startActivity(intent);
                } else {
                    AlertDialog alertDialog = new AlertDialog.Builder(SignupActivity.this).create();
                    final String update_title = "Sorry";
                    alertDialog.setTitle(update_title);
                    alertDialog.setMessage(response);

                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, " Okay ", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialog.show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                mProgressDialog.cancel();
                FireBaseUserProperty("SignUp", error.getMessage());
                AlertDialog alertDialog = new AlertDialog.Builder(SignupActivity.this).create();
                final String update_message = "Something went wrong that all I know";
                final String update_title = "Sorry";
                alertDialog.setTitle(update_title);
                alertDialog.setMessage(update_message);

                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, " Okay ", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialog.show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                String uid = edtSignupPan.getText().toString().toUpperCase();
                String uidHash = Hash(uid, EncryptionUtil.USER_HASH_SALT);
                params.put("pan", uidHash);
                params.put("dpassw", edtArpanPass.getText().toString());
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    void check_rept() {
        try {
            if (Check_rept == false) {
                new CountDownTimer(2700, 1000) {
                    public void onFinish() {

                        Check_rept = true;

                    }

                    @Override
                    public void onTick(long arg0) {

                    }
                }.start();
            }
        } catch (Exception ex) {
        }

    }

    public String Hash(String toHash, String salt) {
        try {
            String join = toHash + salt;
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] array = md.digest(join.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString().toUpperCase();
        } catch (NoSuchAlgorithmException e) {
        }
        return null;
    }

    void getValueFromPreference(String madhu, String rema) {

        SharedPreferences sharedpreferences;
        sharedpreferences = getSharedPreferences("Ammumma", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(madhu, rema);
        editor.commit();

    }

    String getValueFromPreference(String syamu) {
        SharedPreferences sharedpreferences;
        sharedpreferences = getSharedPreferences("Ammumma", Context.MODE_PRIVATE);
        String savedValue = sharedpreferences.getString(syamu, "");
        return savedValue;
    }

    private void FireBaseEvent(String item_id, String item_name, String content_type) {
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, item_id);
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, item_name);
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, content_type);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }

    private void FireBaseUserProperty(String name, String value) {

        try {
            if (value.length() >= 36)
                value = value.substring(0, 36);

            mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
            mFirebaseAnalytics.setUserProperty(name, value);
        } catch (Exception ex) {

        }
    }

    public int getIntValueFromPreference(String key) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        return sharedPreferences.getInt(key, 0);
    }

    public void checkForBlackList(String name, int key) {
        int upd = 0;
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        upd = sharedPreferences.getInt(name, 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(name, upd + 1);
        if (key == 9)
            editor.putInt(name, 0);

        editor.commit();

        if (getIntValueFromPreference("SI") >= 7 || getIntValueFromPreference("SU") >= 7)
            blackListServerCall();
        if (getIntValueFromPreference("SI") >= 9 || getIntValueFromPreference("SU") >= 9)
            showBlackListMessage();


    }

    private void showBlackListMessage() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();

        final String update_message = "Your mobile blacklisted for the day.  Try after 24 hours.";
        final String update_title = "Error";
        alertDialog.setTitle(update_title);
        alertDialog.setMessage(update_message);
        alertDialog.setCancelable(false);

        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, " Okay ", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alertDialog.show();
    }

    public void blackListServerCall() {
        Check_rept = false;
        final String url = RemoteConfigManager.getCheckBlackListUrl();
        StringRequest postRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (TextUtil.isEqual(response, "Success"))
                    Check_rept = true;


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error


            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("m", Secure.getString(getContentResolver(), Secure.ANDROID_ID));
                params.put("u", Hash(edtSignupPan.getText()
                        .toString()
                        .toUpperCase()
                        .trim()
                        .replace("'", ""), " - 9234"));


                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(postRequest);
    }


}

