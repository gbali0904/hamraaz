
package in.gov.hamraaz.Account.model;

public class OtpModel {

    private Sk sk;
    public Sk getSk() {
        return sk;
    }

    public void setSk(Sk sk) {
        this.sk = sk;
    }

    public class Sk {

        private long code;
        private Boolean error;
        private String msg;
        private String txn;


        public long getCode() {
            return code;
        }

        public void setCode(long code) {
            this.code = code;
        }

        public Boolean getError() {
            return error;
        }

        public void setError(Boolean error) {
            this.error = error;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public String getTxn() {
            return txn;
        }

        public void setTxn(String txn) {
            this.txn = txn;
        }
    }

}
