package in.gov.hamraaz.Account;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

import java.util.HashMap;
import java.util.Map;

import in.gov.hamraaz.BuildConfig;
import in.gov.hamraaz.R;
import in.gov.hamraaz.Utils.DialogUtil;
import in.gov.hamraaz.Utils.TextUtil;
import in.gov.hamraaz.Utils.RemoteConfigManager;
import in.gov.hamraaz.login.LoginActivity;

/**
 * Created by Syamu on 13-05-2017.
 */

public class AccountLandingActivity extends Activity {
    private FirebaseAnalytics mFirebaseAnalytics;
    private FirebaseRemoteConfig mFirebaseRemoteConfig;
    Button btnSignUp;
    Button btn_signin;
    String pub;
    String pv;
    String express;
    String version;
    ProgressDialog mProgressDialog;
    private TextView tv_app_version;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.sign_option);

        //Log firebase event
        logFirebaseEvent("0", "item_Sign_Option", "content_Sign_Option");

        //initialize buttons
        init();


        //check current version from firebase
        checkCurrentVersion();
    }

    private void init() {

        btnSignUp = (Button) findViewById(R.id.btn_signup);
        btn_signin = (Button) findViewById(R.id.btn_signin);
        tv_app_version = (TextView) findViewById(R.id.tv_app_version);

        setVersionText();

        toggleButtonEnable(false);

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchActivity(SignupActivity.class);
            }
        });
        btn_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchActivity(LoginActivity.class);
            }
        });
    }

    private void setVersionText() {
        tv_app_version.setText(String.format("Version %s",BuildConfig.VERSION_NAME));
    }

    private void launchActivity(Class<?> className) {
        startActivity(new Intent(AccountLandingActivity.this, className));
    }

    private void logFirebaseEvent(String item_id, String item_name, String content_type) {
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, item_id);
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, item_name);
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, content_type);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }

    public void checkBlackList() {

        final String url = RemoteConfigManager.getCheckBlackListUrl();
        StringRequest postRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (TextUtil.isEqual(response, "0")) {//success
                    //make buttons enable
                    toggleButtonEnable(true);
                } else {// error
                    showBlackListDialog();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("check blacklist", error.getMessage());
                error.getCause().printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                String androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                params.put("m", androidId);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(postRequest);
    }

    private void checkCurrentVersion() {
        double currentVersion = Double.parseDouble(BuildConfig.VERSION_NAME);
        double latestVersion = RemoteConfigManager.getConfigAppVersion();
        final String latestAppLink = RemoteConfigManager.getConfigAppLink();

        mProgressDialog = DialogUtil.createProgressDialog(AccountLandingActivity.this, "Authorising Version Integrity", "Please Wait...");
        mProgressDialog.show();

        mProgressDialog.dismiss();
        if(currentVersion < latestVersion){// need to download latest version
            DialogUtil.createAlertDialog(AccountLandingActivity.this, "Version Integrity failed", "Please download latest version to use this App.  Touch OKAY to proceed.", "Okay", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    try {
                        Uri uri = Uri.parse(latestAppLink);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    } catch (Exception Ex) {
                    }
                }
            }).show();
        }else {
            checkBlackList();
        }
    }

    private void toggleButtonEnable(boolean isEnable) {
        btn_signin.setEnabled(isEnable);
        btnSignUp.setEnabled(isEnable);
        float alpha = (isEnable) ? 1.00f : 0.27f;
        btn_signin.setAlpha(alpha);
        btnSignUp.setAlpha(alpha);
    }

    private void showBlackListDialog() {
        DialogUtil.createAlertDialog(AccountLandingActivity.this, "Error", "Your mobile blacklisted for the day.  Try after 24 hours.", "Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        }).show();
    }


    public static void startActivity(Context context) {
        Intent intent = new Intent(context, AccountLandingActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }
}

