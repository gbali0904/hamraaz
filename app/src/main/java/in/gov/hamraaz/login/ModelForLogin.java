package in.gov.hamraaz.login;

public class ModelForLogin {


    /**
     * ChkCrd : {"code":1,"error":true,"msg":"Success_Topic_Id_37","timestamp":34550165,"postMenu":false,"jwt":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IkQ4fkN-fjMxMUI0LTEwM18zX0IwNjBCMDNfLTFEM0M2QzZEREFFXkYiLCJuYmYiOjE1ODQxNzg1NTAsImV4cCI6MTU4NDE4NTc1MCwiaWF0IjoxNTg0MTc4NTUwfQ.9pltSke4Ej8SWLM_xjiDbWv-x5bHKO2gbu_fdm-6Zn8"}
     */

    private ChkCrdBean ChkCrd;

    public ChkCrdBean getChkCrd() {
        return ChkCrd;
    }

    public void setChkCrd(ChkCrdBean ChkCrd) {
        this.ChkCrd = ChkCrd;
    }

    public static class ChkCrdBean {
        /**
         * code : 1
         * error : true
         * msg : Success_Topic_Id_37
         * timestamp : 34550165
         * postMenu : false
         * jwt : eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IkQ4fkN-fjMxMUI0LTEwM18zX0IwNjBCMDNfLTFEM0M2QzZEREFFXkYiLCJuYmYiOjE1ODQxNzg1NTAsImV4cCI6MTU4NDE4NTc1MCwiaWF0IjoxNTg0MTc4NTUwfQ.9pltSke4Ej8SWLM_xjiDbWv-x5bHKO2gbu_fdm-6Zn8
         */

        private int code;
        private boolean error;
        private String msg;
        private int timestamp;
        private boolean postMenu;
        private String jwt;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public boolean isError() {
            return error;
        }

        public void setError(boolean error) {
            this.error = error;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public int getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(int timestamp) {
            this.timestamp = timestamp;
        }

        public boolean isPostMenu() {
            return postMenu;
        }

        public void setPostMenu(boolean postMenu) {
            this.postMenu = postMenu;
        }

        public String getJwt() {
            return jwt;
        }

        public void setJwt(String jwt) {
            this.jwt = jwt;
        }
    }
}
