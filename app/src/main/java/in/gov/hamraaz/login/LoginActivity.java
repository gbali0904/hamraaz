package in.gov.hamraaz.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;

import java.security.MessageDigest;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import in.gov.hamraaz.Account.ForgotPasswordActivity;
import in.gov.hamraaz.Account.SignupActivity;
import in.gov.hamraaz.App;
import in.gov.hamraaz.BuildConfig;
import in.gov.hamraaz.Home.MainActivity;
import in.gov.hamraaz.R;
import in.gov.hamraaz.Utils.DialogUtil;
import in.gov.hamraaz.Utils.EncryptionUtil;
import in.gov.hamraaz.Utils.FcmManager;
import in.gov.hamraaz.Utils.RemoteConfigManager;
import in.gov.hamraaz.Utils.TAGs;
import in.gov.hamraaz.Utils.TextUtil;
import in.gov.hamraaz.Utils.ToastUtil;
import in.gov.hamraaz.Utils.Util;

/**
 * Created by Syamu on 13-05-2017.
 */

public class LoginActivity extends AppCompatActivity {
    Boolean Check_rept = true;
    Button btnLogin;
    EditText editUid;
    TextView tv_forget;
    EditText editPwd;
    int code = 7;
    private TextView tv_signUp;
    private TextView tv_app_version;

    public static boolean checkPasswordPattern(String value) {
        return (value.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$"));
    }

    public static String getSHA256For(String base, String key) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.concat(key).getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1)
                    hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signin);
        ButterKnife.bind(this);


        editPwd = (EditText) findViewById(R.id.editPassword);
        editUid = (EditText) findViewById(R.id.editAadhar);
        btnLogin = (Button) findViewById(R.id.buttonSignIn);
        tv_forget = (TextView) findViewById(R.id.tv_forget);
        tv_signUp = (TextView) findViewById(R.id.tv_signUp);
        tv_app_version = (TextView) findViewById(R.id.tv_app_version);

        setVersionText();


        toggleEnable(false);

        editUid.setText(getValueFromPreference("d"));

        editUid.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (btnLogin.isEnabled()) {
                    toggleEnable(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().length() == 10) {
                    toggleEnable(true);
                }
            }
        });

        tv_forget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });
        tv_signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, SignupActivity.class);
                startActivity(intent);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            private String url="";

            @Override
            public void onClick(View view) {

                if (!Util.checkInternet(LoginActivity.this)) {
                    url = "https://10.0.0.161/API_Hamraaz_App/api/DataSend/CheckCreden";

                } else {
                    url = RemoteConfigManager.getCheckPanUrl();
                }


                if (App.getInstance().getPreference().getBoolean(TAGs.BLACK_LIST, false)) {
                    showBlackListMessage();
                } else {
                    if (editPwd.getText().toString().length() > 2) {
                        if (TextUtil.isEqual(getValueFromPreference("d"), editUid.getText()
                                .toString()
                                .toUpperCase()
                                .trim())) {

                            checkUserFromServer(url, EncryptionUtil.getHashValue(editUid.getText()
                                    .toString()
                                    .toUpperCase()
                                    .trim(), EncryptionUtil.USER_HASH_SALT), EncryptionUtil.getHashValue(editPwd
                                    .getText()
                                    .toString(), EncryptionUtil.USER_HASH_SALT), "");
                        } else {
                            String pan = EncryptionUtil.getHashValue(editUid.getText()
                                    .toString()
                                    .toUpperCase()
                                    .trim(), EncryptionUtil.USER_HASH_SALT);

                            String randomNumber = EncryptionUtil.genrateRandomNumber();
                            String passw = EncryptionUtil.getHashValue(editPwd.getText()
                                    .toString(), EncryptionUtil.USER_HASH_SALT);
                            checkUserFromServer(url, pan, passw, randomNumber);

                        }

                    } else
                        Toast.makeText(getApplication(), "Please Enter Password", Toast.LENGTH_LONG)
                                .show();

                }

            }
        });
        Button freedomBtn = (Button) findViewById(R.id.buttonSignIn);
        freedomBtn.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View arg0) {
                validator();
                return false;
            }
        });

    }



    @Override
    protected void onResume() {
        super.onResume();
        if (App.getInstance().getPreference().getBoolean(TAGs.BLACK_LIST, false)) {
            long previousTime = App.getInstance().getPreference().getLong(TAGs.BLACK_LIST_TIME, 0L);
            long currentTime = new Date().getTime();
            Log.e("this", "time:::" + (currentTime - previousTime) + "\t cal time::" + (60 * 1000));
            // 30*60*1000 - 30 min, each with 60 sec, each with 1000 millisec
            if (currentTime - previousTime < 60 * 1000) {
                App.getInstance().getPreferenceEditor().putBoolean(TAGs.BLACK_LIST, true).commit();

            } else {
                App.getInstance().getPreferenceEditor().putBoolean(TAGs.BLACK_LIST, false).commit();
            }
        } else {
            App.getInstance().getPreferenceEditor().putBoolean(TAGs.BLACK_LIST, false).commit();
        }
    }

    private void setVersionText() {
        tv_app_version.setText(String.format("Version %s", BuildConfig.VERSION_NAME));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            default:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void toggleEnable(boolean isEnabled) {
        btnLogin.setEnabled(isEnabled);
        float alpha = (isEnabled) ? 1.00f : 0.27f;
        btnLogin.setAlpha(alpha);
    }

    void getValueFromPreference(String key, String value) {
        SharedPreferences sharedpreferences;
        sharedpreferences = getSharedPreferences("Ammumma", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    String getValueFromPreference(String syamu) {
        SharedPreferences sharedpreferences;
        sharedpreferences = getSharedPreferences("Ammumma", Context.MODE_PRIVATE);
        String savedValue = sharedpreferences.getString(syamu, "");
        return savedValue;
    }

    void checkUserFromServer(String url, final String pan, final String password, final String randomNumber) {
        getValueFromPreference("ed", pan.trim());
        if (Check_rept == true) {
            btnLogin.setAlpha(0.36f);
            btnLogin.setEnabled(false);
            Check_rept = false;
            check_rept();
            final ProgressDialog mProgressDialog = DialogUtil.createProgressDialog(LoginActivity.this, "Trying to Login", "Please Wait...");
            mProgressDialog.show();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(final String response) {
                    final ModelForLogin modelForLogin = new Gson().fromJson(response, ModelForLogin.class);
                    Log.e("this", "response::" + new Gson().toJson(modelForLogin));

                    mProgressDialog.cancel();
                    if (modelForLogin.getChkCrd().getCode() == 1) {
                        App.getInstance().getPreferenceEditor().putBoolean(TAGs.BLACK_LIST, false).commit();
                        String fcm_topic = modelForLogin.getChkCrd().getMsg();
                        FirebaseMessaging.getInstance().subscribeToTopic(fcm_topic)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (!task.isSuccessful()) {
                                            Log.e("FCM", "topic registration failed");
                                            DialogUtil.createAlertDialog(LoginActivity.this, "Error", "Something went wrong Please try again", "Okey").show();
                                            return;
                                        }
                                        moveToNextPage(response, modelForLogin, pan);
                                    }
                                });

                    } else if (modelForLogin.getChkCrd().getCode() == 4) {
                        App.getInstance().getPreferenceEditor().putBoolean(TAGs.BLACK_LIST, true).commit();
                        long blackcurrentTime = new Date().getTime();
                        App.getInstance().getPreferenceEditor().putLong(TAGs.BLACK_LIST_TIME, blackcurrentTime).commit();
                        App.getInstance().getPreferenceEditor().putBoolean(TAGs.LOGIN_STATUS, false).commit();
                        AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
                        final String update_message = modelForLogin.getChkCrd().getMsg();
                        final String update_title = "Error";
                        alertDialog.setTitle(update_title);
                        alertDialog.setMessage(update_message);
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, " Okay ", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                toggleEnable(false);
                                finish();
                            }
                        });
                        alertDialog.show();

                    } else {
                        App.getInstance().getPreferenceEditor().putBoolean(TAGs.BLACK_LIST, false).commit();
                        AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this)
                                .create();
                        final String update_message = modelForLogin.getChkCrd().getMsg();
                        final String update_title = "Error";
                        alertDialog.setTitle(update_title);
                        alertDialog.setMessage(update_message);

                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, " Okay ", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();
                                btnLogin.setEnabled(true);
                                btnLogin.setAlpha(1.00f);


                            }
                        });
                        alertDialog.show();

                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    mProgressDialog.cancel();
                    App.getInstance().getPreferenceEditor().putBoolean(TAGs.BLACK_LIST, false).commit();
                    App.getInstance().getPreferenceEditor().putBoolean(TAGs.LOGIN_STATUS, false).commit();
                    //                    FireBaseUserProperty("SignIn", error.getMessage().toString());
                    AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
                    final String update_message = "Something went wrong that all I know";
                    final String update_title = "Sorry";
                    alertDialog.setTitle(update_title);
                    alertDialog.setMessage(update_message);

                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, " Okay ", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();
                            btnLogin.setEnabled(true);

                            btnLogin.setAlpha(1.00f);


                        }
                    });
                    alertDialog.show();


                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();

                    params.put("schemeFrame", pan);
                    params.put("pw", password);
                    params.put("fcmId", FcmManager.getInstance().getToken());

                    String androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                    params.put("Device_Id", androidId);

                    String ipAddress = Util.getIPAddress(true);
                    params.put("IP_Address", ipAddress);
                    // params.put("timestamp", randomNumber);


                    Log.e("this", "data::" + new Gson().toJson(params));


                    return params;
                }

            };
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }
    }

    private void moveToNextPage(String response, ModelForLogin modelForLogin, String pan) {
        App.getInstance().getPreferenceEditor().putBoolean(TAGs.LOGIN_STATUS, true).commit();
        String cake[] = response.split("Success");
        App.getInstance().getPreferenceEditor().putString(TAGs.SUCCESS_RESPONSE, cake[1]).commit();
        Intent LaunchIntent = new Intent();
        LaunchIntent.setClass(getApplicationContext(), MainActivity.class);
        LaunchIntent.putExtra("random", cake[1]);
        LaunchIntent.putExtra("stat", true);
        String panHash = EncryptionUtil.getHashValue(editUid.getText()
                .toString()
                .toUpperCase()
                .trim(), EncryptionUtil.USER_HASH_SALT);
        App.getInstance().getPreferenceEditor().putString(TAGs.KEY_SCHEME_FRAME, panHash).commit();
        App.getInstance().getPreferenceEditor().putString(TAGs.KEY_PAN_HASH, modelForLogin.getChkCrd().getJwt()).commit();
        App.getInstance().getPreferenceEditor().putString(TAGs.KEY_PAN_HASH_VALUE, pan).commit();
        if (checkPasswordPattern(editPwd.getText().toString())) {
            LaunchIntent.putExtra("pwdcomp", true);
        }
        LaunchIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getApplicationContext().startActivity(LaunchIntent);
        finish();
    }

    void check_rept() {
        try {
            if (Check_rept == false) {
                new CountDownTimer(2700, 1000) {
                    public void onFinish() {
                        Check_rept = true;
                    }

                    @Override
                    public void onTick(long arg0) {
                    }
                }.start();
            }
        } catch (Exception ex) {
        }
    }

    private void showBlackListMessage() {
        DialogUtil.createAlertDialog(LoginActivity.this, "Error", "Your mobile blacklisted for the day.  Try after 24 hours.", "Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        }).show();
    }

    void validator() {

        EncryptionUtil encryptionUtil = new EncryptionUtil();

        byte[] decodedString = Base64.decode(encryptionUtil.getProguardPolicy(getApplicationContext()), Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        if (getSHA256For(editPwd.getText()
                .toString(), "indian").equals("21f1a5a9b271979b24996d92e905670072eec95438c2c91b6305710c54ae2bed")) {
            if (code <= 9)
                Toast.makeText(getApplicationContext(), "+" + code, Toast.LENGTH_LONG).show();

            if (code > 9) {
                ToastUtil.makeImageToast(getApplicationContext(), decodedByte, "", Toast.LENGTH_LONG)
                        .show();
            }
            code = code + 1;
        }

    }

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }


}
