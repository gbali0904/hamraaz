package in.gov.hamraaz.Grievance;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import in.gov.hamraaz.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class GrievanceTermsFragment extends Fragment {

    private static String pan_hash;
    private static String status;
    @BindView(R.id.btnGrievanceTermsDeny) Button btnGrievanceTermsDeny;
    @BindView(R.id.btnGrievanceTermsAccept) Button btnGrievanceTermsAccept;
    Unbinder unbinder;

    public GrievanceTermsFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_grievance_terms, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.btnGrievanceTermsDeny, R.id.btnGrievanceTermsAccept})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnGrievanceTermsDeny:
                getActivity().onBackPressed();
                break;
            case R.id.btnGrievanceTermsAccept:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.containerGrievance, LodgeGrievanceFragment.newInstance(pan_hash,status)).addToBackStack(LodgeGrievanceFragment.TAG).commit();
                break;
        }
    }

    public static GrievanceTermsFragment newInstance(String pan_hash, String status) {
        GrievanceTermsFragment.pan_hash = pan_hash;
        GrievanceTermsFragment.status = status;
        return new GrievanceTermsFragment();
    }
}
