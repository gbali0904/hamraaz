package in.gov.hamraaz.Grievance;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import in.gov.hamraaz.Grievance.model.GrievanceType;

/**
 * Created by Pallaw Pathak on 28/10/18. - https://www.linkedin.com/in/pallaw-pathak-a6a324a1/
 */

public class GrievanceTypeAdapter extends ArrayAdapter<String>{
    // Your sent context
    private Context context;
    // Your custom values for the spinner (User)
    private ArrayList<String> values;
    private TextView status;

    public GrievanceTypeAdapter(Context context, int textViewResourceId,
                                ArrayList<String> values) {
        super(context, textViewResourceId, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public int getCount(){
        return values.size();
    }

    @Override
    public String  getItem(int position){
        return values.get(position);
    }

    @Override
    public long getItemId(int position){
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // I created a dynamic TextView here, but you can reference your own  custom layout for each spinner item
            TextView label = (TextView) super.getView(position, convertView, parent);
            label.setTextColor(Color.BLACK);
            // Then you can get the current item using the values array (Users array) and the current position
            // You can NOW reference each method you has created in your bean object (User class)
            label.setText(values.get(position));
            status= label;

        return status ;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(values.get(position));
        return label;
    }
}

