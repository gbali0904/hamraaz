package in.gov.hamraaz.Grievance.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Pallaw Pathak on 28/10/18. - https://www.linkedin.com/in/pallaw-pathak-a6a324a1/
 */

public class GrievanceType {
    /**
     * grev_type_cd : 1
     * grev_tye_desc : Pay Related Issues
     */

    @SerializedName("grev_type_cd")
    private String grev_type_cd;
    @SerializedName("grev_tye_desc")
    private String grev_tye_desc;
    @SerializedName("menu_status")
    private String menu_status;

    public String getGrev_type_cd() {
        return grev_type_cd;
    }

    public void setGrev_type_cd(String grev_type_cd) {
        this.grev_type_cd = grev_type_cd;
    }

    public String getGrev_tye_desc() {
        return grev_tye_desc;
    }

    public void setGrev_tye_desc(String grev_tye_desc) {
        this.grev_tye_desc = grev_tye_desc;
    }

    public String getMenu_status() {
        return menu_status;
    }

    public void setMenu_status(String menu_status) {
        this.menu_status = menu_status;
    }

}
