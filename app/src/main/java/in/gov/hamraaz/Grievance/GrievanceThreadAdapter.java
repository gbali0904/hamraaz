package in.gov.hamraaz.Grievance;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.gov.hamraaz.Grievance.model.GrievanceThread;
import in.gov.hamraaz.R;

/**
 * Created by Pallaw Pathak on 31/10/18. - https://www.linkedin.com/in/pallaw-pathak-a6a324a1/
 */

public class GrievanceThreadAdapter extends RecyclerView.Adapter<GrievanceThreadAdapter.MyViewHolder> {
    private ArrayList<GrievanceThread> mDataset = new ArrayList<>();


    public GrievanceThreadAdapter(ArrayList<GrievanceThread> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_grievance, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.bindData(mDataset.get(position));

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtItemTitle) TextView txtItemGrievanceId;
        @BindView(R.id.txtItemDate) TextView txtItemGrievanceDate;
        @BindView(R.id.txtItemMsgTitle) TextView txtItemGrievance;
        @BindView(R.id.txtItemMsg) TextView txtItemGrievanceReply;
        public MyViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        public void bindData(GrievanceThread grievanceThread) {
            txtItemGrievanceId.setText(String.format("Grievance ID: %s",grievanceThread.getGrev_id()));
            txtItemGrievanceDate.setText(getGrivanceDate(grievanceThread));
            txtItemGrievance.setText(grievanceThread.getL_Query());
            txtItemGrievanceReply.setText(getGrievanceReply(grievanceThread));
        }

        private String getGrievanceReply(GrievanceThread grievanceThread) {
            return (null == grievanceThread.getReply_RO())?"No Reply yet":grievanceThread.getReply_RO();
        }

        private String getGrivanceDate(GrievanceThread grievanceThread) {
            String date ="";
            if(null != grievanceThread.getL_Query_Dt()){
                if(!(TextUtils.isEmpty(grievanceThread.getL_Query_Dt()))){
                    date = String.format("Lodging Date: %s", grievanceThread.getL_Query_Dt());
                }
            }

            if(null != grievanceThread.getReply_RO_dt()){
                if(!(TextUtils.isEmpty(grievanceThread.getReply_RO_dt()))){
                    date = String.format(", Reply Date: %s", grievanceThread.getReply_RO_dt());
                }
            }
            return date;
        }
    }
}
