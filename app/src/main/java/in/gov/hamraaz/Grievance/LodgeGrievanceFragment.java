package in.gov.hamraaz.Grievance;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import in.gov.hamraaz.App;
import in.gov.hamraaz.Grievance.adapter.FeedbackAdapter;
import in.gov.hamraaz.Grievance.model.FeedbackModel;
import in.gov.hamraaz.Grievance.model.GrievanceType;
import in.gov.hamraaz.R;
import in.gov.hamraaz.Utils.DialogUtil;
import in.gov.hamraaz.Utils.RemoteConfigManager;
import in.gov.hamraaz.Utils.TAGs;
import in.gov.hamraaz.Utils.Util;

public class LodgeGrievanceFragment extends Fragment {
    public static final String TAG = LodgeGrievanceFragment.class.getSimpleName();
    private static String pan_hash;
    private static String status;
    @BindView(R.id.spiGrievanceType)
    Spinner spiGrievanceType;
    Unbinder unbinder;
    @BindView(R.id.edtGrievance)
    EditText edtGrievance;
    @BindView(R.id.btnGrievanceSend)
    Button btnGrievanceSend;
    @BindView(R.id.framelay)
    FrameLayout framelay;
    @BindView(R.id.layAFPP)
    LinearLayout layAFPP;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.feedback_recy)
    RecyclerView recyclerview;
    @BindView(R.id.feedbackLay)
    LinearLayout feedbackLay;
    private boolean isAFPPSelected;
    private boolean isfeedbackSelected;
    private String grievanceTypeId;
    private ArrayList<String> grievanceArrayName = new ArrayList<>();
    private ArrayList<String> grievanceArrayID = new ArrayList<String>();
    FeedbackAdapter paoPtoAdapter;

    public LodgeGrievanceFragment() {
        // Required empty public constructor
    }

    public static LodgeGrievanceFragment newInstance(String pan_hash, String status) {
        LodgeGrievanceFragment.pan_hash = pan_hash;
        LodgeGrievanceFragment.status = status;
        LodgeGrievanceFragment fragment = new LodgeGrievanceFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_lodge_grievance, container, false);
        unbinder = ButterKnife.bind(this, view);
        if (status.equals("1")) {
            framelay.setVisibility(View.GONE);
            feedbackLay.setVisibility(View.GONE);
            switchInputMethodForGrievance(11);
            grievanceTypeId = "11";
            layAFPP.setVisibility(View.VISIBLE);
            title.setText("AFPP SUBSCRIPTION");
            title.setVisibility(View.VISIBLE);

        } else if (status.equals("2")) {
            framelay.setVisibility(View.GONE);
            switchInputMethodForGrievance(14);
            grievanceTypeId = "14";
            layAFPP.setVisibility(View.GONE);
            title.setText("Feedback on Hamraaz App");
            title.setVisibility(View.VISIBLE);
            feedbackLay.setVisibility(View.VISIBLE);

            paoPtoAdapter = new FeedbackAdapter(getActivity());
            LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(getActivity());
            recyclerview.setLayoutManager(horizontalLayoutManagaer);
            recyclerview.setAdapter(paoPtoAdapter);
            callFeedback(RemoteConfigManager.getFeedbackURL());

        } else {
            callGrievance();
            framelay.setVisibility(View.VISIBLE);
            layAFPP.setVisibility(View.GONE);
            feedbackLay.setVisibility(View.GONE);
            title.setVisibility(View.GONE);
        }
        return view;
    }

    private void callFeedback(String feedbackURL) {
        final ProgressDialog progress = DialogUtil.createProgressDialog(getActivity(), "Loading", "Please wait");
        progress.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, feedbackURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progress.dismiss();
                if (response.length() > 0) {
                    FeedbackModel feedbackModels = new Gson().fromJson(response, FeedbackModel.class);
                    Log.e("this", "Data::" + new Gson().toJson(feedbackModels));
                    paoPtoAdapter.setData(feedbackModels.getFeedbackSugg());

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + pan_hash);
                headers.put("timestamp", Util.getTimestamp());
                return headers;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void callGrievance() {
        downloadGrievanceType();
    }

    private void switchInputMethodForGrievance(int position) {
        if (position == 11) { //AFPP subscription
            isAFPPSelected = true;
            isfeedbackSelected = false;
            edtGrievance.setText("");
            edtGrievance.setFilters(new InputFilter[]{new InputFilter.LengthFilter(5)});
            edtGrievance.setHint("Enter the amount only E.G 10000");
            edtGrievance.setInputType(InputType.TYPE_CLASS_NUMBER);
        } else if (position == 14) { //FeedBack
            isAFPPSelected = false;
            isfeedbackSelected = true;
            edtGrievance.setFilters(new InputFilter[]{new InputFilter.LengthFilter(150)});
            edtGrievance.setHint("Enter your grievance in 150 characters");
            edtGrievance.setInputType(InputType.TYPE_CLASS_TEXT);
        } else {
            isAFPPSelected = false;
            isfeedbackSelected = false;
            edtGrievance.setFilters(new InputFilter[]{new InputFilter.LengthFilter(150)});
            edtGrievance.setHint("Enter your grievance in 150 characters");
            edtGrievance.setInputType(InputType.TYPE_CLASS_TEXT);
        }
    }

    private void downloadGrievanceType() {
        final ProgressDialog progress = DialogUtil.createProgressDialog(getActivity(), "Loading", "Please wait");
        progress.show();
        final String url = RemoteConfigManager.getGrievanceTypeUrl();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.length() > 0) {
                    try {
                        progress.dismiss();

                        Gson gson = new Gson();
                        Type type = new TypeToken<List<GrievanceType>>() {
                        }.getType();
                        final ArrayList<GrievanceType> grievanceTypes = gson.fromJson(response, type);
                        for (GrievanceType type1 :
                                grievanceTypes) {
                            if (type1.getMenu_status().equalsIgnoreCase("A")) {
                                grievanceArrayName.add(type1.getGrev_tye_desc());
                                grievanceArrayID.add(type1.getGrev_type_cd());
                            }
                        }
                        GrievanceTypeAdapter adapter = new GrievanceTypeAdapter(getActivity(), android.R.layout.simple_spinner_item, grievanceArrayName);
                        spiGrievanceType.setAdapter(adapter);
                        spiGrievanceType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                switchInputMethodForGrievance(Integer.parseInt(grievanceArrayID.get(position)));
                                grievanceTypeId = grievanceArrayID.get(position);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                            }
                        });

                    } catch (Exception e) {
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.getCause().printStackTrace();
                progress.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("schemeFrame", pan_hash);
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btnGrievanceSend)
    public void onViewClicked() {
        String grievance = edtGrievance.getText().toString();
        if (isAFPPSelected) {
            if (TextUtils.isEmpty(grievance) || grievance.length() < 3) {
                edtGrievance.setError("Please enter minimum 3 digit");
                Toast.makeText(getActivity(), "Please enter minimum 3 digit", Toast.LENGTH_LONG).show();
            } else {
                postGrievance(RemoteConfigManager.getAFPPGrievanceUrl());
            }
        } else if (isfeedbackSelected) {
            if (TextUtils.isEmpty(grievance) || grievance.length() < 20) {
                edtGrievance.setError("Please write your grievance in atleast 20 characters");
                Toast.makeText(getActivity(), "Please write your grievance in atleast 20 characters", Toast.LENGTH_LONG).show();
            } else {
                postFeedbackGrievance(RemoteConfigManager.getFeedbackGrievanceUrl());
            }
        } else {
            if (TextUtils.isEmpty(grievance) || grievance.length() < 20) {
                edtGrievance.setError("Please write your grievance in atleast 20 characters");
                Toast.makeText(getActivity(), "Please write your grievance in atleast 20 characters", Toast.LENGTH_LONG).show();
            } else {
                postGrievance(RemoteConfigManager.getLodgeGrievanceUrl());
            }
        }

    }

    private void postFeedbackGrievance(String url) {
        final ProgressDialog progress = DialogUtil.createProgressDialog(getActivity(), "Loading", "Please wait");
        progress.show();
        final String grievanceText = edtGrievance.getText().toString();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progress.dismiss();
                if (response.length() > 0) {
                    if (response.contains("Okay")) {
                        DialogUtil.createAlertDialog(getActivity(), "Grievance lodged", "We have received you grievance. Your grievance id is " + response.substring(4, response.length() - 1) + ". We will get back to you as soon as possible", "Okey", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getActivity().finish();
                            }
                        }).show();
                    } else {
                        DialogUtil.createAlertDialog(getActivity(), "Error", response, "Okay").show();
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.getCause().printStackTrace();
                progress.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("p_hash", App.getInstance().getPreference().getString(TAGs.KEY_SCHEME_FRAME, ""));// pan hash
                params.put("l_query", grievanceText);// grievance text
                params.put("query_type", grievanceTypeId);// grievance type id
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void postGrievance(String url) {
        final ProgressDialog progress = DialogUtil.createProgressDialog(getActivity(), "Loading", "Please wait");
        progress.show();
        final String grievanceText = edtGrievance.getText().toString();

        Log.e("this", "grievance url :::" + url);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progress.dismiss();
                if (response.length() > 0) {
                    if (status.equals("0")) {
                        DialogUtil.createAlertDialog(getActivity(), "Grievance lodged", "We have received you grievance. Your grievance id is " + response + ". We will get back to you as soon as possible", "Okey", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getActivity().finish();
                            }
                        }).show();
                    } else {
                        if (response.contains("Okay")) {
                            DialogUtil.createAlertDialog(getActivity(), "Grievance lodged", "We have received you grievance. Your grievance id is " + response.substring(4, response.length() - 1) + ". We will get back to you as soon as possible", "Okey", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getActivity().finish();
                                }
                            }).show();
                        } else {
                            DialogUtil.createAlertDialog(getActivity(), "Error", response, "Okay").show();
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.getCause().printStackTrace();
                progress.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("p_hash", App.getInstance().getPreference().getString(TAGs.KEY_SCHEME_FRAME, ""));// pan hash
                params.put("l_query", grievanceText);// grievance text
                params.put("query_type", grievanceTypeId);// grievance type id
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}
