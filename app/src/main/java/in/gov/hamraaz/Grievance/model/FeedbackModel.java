
package in.gov.hamraaz.Grievance.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class FeedbackModel {
    @SerializedName("Feedback_Sugg")
    private List<FeedbackSugg> mFeedbackSugg;

    public List<FeedbackSugg> getFeedbackSugg() {
        return mFeedbackSugg;
    }

    public void setFeedbackSugg(List<FeedbackSugg> feedbackSugg) {
        mFeedbackSugg = feedbackSugg;
    }

    public class FeedbackSugg {

        @SerializedName("grev_id")
        private String mGrevId;
        @SerializedName("L_Query")
        private String mLQuery;
        @SerializedName("L_Query_Dt")
        private String mLQueryDt;
        @SerializedName("reply_RO")
        private String mReplyRO;
        @SerializedName("reply_ro_dt")
        private String mReplyRoDt;

        public String getGrevId() {
            return mGrevId;
        }

        public void setGrevId(String grevId) {
            mGrevId = grevId;
        }

        public String getLQuery() {
            return mLQuery;
        }

        public void setLQuery(String lQuery) {
            mLQuery = lQuery;
        }

        public String getLQueryDt() {
            return mLQueryDt;
        }

        public void setLQueryDt(String lQueryDt) {
            mLQueryDt = lQueryDt;
        }

        public String getReplyRO() {
            return mReplyRO;
        }

        public void setReplyRO(String replyRO) {
            mReplyRO = replyRO;
        }

        public String getReplyRoDt() {
            return mReplyRoDt;
        }

        public void setReplyRoDt(String replyRoDt) {
            mReplyRoDt = replyRoDt;
        }

    }


}
