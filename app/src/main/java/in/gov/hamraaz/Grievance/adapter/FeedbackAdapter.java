package in.gov.hamraaz.Grievance.adapter;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.gov.hamraaz.Grievance.model.FeedbackModel;
import in.gov.hamraaz.R;

public class FeedbackAdapter extends RecyclerView.Adapter {
    private final FragmentActivity activity;

    private List<FeedbackModel.FeedbackSugg> feedbackSuggs;

    public FeedbackAdapter(FragmentActivity activity) {
        this.activity = activity;
        feedbackSuggs = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder holder = null;
        View v;
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.feedback_adapter, parent, false);
        holder = new ItemViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ItemViewHolder) holder).bind(position);
    }


    @Override
    public int getItemCount() {
        return feedbackSuggs.size();
    }

    public void setData(List<FeedbackModel.FeedbackSugg> feedbackSuggs) {
        this.feedbackSuggs = feedbackSuggs;
        notifyDataSetChanged();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.grev_id)
        TextView grev_id;
        @BindView(R.id.L_Query)
        TextView L_Query;
        @BindView(R.id.L_Query_Dt)
        TextView L_Query_Dt;

        public ItemViewHolder(View v) {

            super(v);
            ButterKnife.bind(this, v);
        }

        public void bind(int position) {
            FeedbackModel.FeedbackSugg feedbackSugg = feedbackSuggs.get(position);
            grev_id.setText(feedbackSugg.getGrevId());
            L_Query.setText(feedbackSugg.getLQuery());
            L_Query_Dt.setText(feedbackSugg.getLQueryDt());


        }
    }
}
