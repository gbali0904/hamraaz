package in.gov.hamraaz.Grievance;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.gov.hamraaz.App;
import in.gov.hamraaz.Grievance.model.GrievanceThread;
import in.gov.hamraaz.R;
import in.gov.hamraaz.Utils.DialogUtil;
import in.gov.hamraaz.Utils.RemoteConfigManager;
import in.gov.hamraaz.Utils.TAGs;
import in.gov.hamraaz.Utils.Util;

public class ShowGrievanceActivity extends AppCompatActivity {

    private static final java.lang.String KEY_PAN_HASH = "pan_hash";
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rvViewGrievance)
    RecyclerView rvViewGrievance;
    private ArrayList<GrievanceThread> grievanceThreads = new ArrayList<>();
    private GrievanceThreadAdapter adapter;
    private String panHash;
    private String url = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_grievance);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (null != getIntent().getExtras()) {
            panHash = getIntent().getExtras().getString(KEY_PAN_HASH);
        }

        rvViewGrievance.setHasFixedSize(true);
        rvViewGrievance.setLayoutManager(new LinearLayoutManager(ShowGrievanceActivity.this));
        adapter = new GrievanceThreadAdapter(grievanceThreads);
        rvViewGrievance.setAdapter(adapter);

        downloadGrievanceThreads();
    }

    private void downloadGrievanceThreads() {
        final ProgressDialog progress = DialogUtil.createProgressDialog(this, "Loading", "Please wait");
        progress.show();

        if (!Util.checkInternet(ShowGrievanceActivity.this)) {
            url = "https://10.0.0.161/s_pan/v_query.aspx";
        } else {
            url = RemoteConfigManager.getViewGrievanceUrl();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.length() > 0) {
                    try {
                        progress.dismiss();

                        Gson gson = new Gson();
                        Type type = new TypeToken<List<GrievanceThread>>() {
                        }.getType();
                        ArrayList<GrievanceThread> grievanceThreadsList = gson.fromJson(response, type);

                        grievanceThreads.clear();
                        grievanceThreads.addAll(grievanceThreadsList);
                        adapter.notifyDataSetChanged();

                    } catch (Exception e) {
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.getCause().printStackTrace();
                progress.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("p_hash", App.getInstance().getPreference().getString(TAGs.KEY_SCHEME_FRAME, ""));
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            default:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
