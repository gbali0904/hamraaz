package in.gov.hamraaz.Grievance;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import in.gov.hamraaz.R;
import in.gov.hamraaz.Utils.Constant;

public class GrievanceActivity extends AppCompatActivity {

    public static final String KEY_PAN_HASH = "pan_hash";
    private String pan_hash;
    private String status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grievance);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (null != getIntent().getExtras()) {
            pan_hash = getIntent().getExtras().getString(KEY_PAN_HASH);
            status = getIntent().getExtras().getString(Constant.STATUS);
        }

        if (status.equals("1")) {
            getSupportActionBar().setTitle(getResources().getString(R.string.AFPPSubscription));

        } else if (status.equals("2")) {
            getSupportActionBar().setTitle(getResources().getString(R.string.FEEDBACK));
        } else {
            getSupportActionBar().setTitle(getResources().getString(R.string.nav_lodge_grievance));
        }


        getSupportFragmentManager().beginTransaction().replace(R.id.containerGrievance, GrievanceTermsFragment.newInstance(pan_hash, status)).commit();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            default:
                this.finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
