package in.gov.hamraaz.Grievance.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Pallaw Pathak on 31/10/18. - https://www.linkedin.com/in/pallaw-pathak-a6a324a1/
 */

public class GrievanceThread {

    /**
     * L_Query : Test Message for insert Testing
     * L_Query_Dt : /Date(1539602436973)/
     * reply_RO : okay rested for reply
     * reply_RO_dt : null
     * grev_id : 3700002
     */

    @SerializedName("L_Query")
    private String L_Query;
    @SerializedName("L_Query_Dt")
    private String L_Query_Dt;
    @SerializedName("reply_RO")
    private String reply_RO;
    @SerializedName("reply_RO_dt")
    private String reply_RO_dt;
    @SerializedName("grev_id")
    private String grev_id;

    public String getL_Query() {
        return L_Query;
    }

    public void setL_Query(String L_Query) {
        this.L_Query = L_Query;
    }

    public String getL_Query_Dt() {
        return L_Query_Dt;
    }

    public void setL_Query_Dt(String L_Query_Dt) {
        this.L_Query_Dt = L_Query_Dt;
    }

    public String getReply_RO() {
        return reply_RO;
    }

    public void setReply_RO(String reply_RO) {
        this.reply_RO = reply_RO;
    }

    public String getReply_RO_dt() {
        return reply_RO_dt;
    }

    public void setReply_RO_dt(String reply_RO_dt) {
        this.reply_RO_dt = reply_RO_dt;
    }

    public String getGrev_id() {
        return grev_id;
    }

    public void setGrev_id(String grev_id) {
        this.grev_id = grev_id;
    }
}
