package in.gov.hamraaz.contactus.adapter;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.gov.hamraaz.R;
import in.gov.hamraaz.contactus.model.ModelForContactUs;

public class ContactUsAdapter extends RecyclerView.Adapter {
    private final FragmentActivity activity;

    private List<ModelForContactUs.InboxBean> inbox;

    public ContactUsAdapter(FragmentActivity activity) {
        this.activity = activity;
        inbox = new ArrayList<>();

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_contactus, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((MyViewHolder) holder).bindData(position);

    }

    @Override
    public int getItemCount() {
        return inbox.size();
    }

    public void setData(List<ModelForContactUs.InboxBean> inbox) {
        this.inbox = inbox;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.logo)
        ImageView logo;
        @BindView(R.id.txtItemDate)
        TextView txtItemDate;
        @BindView(R.id.lay)
        LinearLayout lay;
        @BindView(R.id.inbox)
        TextView txtinbox;
        @BindView(R.id.layMain)
        LinearLayout layMain;
        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        public void bindData(int position) {
            ModelForContactUs.InboxBean inboxBean = inbox.get(position);
            txtItemDate.setText("Date : "+inboxBean.getMsg_date());
            txtinbox.setText(inboxBean.getInbox_msg());

        }
    }
}
