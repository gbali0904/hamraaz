package in.gov.hamraaz.contactus.model;

import java.util.List;

public class ModelForContactUs {
    private List<InboxBean> Inbox;

    public List<InboxBean> getInbox() {
        return Inbox;
    }

    public void setInbox(List<InboxBean> Inbox) {
        this.Inbox = Inbox;
    }

    public static class InboxBean {
        /**
         * inbox_msg : Thi is test message.   Employees from NIC , Central and State Government departments , PSUs and Autonomous bodies under Central and State Government who are authorised for administration, updation and management of Servers / sites hosted in NIC IDCs can avail VPN service . Users engaged in Network admin
         * msg_date : 20/08/2019
         * Our_mail_id : hamraaz.app@gov.in
         * Our_Mobile_No : 8587940457
         */

        private String inbox_msg;
        private String msg_date;

        public String getInbox_msg() {
            return inbox_msg;
        }

        public void setInbox_msg(String inbox_msg) {
            this.inbox_msg = inbox_msg;
        }

        public String getMsg_date() {
            return msg_date;
        }

        public void setMsg_date(String msg_date) {
            this.msg_date = msg_date;
        }
    }
}
