package in.gov.hamraaz;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.concurrent.TimeUnit;

import in.gov.hamraaz.Utils.AppService;
import in.gov.hamraaz.Utils.ApplicationState;
import in.gov.hamraaz.Utils.SessionExpiryManager;
import in.gov.hamraaz.Utils.TAGs;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Scheduler;
import rx.schedulers.Schedulers;

/**
 * Created by Pallaw Pathak on 31/10/18. - https://www.linkedin.com/in/pallaw-pathak-a6a324a1/
 */

public class App extends Application {
    private SharedPreferences pref;
    private static final String PREFS_GAME = "in.gov.hamraaz";
    private static App mInstance;
    private Scheduler defaultSubscribeScheduler;
    private String deviceId;
    public static synchronized App getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        pref = PreferenceManager.getDefaultSharedPreferences(App.this);
        mInstance = this;
        deviceId = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        getFirebaseToken();
        ApplicationState.get(this).addListener(listener);
    }
    ApplicationState.Listener listener = new ApplicationState.Listener() {
        @Override
        public void onBecameForeground() {
            if(App.getInstance().getPreference().getBoolean(TAGs.LOGIN_STATUS, false)) {
                SessionExpiryManager.getInstance().checkSessionExpiry(deviceId, App.this);
            }
        }
        @Override
        public void onBecameBackground() {
            SessionExpiryManager.getInstance().updateLastUseTimeStamp(deviceId);
        }
    };
    private void getFirebaseToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("FCM", "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();
//                        String msg = getString(R.string.msg_token_fmt, token);
                        Log.d("FCM", token);
                    }
                });
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public SharedPreferences getPreference() {
        if (pref == null)
            pref = getSharedPreferences(PREFS_GAME ,Context.MODE_PRIVATE);
        return pref;
    }
    public SharedPreferences.Editor getPreferenceEditor() {
        if (pref == null)
            pref = getSharedPreferences(PREFS_GAME ,Context.MODE_PRIVATE);
        return pref.edit();
    }


    public static Drawable getImage(Context c, String ImageName) {
        return c.getResources().getDrawable(c.getResources().getIdentifier(ImageName, "drawable", c.getPackageName()));
    }



    /*
     * ------------------------------Retrofit service---------------------
     * */
    public AppService getRetrofitServices(){

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder().connectTimeout(30, TimeUnit.MINUTES).readTimeout(30, TimeUnit.MINUTES).addInterceptor(logging);
        // add logging as last interceptor
        httpClient.addInterceptor(logging);  // <-- this is the important line!
        Retrofit retrofit= new Retrofit.Builder()
                .baseUrl("https://hamraazmp8.gov.in")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(httpClient.build())
                .build();
        AppService appService=retrofit.create(AppService.class);
        return appService;
    }
    public Scheduler defaultSubscribeScheduler() {
        if (defaultSubscribeScheduler == null) {
            defaultSubscribeScheduler = Schedulers.io();
        }
        return defaultSubscribeScheduler;
    }


}
