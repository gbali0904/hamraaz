package in.gov.hamraaz.servicevoter;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import in.gov.hamraaz.App;
import in.gov.hamraaz.R;
import in.gov.hamraaz.Utils.Constant;
import in.gov.hamraaz.Utils.DialogUtil;
import in.gov.hamraaz.Utils.RemoteConfigManager;
import in.gov.hamraaz.Utils.TAGs;
import in.gov.hamraaz.Utils.Util;
import in.gov.hamraaz.fundwithdraw.ModelForFundWithdrawal;
import in.gov.hamraaz.login.LoginActivity;
import in.gov.hamraaz.servicevoter.adapter.ServiceVoterAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class ServiceVoterFragment extends Fragment {


    public static final String TAG = ServiceVoterFragment.class.getSimpleName();
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    @BindView(R.id.horizontalScrollView)
    HorizontalScrollView horizontalScrollView;
    Unbinder unbinder;
    private String pan_hass;
    private ServiceVoterAdapter serviceVoterAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_service_voter, container, false);
        unbinder = ButterKnife.bind(this, view);

         serviceVoterAdapter = new ServiceVoterAdapter(getActivity());
        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(getActivity());
        recyclerview.setLayoutManager(horizontalLayoutManagaer);
        recyclerview.setAdapter(serviceVoterAdapter);
        Bundle bundle = getArguments();
        pan_hass = bundle.getString(Constant.KEY_PAN_HASH);
        makeServerCall(RemoteConfigManager.serviceVoterURL(), pan_hass);
        return view;
    }


    void makeServerCall(String url, final String uid) {
        final ProgressDialog progress = DialogUtil.createProgressDialog(getActivity(), "Loading", "Please wait");
        progress.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.length() > 0) {
                    progress.dismiss();
                    Type collectionType = new TypeToken<List<ModelForServiceVoter>>(){}.getType();
                    List<ModelForServiceVoter> modelForServiceVoters = new Gson().fromJson( response , collectionType);
                    serviceVoterAdapter.setData(modelForServiceVoters);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();

                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                getActivity().finish();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("schemeFrame", App.getInstance().getPreference().getString(TAGs.KEY_SCHEME_FRAME, ""));
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + pan_hass);
                headers.put("timestamp", Util.getTimestamp());
                return headers;

            }


        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
