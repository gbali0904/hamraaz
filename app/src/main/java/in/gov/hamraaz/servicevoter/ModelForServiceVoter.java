package in.gov.hamraaz.servicevoter;

public class ModelForServiceVoter {

    /**
     * Buckle_No : 15332744K
     * ac_pc_name : Srivaikuntam
     * state : Tamil Nadu
     * Gender : M
     * epic_no : A123456Y
     */

    private String Buckle_No;
    private String ac_pc_name;
    private String state;
    private String Gender;
    private String epic_no;

    public String getBuckle_No() {
        return Buckle_No;
    }

    public void setBuckle_No(String Buckle_No) {
        this.Buckle_No = Buckle_No;
    }

    public String getAc_pc_name() {
        return ac_pc_name;
    }

    public void setAc_pc_name(String ac_pc_name) {
        this.ac_pc_name = ac_pc_name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String Gender) {
        this.Gender = Gender;
    }

    public String getEpic_no() {
        return epic_no;
    }

    public void setEpic_no(String epic_no) {
        this.epic_no = epic_no;
    }
}
