package in.gov.hamraaz.servicevoter.adapter;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.gov.hamraaz.R;
import in.gov.hamraaz.servicevoter.ModelForServiceVoter;

public class ServiceVoterAdapter extends RecyclerView.Adapter {
    private final FragmentActivity activity;

    private List<ModelForServiceVoter> modelForServiceVoters;

    public ServiceVoterAdapter(FragmentActivity activity) {
        this.activity = activity;
        modelForServiceVoters=new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        View v;
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.service_voter_adapter, parent, false);
        holder = new ItemViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ItemViewHolder) holder).bind(position);
    }

    @Override
    public int getItemCount() {
        return modelForServiceVoters.size();
    }

    public void setData(List<ModelForServiceVoter> modelForServiceVoters) {
        this.modelForServiceVoters = modelForServiceVoters;
        notifyDataSetChanged();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ser)
        TextView ser;
        @BindView(R.id.Buckle_No)
        TextView BuckleNo;
        @BindView(R.id.ac_pc_name)
        TextView acPcName;
        @BindView(R.id.state)
        TextView state;
        @BindView(R.id.Gender)
        TextView Gender;
        @BindView(R.id.epic_no)
        TextView epicNo;
        public ItemViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        public void bind(int position) {
            ModelForServiceVoter modelForServiceVoter = modelForServiceVoters.get(position);
            int id=++position;
            ser.setText(""+id);
            BuckleNo.setText(""+modelForServiceVoter.getBuckle_No());
            acPcName.setText(""+modelForServiceVoter.getAc_pc_name());
            state.setText(""+modelForServiceVoter.getState());
            Gender.setText(""+modelForServiceVoter.getGender());
            epicNo.setText(""+modelForServiceVoter.getEpic_no());

        }
    }
}
