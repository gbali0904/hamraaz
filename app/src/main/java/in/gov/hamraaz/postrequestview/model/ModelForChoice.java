package in.gov.hamraaz.postrequestview.model;

public class ModelForChoice {

    /**
     * choice_cd : 1
     * choice_Name : Choice 1
     */

    private String choice_cd;
    private String choice_Name;


    private int state_id;
    private int station_id;


    public String getChoice_cd() {
        return choice_cd;
    }

    public void setChoice_cd(String choice_cd) {
        this.choice_cd = choice_cd;
    }

    public String getChoice_Name() {
        return choice_Name;
    }

    public void setChoice_Name(String choice_Name) {
        this.choice_Name = choice_Name;
    }

    public int getState_id() {
        return state_id;
    }

    public void setState_id(int state_id) {
        this.state_id = state_id;
    }

    public int getStation_id() {
        return station_id;
    }

    public void setStation_id(int station_id) {
        this.station_id = station_id;
    }
}
