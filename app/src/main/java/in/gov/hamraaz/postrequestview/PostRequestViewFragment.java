package in.gov.hamraaz.postrequestview;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import in.gov.hamraaz.R;
import in.gov.hamraaz.Utils.Constant;
import in.gov.hamraaz.Utils.DialogUtil;
import in.gov.hamraaz.Utils.RemoteConfigManager;
import in.gov.hamraaz.login.LoginActivity;
import in.gov.hamraaz.postrequestview.adapter.PostRequestViewAdapter;
import in.gov.hamraaz.postrequestview.model.ModelForChoice;
import in.gov.hamraaz.postrequestview.model.ModelForPostRequestView;
import in.gov.hamraaz.postrequestview.model.ModelForSave;
import in.gov.hamraaz.postrequestview.model.ModelForState;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostRequestViewFragment extends Fragment {


    public static final String TAG = PostRequestViewFragment.class.getSimpleName();
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    Unbinder unbinder;
    @BindView(R.id.submit)
    Button submit;
    @BindView(R.id.post_req_id)
    TextView postReqId;
    @BindView(R.id.choice_considered)
    TextView choiceConsidered;
    @BindView(R.id.req_date)
    TextView reqDate;
    @BindView(R.id.accept_dt)
    TextView acceptDt;
    @BindView(R.id.remarks)
    TextView remarks;
    private PostRequestViewAdapter adapter;
    private String pan_hass;
    private ProgressDialog progress;

    List<ModelForChoice> modelForChoices;
    // Creating date format
    DateFormat simple = new SimpleDateFormat("dd-MMM-yyyy");
    private Map<Integer, ModelForChoice> mapData;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_post_request_view, container, false);
        unbinder = ButterKnife.bind(this, view);

        Bundle bundle = getArguments();
        pan_hass = bundle.getString(Constant.KEY_PAN_HASH);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        adapter = new PostRequestViewAdapter(getActivity(), pan_hass);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        callPostRequestView(RemoteConfigManager.postRequestViewURL(), pan_hass);
        return view;
    }

    private void callPostRequestView(String url, final String uid) {
        progress = DialogUtil.createProgressDialog(getActivity(), "Loading", "Please wait");
        progress.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.length() > 0) {
                    Type collectionType = new TypeToken<List<ModelForPostRequestView>>() {
                    }.getType();
                    List<ModelForPostRequestView> modelForPostRequestView = new Gson().fromJson(response, collectionType);

                    if (!modelForPostRequestView.isEmpty()) {
                        ModelForPostRequestView modelForPostRequestViewbeen = modelForPostRequestView.get(0);
                        postReqId.setText("Post Req ID : " + modelForPostRequestViewbeen.getPost_req_id());
                        choiceConsidered.setText("Choice Considered : " + modelForPostRequestViewbeen.getChoice_considered());
                        String req_date = modelForPostRequestViewbeen.getReq_date();
                        req_date = req_date.substring(req_date.indexOf("(") + 1);
                        req_date = req_date.substring(0, req_date.indexOf(")"));
                        Date req = new Date(Long.parseLong(req_date));
                        reqDate.setText("Req Date : " + simple.format(req));

                        String accept_dt = modelForPostRequestViewbeen.getAccept_dt();
                        if (accept_dt != null) {
                            accept_dt = accept_dt.substring(accept_dt.indexOf("(") + 1);
                            accept_dt = accept_dt.substring(0, accept_dt.indexOf(")"));
                            Date accept = new Date(Long.parseLong((accept_dt)));
                            acceptDt.setText("Accept Date : " + simple.format(accept));
                        }
                        remarks.setText("Remarks : " + modelForPostRequestViewbeen.getRemarks());

                    }
                    makeServerCall(RemoteConfigManager.menuChoiceURL(), pan_hass);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();

                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                getActivity().finish();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("schemeFrame", uid);
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    void makeServerCall(String url, final String uid) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.length() > 0) {
                    //progress.dismiss();

                    Type collectionType = new TypeToken<List<ModelForChoice>>() {
                    }.getType();
                    modelForChoices = new Gson().fromJson(response, collectionType);

                    makeStateCall(RemoteConfigManager.stateURL(), pan_hass, modelForChoices);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("schemeFrame", uid);
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void makeStateCall(String stateURL, final String pan_hass, final List<ModelForChoice> modelForChoices) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, stateURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.length() > 0) {
                    progress.dismiss();
                    Type collectionType = new TypeToken<List<ModelForState>>() {}.getType();
                    List<ModelForState> modelForStates = new Gson().fromJson(response, collectionType);
                    adapter.setData(modelForChoices, modelForStates);

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("schemeFrame", pan_hass);
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.submit)
    public void onViewClicked() {
        mapData = adapter.getMapData();
        ModelForSave modelForSave = new ModelForSave();
        List<ModelForSave.AppRequestBean> appRequest = new ArrayList<>();
        if (mapData.size() == modelForChoices.size()) {
            ArrayList<ModelForChoice> strings = new ArrayList<>();

            for (Map.Entry<Integer, ModelForChoice> entry : mapData.entrySet()) {
                strings.add(entry.getValue());
            }
            for (ModelForChoice modelForChoice : strings) {
                ModelForSave.AppRequestBean appRequestBean = new ModelForSave.AppRequestBean();
                appRequestBean.setChoice_cd(modelForChoice.getChoice_cd());
                appRequestBean.setState_id(String.valueOf(modelForChoice.getState_id()));
                appRequestBean.setStation_id(String.valueOf(modelForChoice.getStation_id()));
                appRequest.add(appRequestBean);
            }
            modelForSave.setAppRequest(appRequest);
            modelForSave.setSchemeFrame(pan_hass);

            Log.e("this", "JSON::" + new Gson().toJson(modelForSave));

            savePostRequestCall(RemoteConfigManager.pastRequestURL(), pan_hass, new Gson().toJson(modelForSave));

        } else {
            Toast.makeText(getActivity(), "Please Select All Choices", Toast.LENGTH_LONG).show();
        }
    }

    private void savePostRequestCall(String pastRequestURL, final String pan_hass, final String post_request_data) {

        Log.e("this", "post_request_data:::" + post_request_data);
        progress = DialogUtil.createProgressDialog(getActivity(), "Loading", "Please wait");
        progress.show();

        StringRequest myReq = new StringRequest(Request.Method.POST,
                pastRequestURL,
                createMyReqSuccessListener(),
                createMyReqErrorListener()) {

            @Override
            public byte[] getBody() {
                return post_request_data.getBytes();
            }

            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        myReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(myReq);

    }

    private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progress.dismiss();
                mapData.clear();
                callPostRequestView(RemoteConfigManager.postRequestViewURL(), pan_hass);
                Toast.makeText(getActivity(), response, Toast.LENGTH_LONG).show();
            }
        };
    }


    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
                mapData.clear();
                Log.i(TAG, "Ski error connect - " + error);
            }
        };
    }
}
