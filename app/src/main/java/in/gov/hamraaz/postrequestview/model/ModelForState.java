package in.gov.hamraaz.postrequestview.model;

public class ModelForState {

    /**
     * state_cd : 1
     * state_name : Tamil Nadu
     */

    private String state_cd;
    private String state_name;

    public String getState_cd() {
        return state_cd;
    }

    public void setState_cd(String state_cd) {
        this.state_cd = state_cd;
    }

    public String getState_name() {
        return state_name;
    }

    public void setState_name(String state_name) {
        this.state_name = state_name;
    }
}
