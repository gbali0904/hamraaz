package in.gov.hamraaz.postrequestview.model;

public class ModelForStation {

    /**
     * stn_cd : 301
     * stn_name : Trivandrum
     */

    private String stn_cd;
    private String stn_name;

    public String getStn_cd() {
        return stn_cd;
    }

    public void setStn_cd(String stn_cd) {
        this.stn_cd = stn_cd;
    }

    public String getStn_name() {
        return stn_name;
    }

    public void setStn_name(String stn_name) {
        this.stn_name = stn_name;
    }
}
