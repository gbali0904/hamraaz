package in.gov.hamraaz.postrequestview.model;

public class ModelForSubmitPostRequestView {

    /**
     * aa : Your request  already submitted
     */

    private String aa;

    public String getAa() {
        return aa;
    }

    public void setAa(String aa) {
        this.aa = aa;
    }
}

