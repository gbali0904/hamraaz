package in.gov.hamraaz.postrequestview.model;

import java.util.List;

public class ModelForSave {

    /**
     * SchemeFrame : C3B2710B1C8B3DBAAFD76EBBA9581CC17E5EB9DE7234DB67DF2A9875D1F873AC
     * AppRequest : [{"state_id":"3","choice_cd":"1","station_id":"402"}]
     */

    private String SchemeFrame;
    private List<AppRequestBean> AppRequest;

    public String getSchemeFrame() {
        return SchemeFrame;
    }

    public void setSchemeFrame(String SchemeFrame) {
        this.SchemeFrame = SchemeFrame;
    }

    public List<AppRequestBean> getAppRequest() {
        return AppRequest;
    }

    public void setAppRequest(List<AppRequestBean> AppRequest) {
        this.AppRequest = AppRequest;
    }

    public static class AppRequestBean {
        /**
         * state_id : 3
         * choice_cd : 1
         * station_id : 402
         */

        private String state_id;
        private String choice_cd;
        private String station_id;

        public String getState_id() {
            return state_id;
        }

        public void setState_id(String state_id) {
            this.state_id = state_id;
        }

        public String getChoice_cd() {
            return choice_cd;
        }

        public void setChoice_cd(String choice_cd) {
            this.choice_cd = choice_cd;
        }

        public String getStation_id() {
            return station_id;
        }

        public void setStation_id(String station_id) {
            this.station_id = station_id;
        }
    }
}
