package in.gov.hamraaz.postrequestview.model;

import com.google.gson.annotations.SerializedName;

public class ModelForPostRequestView {

    /**
     * post_req_id : 230001
     * choice_considered : c6
     * req_date : /Date(1564421041530)/
     * accept_dt : /Date(1564421041530)/
     * remarks : Choice c6 is considered
     */

    @SerializedName("post_req_id")
    private String post_req_id;
    @SerializedName("choice_considered")
    private String choice_considered;
    @SerializedName("req_date")
    private String req_date;
    @SerializedName("accept_dt")
    private String accept_dt;
    @SerializedName("remarks")
    private String remarks;

    public String getPost_req_id() {
        return post_req_id;
    }

    public void setPost_req_id(String post_req_id) {
        this.post_req_id = post_req_id;
    }

    public String getChoice_considered() {
        return choice_considered;
    }

    public void setChoice_considered(String choice_considered) {
        this.choice_considered = choice_considered;
    }

    public String getReq_date() {
        return req_date;
    }

    public void setReq_date(String req_date) {
        this.req_date = req_date;
    }

    public String getAccept_dt() {
        return accept_dt;
    }

    public void setAccept_dt(String accept_dt) {
        this.accept_dt = accept_dt;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
