package in.gov.hamraaz.postrequestview.adapter;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.gov.hamraaz.R;
import in.gov.hamraaz.Utils.RemoteConfigManager;
import in.gov.hamraaz.postrequestview.model.ModelForChoice;
import in.gov.hamraaz.postrequestview.model.ModelForState;
import in.gov.hamraaz.postrequestview.model.ModelForStation;

public class PostRequestViewAdapter extends RecyclerView.Adapter {

    private final FragmentActivity activity;
    private final String pan_hass;
    private final ArrayList<String> state;

    private List<ModelForChoice> modelForChoices;
    private List<ModelForState> modelForStates;

    Map<Integer, ModelForChoice> stringStringMap=new HashMap<Integer, ModelForChoice>();

    public PostRequestViewAdapter(FragmentActivity activity, String pan_hass) {

        this.activity = activity;
        this.pan_hass=pan_hass;
        modelForChoices = new ArrayList<>();
        modelForStates=new ArrayList<>();
        state=new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        View v;
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.choice_adapter, parent, false);
        holder = new ItemViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ItemViewHolder) holder).bind(position);
    }

    @Override
    public int getItemCount() {
        return modelForChoices.size();
    }

    public void setData(List<ModelForChoice> modelForChoices, List<ModelForState> modelForStates) {
        this.modelForChoices = modelForChoices;
        this.modelForStates = modelForStates;
        state.add("Select State");
        for  (ModelForState modelForState: modelForStates){
            state.add(modelForState.getState_name());
        }
        notifyDataSetChanged();
    }

    public Map<Integer,ModelForChoice> getMapData() {
        return stringStringMap;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.choice)
        TextView choice;
        @BindView(R.id.spinnerState)
        Spinner spinnerState;

        @BindView(R.id.spinnerStation)
        Spinner spinnerStation;

        public ItemViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        public void bind(final int position) {
            final ModelForChoice modelForChoice = modelForChoices.get(position);
            setIsRecyclable(false);
            choice.setText("" + modelForChoice.getChoice_Name());

            ArrayAdapter aa = new ArrayAdapter(activity,android.R.layout.simple_spinner_item,state);
            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerState.setAdapter(aa);

            spinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                    if (i==0){

                    }
                    else {
                        int index = i-1 ;
                        setStation(position,modelForStates.get(index).getState_cd(),modelForChoice);
                        modelForChoice.setState_id(Integer.parseInt(modelForStates.get(index).getState_cd()));
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

        }

        private void setStation(final int position, final String state_cd, final ModelForChoice modelForChoice) {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, RemoteConfigManager.stationURL(), new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (response.length() > 0) {
                        Type collectionType = new TypeToken<List<ModelForStation>>(){}.getType();
                        final List<ModelForStation> modelForStations = new Gson().fromJson( response , collectionType);
                        ArrayList<String> station=new ArrayList<>();
                        station.add("Select Station");
                        for (ModelForStation modelForStation:modelForStations) {
                            station.add(modelForStation.getStn_name());
                        }
                        ArrayAdapter aa = new ArrayAdapter(activity,android.R.layout.simple_spinner_item,station);
                        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinnerStation.setAdapter(aa);
                        spinnerStation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                                if (i==0){

                                }
                                else {
                                    int index = i-1 ;
                                    modelForChoice.setStation_id(Integer.parseInt(modelForStations.get(index).getStn_cd()));
                                    stringStringMap.put(position,modelForChoice);
                                }

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });

                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("schemeFrame", pan_hass);
                    params.put("st_cd", state_cd);
                    return params;
                }

            };
            RequestQueue requestQueue = Volley.newRequestQueue(activity);
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(stringRequest);

        }


    }
}