package in.gov.hamraaz.Utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ToastUtil {

    public static Toast makeImageToast(Context context, Bitmap imageResId, CharSequence text, int length) {
        Toast toast = Toast.makeText(context, text, length);
        toast.setGravity(Gravity.FILL, 0, 0);
        View rootView = toast.getView();
        LinearLayout linearLayout = null;
        View messageTextView = null;

        // check (expected) toast layout
        if (rootView instanceof LinearLayout) {
            linearLayout = (LinearLayout) rootView;

            if (linearLayout.getChildCount() == 1) {
                View child = linearLayout.getChildAt(0);

                if (child instanceof TextView) {
                    messageTextView = (TextView) child;
                    ((TextView) child).setGravity(Gravity.CENTER);
                }
            }
        }

        // cancel modification because toast layout is not what we expected
        if (linearLayout == null || messageTextView == null) {
            return toast;
        }

        ViewGroup.LayoutParams textParams = messageTextView.getLayoutParams();
        ((LinearLayout.LayoutParams) textParams).gravity = Gravity.FILL_VERTICAL;

        LinearLayout.LayoutParams imageParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT);
        imageParams.gravity = Gravity.FILL;

        // setup image view
        ImageView imageView = new ImageView(context);
        imageView.setImageBitmap(imageResId);
        imageView.setLayoutParams(imageParams);

        // modify root layout
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        BitmapDrawable background = new BitmapDrawable(imageResId);
        linearLayout.setBackgroundDrawable(background);
        linearLayout.setGravity(Gravity.FILL_HORIZONTAL);
        linearLayout.setGravity(Gravity.FILL_VERTICAL);

        return toast;
    }


}
