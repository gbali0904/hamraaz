package in.gov.hamraaz.Utils;

public class TAGs {
    public static final String LOGIN_STATUS = "login_status";
    public static final String KEY_PAN_HASH = "pan_hash";
    public static final String SUCCESS_RESPONSE = "success_response";
    public static final String BLACK_LIST = "black_list";
    public static final String BLACK_LIST_TIME = "black_list_time";
    public static final String KEY_SCHEME_FRAME = "schemeFrame";
    public static final String KEY_PAN_HASH_VALUE = "pan_hash_value";
}
