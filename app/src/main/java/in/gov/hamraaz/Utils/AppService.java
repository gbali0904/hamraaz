package in.gov.hamraaz.Utils;

import in.gov.hamraaz.leave.model.ModelForLeave;
import in.gov.hamraaz.ppodetail.ModelForPPODetail;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;
import rx.Observable;

public interface AppService {

    @POST()
    Observable<ModelForLeave> getLeaveData(@Url  String url , @Header("Authorization") String token , @Header("timestamp") String timestamp);



    @POST()
    Observable<ModelForPPODetail> getPPOData(@Url  String url , @Header("Authorization") String token , @Header("timestamp") String timestamp);


}
