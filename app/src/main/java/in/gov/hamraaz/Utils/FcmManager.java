package in.gov.hamraaz.Utils;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.messaging.FirebaseMessaging;

/**
 * Created by Pallaw Pathak on 31/10/18. - https://www.linkedin.com/in/pallaw-pathak-a6a324a1/
 */

public class FcmManager {
    private static final FcmManager ourInstance = new FcmManager();
    private String token;

    private FcmManager() {
    }

    public static FcmManager getInstance() {
        return ourInstance;
    }

    public void updateToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

}
