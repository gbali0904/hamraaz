package in.gov.hamraaz.Utils;

import android.content.Context;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

import in.gov.hamraaz.login.LoginActivity;
import in.gov.hamraaz.App;

/**
 * Created by Pallaw Pathak on 2019-05-13. - https://www.linkedin.com/in/pallaw-pathak-a6a324a1/
 */
public class SessionExpiryManager {

    private static final String TAG = SessionExpiryManager.class.getSimpleName();
    private static SessionExpiryManager ourInstance;
    private static DatabaseReference lastLoginStamps;
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static SessionExpiryManager getInstance() {
        if(null == ourInstance) {
            initLastLoginReference();
            ourInstance = new SessionExpiryManager();
        }
        return ourInstance;
    }

    private static void initLastLoginReference() {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        lastLoginStamps = mDatabase.child("LastLoginStamps");
    }

    private SessionExpiryManager() {
    }


    public void checkSessionExpiry(String deviceId, final Context context) {
        DatabaseReference child = lastLoginStamps.child(deviceId);
        child.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String timeStamp = dataSnapshot.getValue().toString();
                if(null != timeStamp) {
                    try {

                        long sessionTimeInSeconds = getSessionTimeInSeconds();
                        if (sessionTimeInSeconds != 0) {
                            Date lastUsedDate = dateFormat.parse(timeStamp);
                            Date currentDate = new Date();

                            long diff = currentDate.getTime() - lastUsedDate.getTime();
                            long seconds = diff / 1000;
                            long minutes = seconds / 60;
                            long hours = minutes / 60;
                            long days = hours / 24;

                            Log.d(TAG, "Difference: " + " seconds: " + seconds + " minutes: " + minutes
                                    + " hours: " + hours + " days: " + days);

                            if(seconds > sessionTimeInSeconds ) {
                                //Clear local Session
                                App.getInstance().getPreferenceEditor().putBoolean(TAGs.LOGIN_STATUS,false).commit();
                                App.getInstance().getPreferenceEditor().putString(TAGs.KEY_PAN_HASH,"").commit();

                                LoginActivity.startActivity(context);


                            }

                            Log.d(TAG, "Last timeStamp : "+ timeStamp);
                            Log.d(TAG, "session time : "+ sessionTimeInSeconds);

                        }

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "Last timeStamp error : "+ databaseError.getMessage());
            }
        });
    }

    private long getSessionTimeInSeconds() {
        long timeInSeconds = 0;
        String sessionTime = RemoteConfigManager.getSessionTime();
        Pattern sessionTimePattern = Pattern.compile("([0-9]?[0-9]):([0-5]?[0-9]):([0-5]?[0-9])");
        if (sessionTimePattern.matcher(sessionTime).matches()) {

            String[] split = sessionTime.trim().split(":");

            int hour = Integer.parseInt(split[0]);
            int minute = Integer.parseInt(split[1]);
            int seconds = Integer.parseInt(split[2]);

            timeInSeconds = (hour * 60 * 60) + (minute * 60) + seconds;

        } else {
            try {
                throw new Exception("Session time format is wrong in remote config. Required HH:mm:ss found "+ sessionTime);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return timeInSeconds;
    }

    public void updateLastUseTimeStamp(String deviceId) {
        Date date = new Date();
        String timeStamp = dateFormat.format(date);
        lastLoginStamps.child(deviceId).setValue(timeStamp);
    }

}
