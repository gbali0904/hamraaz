package in.gov.hamraaz.Utils;

/**
 * Created by Pallaw Pathak on 2019-05-13. - https://www.linkedin.com/in/pallaw-pathak-a6a324a1/
 */
public class TimeStamp {
    String deviceId;
    Long timeStamp;

    public TimeStamp(String deviceId, Long timeStamp) {
        this.deviceId = deviceId;
        this.timeStamp = timeStamp;
    }
}
