package in.gov.hamraaz.Utils;

import android.content.Context;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.Random;


/**
 * Created by Syamu on 10-11-2017.
 */

public class EncryptionUtil {

    public static final String USER_HASH_SALT = "-1533**";


    public String getProguardPolicy(Context context) {
        String proguardPolicy = null;
        try {
            JSONObject object = new JSONObject(loadJSONFromAsset(context));
            proguardPolicy = object.getString("proguard_policy");
            proguardPolicy = arg0(proguardPolicy);
            proguardPolicy = arg1(proguardPolicy);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return proguardPolicy;
    }

    public String loadJSONFromAsset(Context context) {
        String json = null;
        try {
            InputStream is = context.getAssets().open("proguard_policy.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public String arg0(String args) {
        return args.substring(0, args.length() - 279);
    }

    public String arg1(String args) {
        return args.substring(279);
    }


    public static String getHashValue(String value, String salt) {
        try {
            String join = value + salt;
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("SHA-256");
            byte[] array = md.digest(join.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString().toUpperCase();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }

    public static String getHashValueForPassword(String value, String salt, String randomNumber) {
        try {
            String join = value + salt + randomNumber;
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("SHA-256");
            byte[] array = md.digest(join.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString().toUpperCase();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }


    public static String getSHA1HashValue(String value, String salt) {
        try {
            String join = value + salt;
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("SHA-1");
            byte[] array = md.digest(join.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100), 1, 3);
            }
            return sb.toString().toUpperCase();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }

    public static String genrateRandomNumber() {
        Random random = new Random();
        String generatedPassword = String.format("%04d", random.nextInt(10000));
        return generatedPassword;
    }

}
