package in.gov.hamraaz.Utils;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;

public class DownloadTask {
    private String downloadUrl ;
    String downloadFileName;

    public DownloadTask(Context context, String fromDownloadBaseUrl, String file, String title) {
        this.downloadUrl = fromDownloadBaseUrl+file;
        downloadFileName=file;
        DownloadManager downloadmanager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        Uri uri = Uri.parse(downloadUrl);
        DownloadManager.Request request = new DownloadManager.Request(uri);
        request.setTitle(title+uri.getLastPathSegment());
        request.setDescription("Downloading");
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, uri.getLastPathSegment());
        downloadmanager.enqueue(request);
    }
}
