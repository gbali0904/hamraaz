package in.gov.hamraaz.Utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

/**
 * Created by Pallaw Pathak on 27/08/18. - https://www.linkedin.com/in/pallaw-pathak-a6a324a1/
 */

public class DialogUtil {

    public static AlertDialog createAlertDialog(Context context, String title, String message, String positivebutton) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);

        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, positivebutton, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        return alertDialog;
    }

    public static AlertDialog createAlertDialog(Context context, String title, String message, String positivebutton, DialogInterface.OnClickListener positiveButtonListener) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);

        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, positivebutton, positiveButtonListener);
        return alertDialog;
    }

    public static AlertDialog createAlertDialog(Context context, String title, String message, String positivebutton,boolean cancelable, DialogInterface.OnClickListener positiveButtonListener) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setCancelable(cancelable);

        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, positivebutton, positiveButtonListener);
        return alertDialog;
    }

    public static AlertDialog createAlertDialog(Context context, String title, String message, DialogInterface.OnClickListener positiveButtonListener, DialogInterface.OnClickListener negativeButtonListener) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes", positiveButtonListener);
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No", negativeButtonListener);
        return alertDialog;
    }

    public static ProgressDialog createProgressDialog(Context context, String title, String message) {
        ProgressDialog mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setTitle(title);
        mProgressDialog.setMessage(message);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setCancelable(false);
        return mProgressDialog;
    }
}
