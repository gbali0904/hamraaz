package in.gov.hamraaz.Utils;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

/**
 * Created by Pallaw Pathak on 31/08/18. - https://www.linkedin.com/in/pallaw-pathak-a6a324a1/
 */

public class RemoteConfigManager {

    private static final String KEY_USER_SERVICE_VOTER = "url_65_service_voter";
    private static final String KEY_MENU_CHOICE_URL = "url_65_menu_choice";
    private static final String KEY_STATE_URL = "url_65_menu_state";
    private static final String KEY_STATATION_URL = "url_65_menu_stn";
    private static final String KEY_POST_REQUEST_VIEW_URL = "url_65_dsc_post_view";
    private static final String KEY_AFPP_URL = "url_65_afpp_subs_save";
    private static final String KEY_APP_MENU_MSG = "url_65_app_menu";
    private static final String KEY_PROFILE_UPDATE_URL = "url_65_profile_update";
    private static final String KEY_POST_REQUEST_URL = "url_65_post_view_update";
    private static final String KEY_CONTACT_US_URL = "url_65_ContactUs_Inbox";
    private static final String KEY_REQ_OTP_URL = "url_65_otp_signUP";
    private static final String KEY_SEQURITY_QUES = "url_65_security_ques";
    private static RemoteConfigManager mInstance;
    private static FirebaseRemoteConfig firebaseRemoteConfig;
    private static String KEY_BASE_URL = "url_base";
    private static String KEY_BLACKLIST_CHECK = "url_account_blacklist_check";
    private static String KEY_BLACKLIST_UPDATE = "url_account_blacklist_update";
    private static String KEY_APP_CURRENT_VERSION = "url_app_current_version";


   // private static String KEY_ACCOUNT_LOGIN_CHECK_PAN = "url_account_login_check_pan";
    private static String KEY_ACCOUNT_LOGIN_CHECK_PAN = "url_65_checkCredApp";



    private static String KEY_ACCOUNT_SIGNUP_WITH_ARPAN = "url_account_signup";
    private static String KEY_ACCOUNT_SIGNUP_CHECK_PAN = "url_account_signup_check_pan";

    // private static String KEY_ACCOUNT_OTP_SEND = "url_account_opt_send";
    private static String KEY_ACCOUNT_OTP_SEND = "url_65_otp_send_sigin";
    private static String KEY_ACCOUNT_OTP_SEND_MYPROFILE = "url_65_profile_otp_send";


//    private static String KEY_ACCOUNT_OTP_VERIFY = "url_account_otp_verfiy";
    private static String KEY_ACCOUNT_OTP_VERIFY = "url_65_Verify_otp";

    private static String KEY_ACCOUNT_CHANGE_DEFAULT_PASSWORD = "url_account_change_pass_after_auth";
    private static String KEY_ACCOUNT_FORGOTPASS_CHECK_PAN = "url_account_forgot_pass_check_pan";
    private static String KEY_ACCOUNT_VERIFY_SECURITY_ANSWER = "url_account_verify_security_answer";


    //private static String KEY_ACCOUNT_CHANGE_PASS_EXISTING = "url_account_change_pass_existing";
    private static String KEY_ACCOUNT_CHANGE_PASS_EXISTING = "url_65_changeCurrentpass";

    private static final String KEY_MY_PROFILE_URL = "url_65_my_profile";

//    private static String KEY_USER_FAMILY = "url_user_family";
    private static String KEY_USER_FAMILY = "url_65_Family_Details";

    private static String KEY_PDF_URL = "url_65_base_pdf";

//    private static final String KEY_FORM_DOWNLOAD_URL = "url_65_pdf_form16";
    private static final String KEY_FORM_DOWNLOAD_URL = "url_65_form16_pdf";
    private static final String KEY_BASE_FORM_DOWNLOAD_URL = "url_65_form16_pdf_download";


    private static final String KEY_FORM_URL = "url_65_form_16_menu";

    private static String KEY_USER_PAY_DETAILS = "url_user_pay_details";
    private static String KEY_USER_PTO = "url_user_pto";
    private static String KEY_USER_VIEW_NOTIFICATION = "url_user_view_notification";

//    private static String KEY_USER_LEAVE = "url_user_leave";
   // private static String KEY_USER_LEAVE = "url_65_Leave_details";
    private static String KEY_USER_LEAVE = "url_65_leave_ltc_details";

  //  private static String KEY_USER_PPO = "url_user_ppo";
    private static String KEY_USER_PPO = "url_65_PPO_details";
    private static String KEY_USER_FUND_WITHDRAWAL = "url_user_fund_withdrawal";
    private static String KEY_USER_OBSERVATION = "url_user_observation";
    private static String KEY_MENU_MPS = "url_menu_mps";

//    private static String KEY_URL_FLASH_MSG = "url_flash_msg";
    private static String KEY_URL_FLASH_MSG = "url_65_policy_info";

//    private static String KEY_URL_PDF_DOWNLOAD = "url_pdf_download";
    private static String KEY_URL_PDF_DOWNLOAD = "url_65_pay_pdf";

    //private static String KEY_URL_GRIEVANCE_TYPE = "url_grievance_type";
    private static String KEY_URL_GRIEVANCE_TYPE = "url_65_menu_grievaince";

    private static String KEY_URL_LODGE_GRIEVANCE= "url_lodge_grievance";
    private static String KEY_URL_FEEDBACK_GRIEVANCE= "url_7_insert_feedback";
    private static String KEY_URL_FEEDBACK= "url_7_SendFeedback_Sugg";

    private static String KEY_URL_VIEW_GRIEVANCE= "url_show_grievance";

    private static String KEY_CONFIG_APP_VERSION = "config_app_version";
    private static String KEY_CONFIG_APP_LINK= "config_app_link";
    private static String KEY_CONFIG_SESSION_TIME= "config_session_time";

    private static String KEY_PAO_PTO_STATUS_URL= "url_7_SendPAO_PTO_Status";
    private static String pdfBaseUrl;

    private RemoteConfigManager() {
    }

    public static synchronized RemoteConfigManager getInstance() {
        if (null == mInstance) {
            mInstance = new RemoteConfigManager();
        }
        return mInstance;
    }

    public static String getCheckBlackListUrl() {
        return firebaseRemoteConfig.getString(KEY_BLACKLIST_CHECK);
    }

    public static String getCheckCurrentVersionUrl() {
        return firebaseRemoteConfig.getString(KEY_APP_CURRENT_VERSION);
    }

    public static String getCheckPanUrl() {
        return firebaseRemoteConfig.getString(KEY_ACCOUNT_LOGIN_CHECK_PAN);
    }

    public static String getUpdateBlackListCounterUrl() {
        return firebaseRemoteConfig.getString(KEY_BLACKLIST_UPDATE);
    }

    public static String getSignupWithArpanPassUrl() {
        return firebaseRemoteConfig.getString(KEY_ACCOUNT_SIGNUP_WITH_ARPAN);
    }

    public static String getSignupCheckPanUrl() {
        return firebaseRemoteConfig.getString(KEY_ACCOUNT_SIGNUP_CHECK_PAN);
    }

    public static String getSubmitOtpUrl() {
        return firebaseRemoteConfig.getString(KEY_ACCOUNT_OTP_VERIFY);
    }
    public static String getSequrityQuestion() {
        return firebaseRemoteConfig.getString(KEY_SEQURITY_QUES);
    }

    public static String getRequestOtpUrl() {
        return firebaseRemoteConfig.getString(KEY_ACCOUNT_OTP_SEND);
    }

    public static String getRequestMyProfileOtpUrl() {
        //KEY_ACCOUNT_OTP_SEND_myprofile
        return firebaseRemoteConfig.getString(KEY_ACCOUNT_OTP_SEND_MYPROFILE);
    }

    public static String getChangePassAfterAuthUrl() {
        return firebaseRemoteConfig.getString(KEY_ACCOUNT_CHANGE_DEFAULT_PASSWORD);
    }

    public static String getForgotPassCheckPanUrl() {
        return firebaseRemoteConfig.getString(KEY_ACCOUNT_FORGOTPASS_CHECK_PAN);
    }

    public static String verifySecurityAnswerUrl() {
        return firebaseRemoteConfig.getString(KEY_ACCOUNT_VERIFY_SECURITY_ANSWER);
    }

    public static String getChangeExistingPassUrl() {
        return firebaseRemoteConfig.getString(KEY_ACCOUNT_CHANGE_PASS_EXISTING);
    }

    public static String getFramilyUrl() {

        return firebaseRemoteConfig.getString(KEY_USER_FAMILY);
    }

    public static String getViewPayDetailsUrl() {
        return firebaseRemoteConfig.getString(KEY_USER_PAY_DETAILS);
    }

    public static String getPtoUrl() {
        return firebaseRemoteConfig.getString(KEY_USER_PTO);
    }

    public static String getViewNotificationUrl() {
        return firebaseRemoteConfig.getString(KEY_USER_VIEW_NOTIFICATION);
    }

    public static String getLeaveUrl() {
        return firebaseRemoteConfig.getString(KEY_USER_LEAVE);
    }

    public static String getPpoDetailsUrl() {
        return firebaseRemoteConfig.getString(KEY_USER_PPO);
    }

    public static String fundWithdrawalUrl() {
        return firebaseRemoteConfig.getString(KEY_USER_FUND_WITHDRAWAL);

    }
  public static String serviceVoterURL() {
        return firebaseRemoteConfig.getString(KEY_USER_SERVICE_VOTER);

    }

    public static String getObservationUrl() {
        return firebaseRemoteConfig.getString(KEY_USER_OBSERVATION);
    }

    public static String getMenuMpsUrl() {
        return firebaseRemoteConfig.getString(KEY_MENU_MPS);
    }

    public static String getPdfBaseUrl() {
        return firebaseRemoteConfig.getString(KEY_PDF_URL);
    }

    public static String getMenuFormUrl() {
        return firebaseRemoteConfig.getString(KEY_FORM_URL);
    }

    public static String getFormDownloadUrl() {
        return firebaseRemoteConfig.getString(KEY_FORM_DOWNLOAD_URL);
    }

 

    public static String getFromDownloadBaseUrl() {

        return firebaseRemoteConfig.getString(KEY_BASE_FORM_DOWNLOAD_URL);
    }

    public static String getMyProfile() {
        return firebaseRemoteConfig.getString(KEY_MY_PROFILE_URL);

    }

    public static String menuChoiceURL() {
        return  firebaseRemoteConfig.getString(KEY_MENU_CHOICE_URL);
    }
    public static String pastRequestURL() {
        return  firebaseRemoteConfig.getString(KEY_POST_REQUEST_URL);
    }

    public static String stateURL() {
        return  firebaseRemoteConfig.getString(KEY_STATE_URL);
        
        
    }

    public static String stationURL() {
        return  firebaseRemoteConfig.getString(KEY_STATATION_URL);


    }

    public static String postRequestViewURL() {
        return  firebaseRemoteConfig.getString(KEY_POST_REQUEST_VIEW_URL);


    }

    public static String getAFPPGrievanceUrl() {


        return firebaseRemoteConfig.getString(KEY_AFPP_URL);
    }

    public static String getAppMenuUrl() {

        return firebaseRemoteConfig.getString(KEY_APP_MENU_MSG);
    }

    public static String getProfileUpdateUrl() {
        return  firebaseRemoteConfig.getString(KEY_PROFILE_UPDATE_URL);

    }

    public static String getContactUs() {

        return  firebaseRemoteConfig.getString(KEY_CONTACT_US_URL);
    }

    public static String getReqOTP() {
        
        return  firebaseRemoteConfig.getString(KEY_REQ_OTP_URL);
    }

    public static String getPaoPtoStatusUrl() {
        return  firebaseRemoteConfig.getString(KEY_PAO_PTO_STATUS_URL);

    }


    public FirebaseRemoteConfig getFirebaseRemoteConfig() {
        return firebaseRemoteConfig;
    }

    public void setFirebaseRemoteConfig(FirebaseRemoteConfig firebaseRemoteConfig) {
        this.firebaseRemoteConfig = firebaseRemoteConfig;
    }

    public static String getFlashMsgUrl() {

        return firebaseRemoteConfig.getString(KEY_URL_FLASH_MSG);
    }

    public static double getConfigAppVersion() {
        return Double.parseDouble(firebaseRemoteConfig.getString(KEY_CONFIG_APP_VERSION));
    }

    public static String getConfigAppLink() {
        return firebaseRemoteConfig.getString(KEY_CONFIG_APP_LINK);
    }

    public static String getBaseUrl() {
        return firebaseRemoteConfig.getString(KEY_BASE_URL);
    }

    public static String getPDFDownloadUrl() {
        return firebaseRemoteConfig.getString(KEY_URL_PDF_DOWNLOAD);
    }

    public static String getGrievanceTypeUrl() {
        return firebaseRemoteConfig.getString(KEY_URL_GRIEVANCE_TYPE);
    }

    public static String getLodgeGrievanceUrl() {
        return firebaseRemoteConfig.getString(KEY_URL_LODGE_GRIEVANCE);
    }
    public static String getFeedbackGrievanceUrl() {
        return firebaseRemoteConfig.getString(KEY_URL_FEEDBACK_GRIEVANCE);
    }

    public static String getFeedbackURL() {
        return firebaseRemoteConfig.getString(KEY_URL_FEEDBACK);
    }
    public static String getViewGrievanceUrl() {
        return firebaseRemoteConfig.getString(KEY_URL_VIEW_GRIEVANCE);
    }

    public static String getSessionTime() {
        return firebaseRemoteConfig.getString(KEY_CONFIG_SESSION_TIME);
    }
}
