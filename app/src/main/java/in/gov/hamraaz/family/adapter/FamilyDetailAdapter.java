package in.gov.hamraaz.family.adapter;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.gov.hamraaz.R;
import in.gov.hamraaz.family.model.ModelForFamilyDetail;

public class FamilyDetailAdapter extends RecyclerView.Adapter {
    private final FragmentActivity activity;
    private List<ModelForFamilyDetail> modelForFamilyDetail;


    public FamilyDetailAdapter(FragmentActivity activity) {
        this.activity = activity;
        modelForFamilyDetail = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        View v;
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.family_adapter, parent, false);
        holder = new ItemViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ItemViewHolder) holder).bind(position);
    }

    @Override
    public int getItemCount() {
        return modelForFamilyDetail.size();
    }

    public void setData(List<ModelForFamilyDetail> modelForFamilyDetails) {
        modelForFamilyDetail = modelForFamilyDetails;
        notifyDataSetChanged();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ser)
        TextView ser;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.dob)
        TextView dob;
        @BindView(R.id.relation)
        TextView relation;
        @BindView(R.id.ptodot)
        TextView ptodot;
        @BindView(R.id.ptono)
        TextView ptono;
        public ItemViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        public void bind(int position) {
            ModelForFamilyDetail familtDettailBean = modelForFamilyDetail.get(position);
            int id=++position;
            ser.setText(""+id);
            name.setText(""+familtDettailBean.getFM_NAME());
            dob.setText(""+familtDettailBean.getDOB());
            relation.setText(""+familtDettailBean.getRELATION());
            ptono.setText(""+familtDettailBean.getPto_no());
            ptodot.setText(""+familtDettailBean.getPto_dt());

        }
    }
}
