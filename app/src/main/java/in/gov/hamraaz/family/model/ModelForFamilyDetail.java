package in.gov.hamraaz.family.model;

import com.google.gson.annotations.SerializedName;

public class ModelForFamilyDetail {

    /**
     * FM_NAME : B ASWANI
     * RELATION : Wife
     * DOB : 15/06/1986
     * pto_no : null
     * pto_dt : null
     */

    @SerializedName("FM_NAME")
    private String FM_NAME;

    @SerializedName("RELATION")
    private String RELATION;

    @SerializedName("DOB")
    private String DOB;

    @SerializedName("pto_no")
    private String pto_no;

    @SerializedName("pto_dt")
    private String pto_dt;

    public String getFM_NAME() {
        return FM_NAME;
    }

    public void setFM_NAME(String FM_NAME) {
        this.FM_NAME = FM_NAME;
    }

    public String getRELATION() {
        return RELATION;
    }

    public void setRELATION(String RELATION) {
        this.RELATION = RELATION;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getPto_no() {
        return pto_no;
    }

    public void setPto_no(String pto_no) {
        this.pto_no = pto_no;
    }

    public String getPto_dt() {
        return pto_dt;
    }

    public void setPto_dt(String pto_dt) {
        this.pto_dt = pto_dt;
    }
}
