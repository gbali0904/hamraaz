package in.gov.hamraaz.obsns;

public class ModelForObsns {

    /**
     * fd_desc : TFRDEP
     * fd_amt_claimed : 68500
     * fd_dt_recd_ro : 2017-08-16
     * fd_dt_recd_paor : 2017-08-17
     * fd_status_paor : Approved
     * fd_adjusted_mps : 09/2017
     * p_hash : D85C55311B47103939B060B03971D3C6C6DDAE2F
     */

    private String fd_desc;
    private String fd_amt_claimed;
    private String fd_dt_recd_ro;
    private String fd_dt_recd_paor;
    private String fd_status_paor;
    private String fd_adjusted_mps;
    private String p_hash;

    public String getFd_desc() {
        return fd_desc;
    }

    public void setFd_desc(String fd_desc) {
        this.fd_desc = fd_desc;
    }

    public String getFd_amt_claimed() {
        return fd_amt_claimed;
    }

    public void setFd_amt_claimed(String fd_amt_claimed) {
        this.fd_amt_claimed = fd_amt_claimed;
    }

    public String getFd_dt_recd_ro() {
        return fd_dt_recd_ro;
    }

    public void setFd_dt_recd_ro(String fd_dt_recd_ro) {
        this.fd_dt_recd_ro = fd_dt_recd_ro;
    }

    public String getFd_dt_recd_paor() {
        return fd_dt_recd_paor;
    }

    public void setFd_dt_recd_paor(String fd_dt_recd_paor) {
        this.fd_dt_recd_paor = fd_dt_recd_paor;
    }

    public String getFd_status_paor() {
        return fd_status_paor;
    }

    public void setFd_status_paor(String fd_status_paor) {
        this.fd_status_paor = fd_status_paor;
    }

    public String getFd_adjusted_mps() {
        return fd_adjusted_mps;
    }

    public void setFd_adjusted_mps(String fd_adjusted_mps) {
        this.fd_adjusted_mps = fd_adjusted_mps;
    }

    public String getP_hash() {
        return p_hash;
    }

    public void setP_hash(String p_hash) {
        this.p_hash = p_hash;
    }
}

