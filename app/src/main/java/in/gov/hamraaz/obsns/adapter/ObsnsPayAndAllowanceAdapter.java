package in.gov.hamraaz.obsns.adapter;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.gov.hamraaz.R;
import in.gov.hamraaz.obsns.ModelForObsns;

public class ObsnsPayAndAllowanceAdapter extends RecyclerView.Adapter {
    private final FragmentActivity activity;

    private List<ModelForObsns> ModelForObsnss;

    public ObsnsPayAndAllowanceAdapter(FragmentActivity activity) {
        this.activity = activity;
        ModelForObsnss = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        View v;
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.obsns_pay_and_allow_adapter, parent, false);
        holder = new ItemViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ItemViewHolder) holder).bind(position);
    }

    @Override
    public int getItemCount() {
        return ModelForObsnss.size();
    }

    public void setData(List<ModelForObsns> ModelForObsnss) {
        this.ModelForObsnss = ModelForObsnss;
        notifyDataSetChanged();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ser)
        TextView ser;
        @BindView(R.id.items)
        TextView items;
        @BindView(R.id.amtclaimed)
        TextView amtclaimed;
        @BindView(R.id.DtrecdInPAO)
        TextView DtrecdInPAO;
        @BindView(R.id.dtRecdinro)
        TextView dtRecdinro;
        @BindView(R.id.statusinPAO)
        TextView statusinPAO;
        @BindView(R.id.AdjustedinMps)
        TextView AdjustedinMps;
        public ItemViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        public void bind(int position) {
            ModelForObsns modelForObsns = ModelForObsnss.get(position);
            int id=++position;
            ser.setText(""+id);
            items.setText(""+modelForObsns.getFd_desc());
            amtclaimed.setText(""+modelForObsns.getFd_amt_claimed());
            DtrecdInPAO.setText(""+modelForObsns.getFd_dt_recd_paor());
            dtRecdinro.setText(""+modelForObsns.getFd_dt_recd_ro());
            statusinPAO.setText(""+modelForObsns.getFd_status_paor());
            AdjustedinMps.setText(""+modelForObsns.getFd_adjusted_mps());
        }
    }
}
