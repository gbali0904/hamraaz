package in.gov.hamraaz.changepassword;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.gov.hamraaz.Home.MainActivity;
import in.gov.hamraaz.R;
import in.gov.hamraaz.Utils.DialogUtil;
import in.gov.hamraaz.Utils.EncryptionUtil;
import in.gov.hamraaz.Utils.RemoteConfigManager;
import in.gov.hamraaz.Utils.TextUtil;
import in.gov.hamraaz.Utils.Util;
import in.gov.hamraaz.changepassword.model.SequrityQuestions;
import in.gov.hamraaz.login.LoginActivity;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;

/**
 * Created by Syamu on 26-08-2017.
 */

public class ChangePasswordActivity extends AppCompatActivity {
    public static final String KEY_PAN_HASH = "pan_hash";
    EditText edtOPwd;
    EditText edtPwd;
    EditText edtCPwd;
    boolean Check_rept = true;
    Spinner spin_qn;
    EditText edtAns;
    Button btnsubmit;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private String panHash;

    List<String> Security_Qn = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chpwd);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Change password");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        Bundle extras = getIntent().getExtras();
        if (null != extras) {
            panHash = extras.getString(KEY_PAN_HASH);
        }

        spin_qn = (Spinner) findViewById(R.id.spinner_security_qnC);
        edtOPwd = (EditText) findViewById(R.id.edit_Curr_Pwd);
        edtPwd = (EditText) findViewById(R.id.edit_pwdC);
        edtCPwd = (EditText) findViewById(R.id.edit_cpwdC);
        edtAns = (EditText) findViewById(R.id.edit_ansC);

        initializeSecurityQuestions();

        btnsubmit = (Button) findViewById(R.id.btn_chg_submit);
        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (validatePassword(edtPwd.getText().toString()) && edtOPwd.getText()
                        .toString()
                        .length() > 2 && edtAns.getText().toString().length() > 2) {
                    if (TextUtil.isEqual(edtPwd.getText().toString(), edtCPwd.getText()
                            .toString())) {

                        String public_path = RemoteConfigManager.getChangeExistingPassUrl();

                        String android_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                        makeServerCall(public_path, panHash, EncryptionUtil.getHashValue(edtOPwd.getText()
                                .toString()
                                .trim(), EncryptionUtil.USER_HASH_SALT), EncryptionUtil.getHashValue(edtPwd
                                .getText()
                                .toString()
                                .trim(), EncryptionUtil.USER_HASH_SALT), spin_qn.getSelectedItem()
                                .toString(), edtAns.getText().toString(), android_id);

                    } else {
                        AlertDialog alertDialog = new AlertDialog.Builder(ChangePasswordActivity.this)
                                .create();
                        final String update_message = "Password and Confirm Password must match";
                        alertDialog.setTitle("Check");
                        alertDialog.setMessage(update_message);
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, " Okay ", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();


                            }
                        });
                        alertDialog.show();
                    }
                } else {

                    AlertDialog alertDialog = new AlertDialog.Builder(ChangePasswordActivity.this).create();
                    final String update_message = "Please check password and security answer field" + "In Password \n" + "A digit must be there\n" + "A lower case letter must be there\n" + "An upper case letter must be there\n" + "A special character must be there\n" + "No whitespace allowed\n" + "Minimum 12 characters\n" + "Password and Confirm Password must match";
                    alertDialog.setTitle("Check");
                    alertDialog.setMessage(update_message);
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, " Okay ", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();


                        }
                    });
                    alertDialog.show();
                }

            }
        });


    }


    private void initializeSecurityQuestions() {
        final ProgressDialog progressDialog = DialogUtil.createProgressDialog(ChangePasswordActivity.this, "Request OTP", "Please wait while we verifying your OTP");
        progressDialog.show();
        String url = RemoteConfigManager.getSequrityQuestion();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.cancel();
                SequrityQuestions sequrityQuestions = new Gson().fromJson(response, SequrityQuestions.class);
                List<SequrityQuestions.SecQuesBean> secQues = sequrityQuestions.getSecQues();
                for (SequrityQuestions.SecQuesBean seq_question : secQues) {
                    Security_Qn.add(seq_question.getSec_ques());
                }
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(ChangePasswordActivity.this, android.R.layout.simple_spinner_item, Security_Qn);
                // Drop down layout style - list view with radio button
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                // Drop down layout style - list view with radio button
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                // attaching data adapter to spinner
                spin_qn.setAdapter(dataAdapter);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.cancel();
                DialogUtil.createAlertDialog(ChangePasswordActivity.this, "Error", "Something went wrong Please try again", "Okay")
                        .show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + panHash);
                headers.put("timestamp", Util.getTimestamp());
                return headers;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            default:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public String getHashValue(String toHash, String salt) {
        try {
            String join = toHash + salt;
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] array = md.digest(join.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString().toUpperCase();
        } catch (NoSuchAlgorithmException e) {
        }
        return null;
    }

    String saveToPreference(String syamu) {
        SharedPreferences sharedpreferences;
        sharedpreferences = getSharedPreferences("Ammumma", Context.MODE_PRIVATE);
        String savedValue = sharedpreferences.getString(syamu, "");
        return savedValue;
    }

    void makeServerCall(final String url, final String u, final String oldPassword, final String currentPassord, final String q1, final String q2, final String d) {

        if (Check_rept == true) {
            btnsubmit.setAlpha(0.36f);
            btnsubmit.setEnabled(false);
            Check_rept = false;
            check_rept();

            Log.e("this","url::"+url);

            final ProgressDialog mProgressDialog = DialogUtil.createProgressDialog(ChangePasswordActivity.this, "Changing your password", "Please wait while we are changing your password");
            mProgressDialog.show();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    mProgressDialog.cancel();

                    ModelForChangePassword modelForChangePassword = new Gson().fromJson(response, ModelForChangePassword.class);


                    if (modelForChangePassword.getCh_curr().getCode() == 1) {
                        btnsubmit.setEnabled(true);
                        btnsubmit.setAlpha(1.00f);

                        DialogUtil.createAlertDialog(ChangePasswordActivity.this, "Success", modelForChangePassword.getCh_curr().getMsg(), "Okay", false, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(ChangePasswordActivity.this, LoginActivity.class);
                                intent.setFlags(FLAG_ACTIVITY_CLEAR_TOP);
                                intent.putExtra(MainActivity.KEY_PAN_HASH, panHash);
                                startActivity(intent);
                                finishAffinity();
                            }
                        }).show();

                    } else if (modelForChangePassword.getCh_curr().getCode() == 2) {
                        AlertDialog alertDialog = new AlertDialog.Builder(ChangePasswordActivity.this)
                                .create();
                        final String update_message = modelForChangePassword.getCh_curr().getMsg();
                        final String update_title = "Error";
                        alertDialog.setTitle(update_title);
                        alertDialog.setMessage(update_message);

                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, " Okay ", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();
                                btnsubmit.setEnabled(true);

                                btnsubmit.setAlpha(1.00f);


                            }
                        });
                        alertDialog.show();
                    } else {
                        AlertDialog alertDialog = new AlertDialog.Builder(ChangePasswordActivity.this)
                                .create();
                        final String update_message = modelForChangePassword.getCh_curr().getMsg();
                        final String update_title = "Sorry";
                        alertDialog.setTitle(update_title);
                        alertDialog.setMessage(update_message);
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, " Okay ", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();
                                btnsubmit.setEnabled(true);
                                btnsubmit.setAlpha(1.00f);


                            }
                        });
                        alertDialog.show();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    mProgressDialog.cancel();
                    AlertDialog alertDialog = new AlertDialog.Builder(ChangePasswordActivity.this).create();
                    final String update_message = "Something went wrong that all I know";
                    final String update_title = "Sorry";
                    alertDialog.setTitle(update_title);
                    alertDialog.setMessage(update_message);

                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, " Okay ", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();
                            btnsubmit.setEnabled(true);

                            btnsubmit.setAlpha(1.00f);


                        }
                    });
                    alertDialog.show();


                }
            }) {

                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("Authorization", "Bearer " + u);
                    headers.put("timestamp", Util.getTimestamp());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("schemeFrame", u);
                    params.put("pw", oldPassword);
                    params.put("new_pw", currentPassord);
                    params.put("q1", q1);
                    params.put("q2", q2);
                    String ipAddress = Util.getIPAddress(true);
                    params.put("ip", ipAddress);
                    params.put("mode", "1_ch_curr");
                    Log.e("this","parameters::::"+new Gson().toJson(params));

                /*{
	"schemeFrame" : "C3B2710B1C8B3DBAAFD76EBBA9581CC17E5EB9DE7234DB67DF2A9875D1F873AC",
	"pw" : "258FC0DF163F4AFED12AC021A20E26DDE8B695BDA16CC6FAD423C7879E840305",
	"mode":"1_ch_curr",
	"q1" : "Where do you born?",
	"q2" : "srivai",
	"ip" : "100.85.122.92",
	"new_pw":"258FC0DF163F4AFED12AC021A20E26DDE8B695BDA16CC6FAD423C7879E840305"
}
*/


                    return params;
                }

            };
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);


        }
    }

    void check_rept() {
        try {
            if (Check_rept == false) {
                new CountDownTimer(2700, 1000) {
                    public void onFinish() {

                        Check_rept = true;


                    }

                    @Override
                    public void onTick(long arg0) {


                    }
                }.start();
            }
        } catch (Exception ex) {
        }

    }

    public static boolean validatePassword(String pwd) {
        return (pwd.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{12,}$"));
    }

}
