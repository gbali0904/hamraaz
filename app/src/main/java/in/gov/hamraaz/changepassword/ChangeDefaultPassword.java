package in.gov.hamraaz.changepassword;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.gov.hamraaz.Home.MainActivity;
import in.gov.hamraaz.R;
import in.gov.hamraaz.Utils.Constant;
import in.gov.hamraaz.Utils.DialogUtil;
import in.gov.hamraaz.Utils.EncryptionUtil;
import in.gov.hamraaz.Utils.RemoteConfigManager;
import in.gov.hamraaz.Utils.Util;
import in.gov.hamraaz.changepassword.model.SequrityQuestions;
import in.gov.hamraaz.login.LoginActivity;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;

public class ChangeDefaultPassword extends AppCompatActivity {


    public static final String KEY_PAN_HASH = "pan";
    public static final String STATUS_MODE = "status";
    @BindView(R.id.txtCdpPassword)
    TextView txtCdpPassword;
    @BindView(R.id.edtCdpPassword)
    EditText edtCdpPassword;
    @BindView(R.id.txtCdpConfirmPassword)
    TextView txtCdpConfirmPassword;
    @BindView(R.id.edtCdpConfirmPassword)
    EditText edtCdpConfirmPassword;
    @BindView(R.id.txtCdpSecurityQuestion)
    TextView txtCdpSecurityQuestion;
    @BindView(R.id.spiCdpSecurityQuestion)
    Spinner spiCdpSecurityQuestion;
    @BindView(R.id.edtCdpSecurityAnswer)
    EditText edtCdpSecurityAnswer;
    @BindView(R.id.swCdpTerm)
    Switch swCdpTerm;
    @BindView(R.id.btnCdpRegister)
    Button btnCdpRegister;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    List<String> Security_Qn = new ArrayList<String>();
    private String panHash = "";
    private String status;

    public static boolean isValidPassword(String pwd) {
        return (pwd.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{12,}$"));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_default_password);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Change password");

        toggleButtonEnable(false);
        Bundle extras = getIntent().getExtras();
        if (null != extras) {
            panHash = extras.getString(ChangeDefaultPassword.KEY_PAN_HASH);
            status = extras.getString(ChangeDefaultPassword.STATUS_MODE);
        }

        initializeSecurityQuestions();

        initInputValidation();


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            default:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent LaunchIntent = new Intent(ChangeDefaultPassword.this, LoginActivity.class);
        LaunchIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(LaunchIntent);
        finish();
    }

    private void initInputValidation() {
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //toggle register button on the basis of validation
                toggleButtonEnable(isAllFieldsValid());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
        edtCdpSecurityAnswer.addTextChangedListener(textWatcher);
        edtCdpPassword.addTextChangedListener(textWatcher);
        edtCdpConfirmPassword.addTextChangedListener(textWatcher);

    }

    private boolean isAllFieldsValid() {
        String securityAnswer = edtCdpSecurityAnswer.getText().toString();
        String password = edtCdpPassword.getText().toString();
        String confirmPassword = edtCdpConfirmPassword.getText().toString();

        if (TextUtils.isEmpty(securityAnswer) || securityAnswer.length() < 3) {
            edtCdpSecurityAnswer.setError("The answer must contain at least 3 characters");
            return false;
        }

        if (TextUtils.isEmpty(password) || !isValidPassword(password)) {
            edtCdpPassword.setError("Your password must be minimum 12 characters and contain a digit, a lower case, an upper case and a special character. No whitesapce allowed ");
            return false;
        }

        if (TextUtils.isEmpty(confirmPassword) || !(confirmPassword.equals(password))) {
            edtCdpConfirmPassword.setError("Confirm password is not matching");
            return false;
        }

        return true;
    }

    private boolean isTermsAccepted() {
        if (!swCdpTerm.isChecked()) {
            DialogUtil.createAlertDialog(ChangeDefaultPassword.this, "Error", "You must accept term and conditions", "Okay")
                    .show();
            return false;
        }
        return true;
    }

    private void initializeSecurityQuestions() {


        final ProgressDialog progressDialog = DialogUtil.createProgressDialog(ChangeDefaultPassword.this, "Request OTP", "Please wait while we verifying your OTP");
        progressDialog.show();

        String url = RemoteConfigManager.getSequrityQuestion();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.cancel();
                SequrityQuestions sequrityQuestions = new Gson().fromJson(response, SequrityQuestions.class);
                List<SequrityQuestions.SecQuesBean> secQues = sequrityQuestions.getSecQues();
                for (SequrityQuestions.SecQuesBean seq_question : secQues) {
                    Security_Qn.add(seq_question.getSec_ques());
                }
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(ChangeDefaultPassword.this, android.R.layout.simple_spinner_item, Security_Qn);
                // Drop down layout style - list view with radio button
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                // attaching data adapter to spinner
                spiCdpSecurityQuestion.setAdapter(dataAdapter);

                //Initialize term and condition switch
                swCdpTerm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            showTermAndConditions();
                            btnCdpRegister.setEnabled(true);
                        } else {
                            btnCdpRegister.setEnabled(false);
                        }
                    }
                });

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.cancel();
                DialogUtil.createAlertDialog(ChangeDefaultPassword.this, "Error ChangePassword", "Something went wrong Please try again", "Okay")
                        .show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + panHash);
                headers.put("timestamp", Util.getTimestamp());
                return headers;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    private void showTermAndConditions() {
        AlertDialog alertDialog = new AlertDialog.Builder(ChangeDefaultPassword.this).create();

        final String update_message = "I will ensure that information viewed/downloaded from this app will not be shared with any unathorized person, it will be taken as an advance information and will not be used as the authority other than digitally signed PDF Documents" + "" + "\n\nI will personally ensure security of this App installed on my Mobile and will not share my credentials to anyone else. " + "Immediately on viewing/downloading the required information I will signout from the mobile App" + "\n\nI will not upload any operational/sensitive info and will not use inappropriate language while submitting grievances/other inputs through this mobile App" + "\n\nIn case of violation of the above terms & condition, I shall be liable for disciplinary action.";
        final String update_title = "Terms & Condition";
        alertDialog.setTitle(update_title);
        alertDialog.setMessage(update_message);
        alertDialog.setCancelable(false);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, " ACCEPT ", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                swCdpTerm.setChecked(true);
                dialog.dismiss();
            }
        });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, " REJECT ", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                swCdpTerm.setChecked(false);
            }
        });
        alertDialog.show();
    }

    private void toggleButtonEnable(boolean isEnable) {
        if (isEnable) {
            btnCdpRegister.setEnabled(true);
            btnCdpRegister.setAlpha(1.00f);
        } else {
            btnCdpRegister.setEnabled(false);
            btnCdpRegister.setAlpha(0.36f);
        }
    }

    @OnClick({R.id.btnCdpRegister})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnCdpRegister:
                if (isTermsAccepted()) {
                    changePasswordAfterAuth();
                }
                break;
        }
    }

    private void changePasswordAfterAuth() {
        final ProgressDialog progressDialog = DialogUtil.createProgressDialog(ChangeDefaultPassword.this, "Change password", "Please wait while we are changing your password");
        progressDialog.show();

        String url = RemoteConfigManager.getChangePassAfterAuthUrl();
        // String url = RemoteConfigManager.getForgotPassCheckUrl();

        Log.e("this", "url::::" + url);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                Log.e("this", "response::::" + response);
                progressDialog.cancel();

                if (response.equalsIgnoreCase("Success")) {
                    DialogUtil.createAlertDialog(ChangeDefaultPassword.this, "Congratulations",response, "Okay", false, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(ChangeDefaultPassword.this, LoginActivity.class);
                            intent.setFlags(FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtra(MainActivity.KEY_PAN_HASH, panHash);
                            startActivity(intent);
                            finishAffinity();

                        }
                    }).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.cancel();
                DialogUtil.createAlertDialog(ChangeDefaultPassword.this, "Error", "Something went wrong Please try again", "Okay")
                        .show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                String passwordHash = EncryptionUtil.getHashValue(edtCdpPassword.getText().toString(), EncryptionUtil.USER_HASH_SALT);
                String securityQuestion = Security_Qn.get(spiCdpSecurityQuestion.getSelectedItemPosition());
                String securityAnswer = edtCdpSecurityAnswer.getText().toString().trim();
                params.put("pan", panHash);// password hash
                params.put("p", passwordHash);// password hash
                params.put("q1", securityQuestion);// security question raw
                params.put("q2", securityAnswer);// security answer raw

                params.put("mode", status);// pan hash
                String ipAddress = Util.getIPAddress(true);
                params.put("REMOTE_HOST", ipAddress);

                String androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                params.put("android_id", androidId);


                Log.e("this", "DATA::::" + params);

                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
