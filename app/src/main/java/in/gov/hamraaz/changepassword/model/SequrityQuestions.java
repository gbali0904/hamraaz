package in.gov.hamraaz.changepassword.model;


import java.util.List;

public class SequrityQuestions {


    private List<SecQuesBean> SecQues;

    public List<SecQuesBean> getSecQues() {
        return SecQues;
    }

    public void setSecQues(List<SecQuesBean> SecQues) {
        this.SecQues = SecQues;
    }

    public static class SecQuesBean {
        /**
         * Sec_ques : Who is your childhood hero?
         */

        private String Sec_ques;

        public String getSec_ques() {
            return Sec_ques;
        }

        public void setSec_ques(String Sec_ques) {
            this.Sec_ques = Sec_ques;
        }
    }
}
