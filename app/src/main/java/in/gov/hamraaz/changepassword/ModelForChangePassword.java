package in.gov.hamraaz.changepassword;

public class ModelForChangePassword {

    /**
     * Ch_curr : {"code":1,"error":true,"msg":"Success"}
     */

    private ChCurrBean Ch_curr;

    public ChCurrBean getCh_curr() {
        return Ch_curr;
    }

    public void setCh_curr(ChCurrBean Ch_curr) {
        this.Ch_curr = Ch_curr;
    }

    public static class ChCurrBean {
        /**
         * code : 1
         * error : true
         * msg : Success
         */

        private int code;
        private boolean error;
        private String msg;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public boolean isError() {
            return error;
        }

        public void setError(boolean error) {
            this.error = error;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }
    }
}
