package in.gov.hamraaz;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import in.gov.hamraaz.Utils.Util;
import in.gov.hamraaz.login.LoginActivity;
import in.gov.hamraaz.Home.MainActivity;
import in.gov.hamraaz.Utils.DialogUtil;
import in.gov.hamraaz.Utils.FcmManager;
import in.gov.hamraaz.Utils.RemoteConfigManager;
import in.gov.hamraaz.Utils.TAGs;
import in.gov.hamraaz.Utils.TextUtil;

public class SplashActivity extends AppCompatActivity {

    private FirebaseRemoteConfig mFirebaseRemoteConfig;
    private long cacheExpiration = 120;// two minutes
    private ProgressDialog mProgressDialog;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        if (!Util.checkInternet(SplashActivity.this)) {
            FcmManager.getInstance().updateToken("abc");
            startNextActivity();
        } else {
            //Log firebase event
            logFirebaseEvent("0", "item_Sign_Option", "content_Sign_Option");
            downloadRemoteConfigData();
        }
    }
    private void logFirebaseEvent(String item_id, String item_name, String content_type) {
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, item_id);
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, item_name);
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, content_type);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }

    private void checkCurrentVersion() {
        double currentVersion = Double.parseDouble(BuildConfig.VERSION_NAME);
        double latestVersion = RemoteConfigManager.getConfigAppVersion();
        final String latestAppLink = RemoteConfigManager.getConfigAppLink();

        mProgressDialog = DialogUtil.createProgressDialog(SplashActivity.this, "Authorising Version Integrity", "Please Wait...");
        mProgressDialog.show();

        mProgressDialog.dismiss();
        if(currentVersion < latestVersion){// need to download latest version
            DialogUtil.createAlertDialog(SplashActivity.this, "Version Integrity failed", "Please download latest version to use this App.  Touch OKAY to proceed.", "Okay", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    try {
                        Uri uri = Uri.parse(latestAppLink);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    } catch (Exception Ex) {
                    }
                }
            }).show();
        }
        else {
            startNextActivity();
        }
    }
    private void downloadRemoteConfigData() {
        final ProgressDialog progressDialog = DialogUtil.createProgressDialog(SplashActivity.this, "Initializing", "Please wait we are preparing the app to work for you");
        progressDialog.show();
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);

        mFirebaseRemoteConfig.fetch(cacheExpiration)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        progressDialog.dismiss();
                        if (task.isSuccessful()) {
                            mFirebaseRemoteConfig.activateFetched();
                            RemoteConfigManager.getInstance().setFirebaseRemoteConfig(mFirebaseRemoteConfig);

                            getFCMId();

                        } else {
                            Log.e("Firebase", "Remote config download failed");
                            showErrorDialog();
                        }
                    }
                });
    }

    private void showErrorDialog() {
        DialogUtil.createAlertDialog(SplashActivity.this, "Error ", "Something went wrong. Please check you internet connection and try again", "Try again", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                downloadRemoteConfigData();
            }
        }).show();
    }

    private void getFCMId() {
        // Get token
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("FCM", "getInstanceId failed", task.getException());
                            showErrorDialog();
                            return;
                        }

                        String token = task.getResult().getToken();

                        Log.d("FCM", token);

                        FcmManager.getInstance().updateToken(token);
                        checkCurrentVersion();
                    }
                });
    }

    private void startNextActivity() {
        if (App.getInstance().getPreference().getBoolean(TAGs.LOGIN_STATUS,false)) {
            startActivity(new Intent(SplashActivity.this, MainActivity.class));
            finish();
        }
        else {
           // AccountLandingActivity.startActivity(SplashActivity.this);
            startActivity(new Intent(SplashActivity.this, LoginActivity.class));
            SplashActivity.this.finish();
        }
    }


}
