package in.gov.hamraaz.fundwithdraw;

public class ModelForFundWithdrawal {

    /**
     * fd_ro_recd_dt : 02/08/2019
     * fd_pao_recd_dt : 08/08/2018
     * fd_pao_passed_dt : 10/08/2018
     * fd_amt_passed : 250000
     */

    private String fd_ro_recd_dt;
    private String fd_pao_recd_dt;
    private String fd_pao_passed_dt;
    private int fd_amt_passed;

    public String getFd_ro_recd_dt() {
        return fd_ro_recd_dt;
    }

    public void setFd_ro_recd_dt(String fd_ro_recd_dt) {
        this.fd_ro_recd_dt = fd_ro_recd_dt;
    }

    public String getFd_pao_recd_dt() {
        return fd_pao_recd_dt;
    }

    public void setFd_pao_recd_dt(String fd_pao_recd_dt) {
        this.fd_pao_recd_dt = fd_pao_recd_dt;
    }

    public String getFd_pao_passed_dt() {
        return fd_pao_passed_dt;
    }

    public void setFd_pao_passed_dt(String fd_pao_passed_dt) {
        this.fd_pao_passed_dt = fd_pao_passed_dt;
    }

    public int getFd_amt_passed() {
        return fd_amt_passed;
    }

    public void setFd_amt_passed(int fd_amt_passed) {
        this.fd_amt_passed = fd_amt_passed;
    }
}
