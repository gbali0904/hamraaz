package in.gov.hamraaz.fundwithdraw.adapter;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.gov.hamraaz.R;
import in.gov.hamraaz.fundwithdraw.ModelForFundWithdrawal;

public class FundWithdrawalAdapter extends RecyclerView.Adapter {
    private final FragmentActivity activity;

    private List<ModelForFundWithdrawal> modelForFundWithdrawals;

    public FundWithdrawalAdapter(FragmentActivity activity) {
        this.activity = activity;
        modelForFundWithdrawals=new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        View v;
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fund_withdrawal_adapter, parent, false);
        holder = new ItemViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ItemViewHolder) holder).bind(position);
    }

    @Override
    public int getItemCount() {
         return modelForFundWithdrawals.size();
    }

    public void setData(List<ModelForFundWithdrawal> modelForFundWithdrawals) {
        this.modelForFundWithdrawals = modelForFundWithdrawals;
        notifyDataSetChanged();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ser)
        TextView ser;
        @BindView(R.id.DtRORecd)
        TextView DtRORecd;
        @BindView(R.id.DtPAORecd)
        TextView DtPAORecd;
        @BindView(R.id.DtPAOPassed)
        TextView DtPAOPassed;
        @BindView(R.id.amtPassed)
        TextView amtPassed;
        public ItemViewHolder(View v) {
            super(v);
            ButterKnife.bind(this,v);
        }

        public void bind(int position) {
            ModelForFundWithdrawal modelForFundWithdrawal = modelForFundWithdrawals.get(position);
            int id=++position;
            ser.setText(""+id);
            DtRORecd.setText(""+modelForFundWithdrawal.getFd_ro_recd_dt());
            DtPAOPassed.setText(""+modelForFundWithdrawal.getFd_pao_passed_dt());
            DtPAORecd.setText(""+modelForFundWithdrawal.getFd_pao_recd_dt());
            amtPassed.setText(""+modelForFundWithdrawal.getFd_amt_passed());

        }
    }
}
